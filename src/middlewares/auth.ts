import { verifyIdToken } from "@/database/admin";
import createResponse from "@/util/createResponse";
import { auth } from "firebase-admin";
import { NextApiRequest, NextApiResponse } from "next";
import { NextHandler } from "next-connect";
import { parseCookies } from "nookies";

export async function AuthenticatedMiddleware(
  req: NextApiRequest & RequestWithRole,
  res: NextApiResponse,
  next: NextHandler
) {
  try {
    const cookie = parseCookies({ req });
    const token: auth.DecodedIdToken = await verifyIdToken(cookie.idtoken);
    req.isAuthenticated = true;
    req.role = token.role;
    next();
  } catch (err) {
    res.status(400).send(createResponse("No user detected", "error"));
  }
}
