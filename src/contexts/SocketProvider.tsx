import React, { createContext, useContext, useState, useEffect } from "react";
import io, { Socket } from "socket.io-client";

const SocketContext = createContext<Socket<any, any>>({} as Socket);

export function useSocket() {
  return useContext(SocketContext);
}

export function SocketProvider({ children }: HasChildren & HasId) {
  const [socket, setSocket] = useState<Socket<any, any>>(() => {
    return io("http://localhost:3000", {});
  });
  useEffect(() => {
    return () => {
      socket.close();
    };
  }, [socket]);

  return (
    <SocketContext.Provider value={socket}>{children}</SocketContext.Provider>
  );
}
