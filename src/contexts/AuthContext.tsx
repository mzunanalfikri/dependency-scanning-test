import { auth } from "@/database/firebase";
import firebase from "firebase";
import { destroyCookie, setCookie } from "nookies";
import React, { createContext, useContext, useEffect, useState } from "react";

interface AuthData {
  user: firebase.User | null;
  signin: Function;
  signout: Function;
}

async function signout() {
  if (!auth.currentUser) {
    return;
  }
  console.log("signing out...");
  destroyCookie({}, "idtoken");
  await auth.signOut();
}

async function signin(email: string, password: string) {
  const userCred = await auth.signInWithEmailAndPassword(email, password);
  return userCred.user;
}

const AuthContext = createContext<AuthData>({
  user: null,
  signin,
  signout,
});

function AuthProvider({ children }: { children: React.ReactNode }) {
  const [user, setUser] = useState<firebase.User | null>(null);
  useEffect(() => {
    auth.onAuthStateChanged(async (user) => {
      if (!user) {
        destroyCookie({}, "idtoken");
        setUser(null);
        return;
      } else {
        const idToken = await user.getIdToken();
        setCookie(null, "idtoken", idToken, {
          path: "/",
        });
        setUser(user);
      }
    });
  }, []);

  return (
    <AuthContext.Provider value={{ user, signout, signin }}>
      {children}
    </AuthContext.Provider>
  );
}

export default AuthProvider;
export const useAuth = () => useContext(AuthContext);
