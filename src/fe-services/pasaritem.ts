import { getCollection } from "@/database/firebase";
import Item from "@/models/Barang";
import axios from "axios";

class PasarItemFeService {
  static async getAllItemPasar() {
    const data = await (await axios.get("http://localhost:3000/api/pasaritem"))
      .data.res;
    return data;
  }
  static async getAllItemFrom(pasarId: string) {
    const marketItems = (
      await getCollection("market").doc(pasarId).collection("items").get()
    ).docs;
    const items: Item[] = [];
    for (const marketItem of marketItems) {
      const item = await marketItem.data().item.get();
      const data: Item = {
        id: marketItem.id,
        price: marketItem.data().price,
        ...item.data(),
      };
      items.push(data);
    }

    return items;
  }
}

export default PasarItemFeService;
