import axios from "axios";
import { v4 } from "uuid";
import { storage } from "@/database/firebase";

class PasarFeService {
  static async getAllPasar() {
    const data = await (await axios.get("http://localhost:3000/api/pasar")).data
      .res;
    return data;
  }

  static async addPasar(payload) {
    try {
      const res = await axios.post("http://localhost:3000/api/pasar", payload);
      console.log(res);
      return {
        status: res.status,
        // message : res.data, // gatau kenapa datanya ngga ke send
        message: "terjadi kesalahan saat isi form",
      };
    } catch (error) {
      return {
        status: 500,
        message: "Server Error",
      };
    }
  }
  static async uploadImagePasar(image) {
    try {
      const imgPath = `pasar/${image.name}_${v4()}`;
      const itemRef = storage.child(imgPath);
      // save the image
      await itemRef.put(image);
      const imageUrl = await itemRef.getDownloadURL();
      return imageUrl;
    } catch (err) {
      console.log(err);
      throw err;
    }
  }
}

export default PasarFeService;
