import router from "next/router";

export default function RedirectClientHelper(ri: RedirectionInstruction): void {
  if (!ri) {
    return;
  }
  if (ri.redirectFrom == "none") return;
  else {
    if (ri.redirectFrom == "client") {
      router.push(ri.clientRedirectTo ?? "/");
    } else {
      // seharusnya kalo dari server udah di redirect
      // gatau handlenya kyk gimana
      return;
    }
  }
}
