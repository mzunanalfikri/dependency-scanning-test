import { db, getCollection, storage } from "@/database/firebase";
import Item from "@/models/Barang";
import axios from "axios";
import { v4 } from "uuid";

const itemsCollection = db.collection("items");

export default class TambahBarangFeService {
  static async uploadImage(image: File): Promise<string> {
    try {
      const imgPath = `items/${image.name}_${v4()}`;
      const itemRef = storage.child(imgPath);
      // save the image
      await itemRef.put(image);
      const imageUrl = await itemRef.getDownloadURL();
      console.log("successfully added the image");
      return imageUrl;
    } catch (err) {
      console.log(err);
      throw err;
    }
  }

  static async createItem(barang: Item & { marketId: string }): Promise<any> {
    try {
      const newItem = { ...barang };
      const res = await axios.post("/api/pengelola-pasar/market/item", newItem);
      return res;
    } catch (err) {
      throw err;
    }
  }

  static async getItem(name: string): Promise<Item | null> {
    try {
      const item = await getCollection("items").doc(name).get();
      if (!item.exists) {
        return null;
      } else {
        return item.data() as Item;
      }
    } catch (err) {
      return null;
    }
  }
}
