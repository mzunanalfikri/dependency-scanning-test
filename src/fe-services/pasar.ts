import { db, getCollection } from "@/database/firebase";
import Item from "@/models/Barang";
import Pasar from "@/models/Pasar";
import axios from "axios";
import PasarItemFeService from "./pasaritem";

class PasarFeService {
  static async getAllPasar() {
    const data = await (await axios.get("http://localhost:3000/api/pasar")).data
      .res;
    console.log("GETTING ALL PASAR");
    console.log(data);
    return data;
  }

  static async getPasar(id: string) {
    const docRef = getCollection("market").doc(id);
    const pasar = (await docRef.get()).data() as Pasar;
    const items = await PasarItemFeService.getAllItemFrom(id);
    const data: Pasar & { items: Item[] } = {
      ...pasar,
      items,
    };
    console.log(data);
    return data;
  }

  static async addPasar(payload: any) {
    const res = await axios.post("http://localhost:3000/api/pasar", payload);
    console.log(res);
    return {
      status: res.status,
      // message : res.data, // gatau kenapa datanya ngga ke send
      message: "terjadi kesalahan saat isi form",
    };
  }
}

export default PasarFeService;
