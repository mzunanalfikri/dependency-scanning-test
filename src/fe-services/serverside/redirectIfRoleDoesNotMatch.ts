import { verifyIdToken } from "@/database/admin";
import { NextPageContext } from "next";
import nookies from "nookies";
import { NoIdTokenException, Redirecter, roleDoesNotMatch } from "./Redirect";

/**
 * dia bakal redirect ke page /redirect misal dia authenticated tp beda role
 * misal agen akses admin--> redirect
 * misal pembeli akses agen --> redirect
 * dst....
 * buat protect:
 * semua halaman yg perlu diproteksi
 */
export default async function redirectIfRoleDoesNotMatch(
  context: NextPageContext,
  role: Role | Role[]
): Promise<RedirectionInstruction<AuthUserData>> {
  const redirecter = new Redirecter(context);

  try {
    const cookies = nookies.get(context);
    if (!cookies.idtoken) {
      throw new NoIdTokenException();
    }
    console.log(cookies.idtoken);
    const token = await verifyIdToken(cookies.idtoken);

    const data: AuthUserData = {
      uid: token.uid,
      email: token.email as string,
      role: token.role ?? "pembeli",
      banned: token.banned ?? false,
      name: token.name,
    };
    if (data.banned === true) {
      return redirecter.serverRedirectIfNoReferer<AuthUserData>(
        "/banned",
        data
      );
    }
    if (typeof role == "string") {
      if (roleDoesNotMatch(role, data.role)) {
        return redirecter.serverRedirectIfNoReferer<AuthUserData>(
          "/redirect",
          data
        );
      }
    } else {
      if (!role.includes(data.role)) {
        return redirecter.serverRedirectIfNoReferer<AuthUserData>(
          "/redirect",
          data
        );
      }
    }

    if (token.role == "pengelola-pasar") {
      data.marketId = token.pasar;
    }
    // kalo role semuanya passed, baru gaskeun
    return redirecter.noRedirect(data);
  } catch (err) {
    // if error on verifying id token, then role is visitor
    if (role == "visitor" || role.includes("visitor")) {
      return {
        redirectFrom: "none",
      };
    }
    return redirecter.serverRedirectIfNoReferer<AuthUserData>("/login");
  }
}
