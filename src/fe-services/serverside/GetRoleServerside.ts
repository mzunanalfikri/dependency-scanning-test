import { verifyIdToken } from "@/database/admin";
import { NextPageContext } from "next";
import nookies from "nookies";

/**
 * get role from server side, if there is an error,
 * set role as visitor
 */

export default async function GetRoleServerside(context: NextPageContext) {
  const cookies = nookies.get(context);
  try {
    const token = await verifyIdToken(cookies.idtoken);
    let role: Role;
    if (!token.role) {
      role = "pembeli";
    } else if (token.role == "admin") {
      role = "admin";
    } else if (token.role == "agen") {
      role = "agen";
    } else if (token.role == "pengelola-pasar") {
      role = "pengelola-pasar";
    } else {
      role = "visitor";
    }
    return role;
  } catch (err) {
    // if there is an error, set the guy as a visitor
    return "visitor";
  }
}
