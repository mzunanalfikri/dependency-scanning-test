import { NextPageContext } from "next";

export default function redirect(context: NextPageContext) {
  return (path: string) => {
    context.res?.writeHead(302, { location: path });
    context.res?.end();
  };
}

export function redirectIfNoReferer<T = any>(
  context: NextPageContext,
  path: string,
  data?: T
): RedirectionInstruction<T> {
  const ri: RedirectionInstruction = {
    redirectFrom: "none",
  };
  if (!context.req?.headers.referer) {
    redirect(context)(path);
    ri.redirectFrom = "server";
  } else {
    ri.redirectFrom = "client";
    ri.clientRedirectTo = path;
  }
  if (data) {
    ri.data = data;
  }
  return ri;
}

export class Redirecter {
  private data: any = null;
  constructor(private context: NextPageContext) {}

  noRedirect<T = any>(
    suppliedData: T
  ): RedirectionInstruction<T> | PromiseLike<RedirectionInstruction<T>> {
    return {
      redirectFrom: "none",
      data: suppliedData ?? this.data ?? null,
    };
  }

  serverRedirect(path: string): void {
    redirect(this.context)(path);
  }

  setData(data: any): void {
    this.data = data;
  }

  serverRedirectIfNoReferer<T = any>(path: string, data?: T) {
    return redirectIfNoReferer<T>(this.context, path, data ?? this.data);
  }
}

export function roleDoesNotMatch(role1: Role, role2: Role) {
  return role1 != role2;
}

export class NoIdTokenException extends Error {
  constructor() {
    super("No id token found");
  }
}
