import { verifyIdToken } from "@/database/admin";
import { NextPageContext } from "next";
import nookies from "nookies";
import { NoIdTokenException, Redirecter } from "./Redirect";

// proteksi rute yang perlu authentikasi
export default async function redirectIfAuthenticated(
  context: NextPageContext
): Promise<RedirectionInstruction<AuthUserData>> {
  const redirecter = new Redirecter(context);

  try {
    const cookies = nookies.get(context);
    if (!cookies.idtoken) {
      throw new NoIdTokenException();
    }
    const token = await verifyIdToken(cookies.idtoken);
    const data: AuthUserData = {
      name: token.name,
      uid: token.uid,
      role: token.role ?? ("pembeli" as Role),
      email: token.email as string,
      banned: token.banned ?? false,
    };
    redirecter.setData(data);
    if (data.banned) {
      return redirecter.serverRedirectIfNoReferer<AuthUserData>("/banned");
    }
    switch (data.role) {
      case "admin":
        return redirecter.serverRedirectIfNoReferer("/admin");
      case "agen":
        return redirecter.serverRedirectIfNoReferer("/agen");
      case "pengelola-pasar":
        return redirecter.serverRedirectIfNoReferer("/pengelola-pasar");
      case "pembeli":
        return redirecter.serverRedirectIfNoReferer("/pasarpembeli");
      default:
        return redirecter.noRedirect<AuthUserData>(data);
    }
  } catch (err) {
    // berarti emang dia visitor, gaada id token
    return {
      redirectFrom: "none",
    };
  }
}
