import { verifyIdToken } from "@/database/admin";
import { NextPageContext } from "next";
import nookies from "nookies";

/**
 * dia bakal redirect ke page utama suatu role misal dia authenticated
 * misal admin --> /admin
 * misal pembeli --> /pasarpembeli
 * dst....
 * buat protect:
 * /login
 * /ignup
 */
export default async function isAgenAuthenticated(context: NextPageContext) {
  try {
    const cookies = nookies.get(context);
    if (!cookies.idtoken) {
      return { props: {} };
    }
    const token = await verifyIdToken(cookies.idtoken);
    if (token.role == "agen") {
      return {
        props: {
          name: token.name,
          role: token.role,
        },
      };
    } else {
      return false;
    }
  } catch (err) {
    return { props: {} };
  }
}
