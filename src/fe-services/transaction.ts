import { db, getCollection } from "@/database/firebase";
import axios from "axios";

class TransactionService {
  static async getTransactionById(id: string) {
    const data = await await axios.get("/api/data");
  }

  static async getAllTransaction() {
    const data = await (await axios.get("/api/transaction")).data.res;
    console.log(data);
    return data;
  }
  static async addTransaction(payload: any) {
    const res = await axios.post("/api/transaction", payload);
    console.log(res);
    return {
      status: res.status,
      // message : res.data, // gatau kenapa datanya ngga ke send
      message: res.data,
    };
  }

  static async addTransactionWithSelectedAgen(payload: any) {
    const res = await axios.post("/api/transactionwithselectedagen", payload);
    console.log(res);
    return {
      status: res.status,
      // message : res.data, // gatau kenapa datanya ngga ke send
      message: res.data,
    };
  }

  static async getTransactionbyId(url: string, idTransaction: string) {
    const data = await (
      await axios.get(url, {
        params: {
          id: idTransaction,
        },
      })
    ).data;
    console.log(data);
    return data;
  }

  //   static async deleteTransaction(payload: any) {
  //       const res = await axios.delete("http://localhost:3000/api/transaction")
  //   }
}

export default TransactionService;
