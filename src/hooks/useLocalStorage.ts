import React, { useEffect, useState } from "react";

const PREFIX = "titipbelanja-";
const ISSERVER = typeof window === "undefined";

function useLocalStorage<T = any>(
  name: string,
  initialValue: T
): [T, React.Dispatch<React.SetStateAction<T>>] {
  const [state, setState] = useState(() => {
    const prefixedName = PREFIX + name;
    if (!ISSERVER) {
      const item = localStorage.getItem(prefixedName);
      if (item) {
        return JSON.parse(item);
      }
      if (typeof initialValue == "function") {
        return initialValue();
      }
    }
    return initialValue;
  });

  useEffect(() => {
    const prefixedName = PREFIX + name;
    localStorage.setItem(prefixedName, JSON.stringify(state));
  }, [state]);

  return [state, setState];
}

export default useLocalStorage;
