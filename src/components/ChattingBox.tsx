import { useSocket } from "@/contexts/SocketProvider";
import useLocalStorage from "@/hooks/useLocalStorage";
import { createSocket } from "@/util/socket";
import { SocketMessage } from "@/util/Socket/SocketMessage";
import React, { useCallback, useEffect, useState } from "react";
import { useRef } from "react";
import {
  InputGroup,
  Card,
  FormControl,
  Button,
  Container,
} from "react-bootstrap";
import { v4 } from "uuid";

function ChattingBox({
  sender,
  receiver,
  node_env,
  sessionId,
}: {
  node_env: "production" | "development" | "test";
  sender: MessageUserDetail;
  receiver: MessageUserDetail;
  sessionId: string;
}) {
  const socket = useSocket();
  const [messages, setMessages] = useState<Message[]>([]);
  const [text, setText] = useState<string>("");
  const setRef = useCallback((node) => {
    if (node) {
      node.scrollIntoView({ smooth: true });
    }
  }, []);

  const addMessage = useCallback(
    ({ text, from, to }) => {
      console.log(`adding ${text} to the convo`);
      setMessages((prevMessages) => [...prevMessages, { text, from, to }]);
    },
    [setMessages]
  );

  function sendMessage() {
    console.log(`sending text ${text} from ${sender.id} to ${receiver.id}`);
    socket.emit("private message", {
      text,
      from: sender,
      to: receiver,
    });
    setText("");
    addMessage({ text, from: sender, to: receiver });
  }

  useEffect(() => {
    socket.auth = {
      ...sender,
      sessionId,
    };
    socket.connect();

    socket.on("private message", addMessage);

    return () => {
      socket.disconnect();
    };
  }, [socket, addMessage]);

  return (
    <Container>
      <Card className="w-100">
        <Card.Header>
          <h3>Chat</h3>
        </Card.Header>
        <Card.Body>
          <div
            className="d-flex flex-column overflow-auto"
            style={{
              maxHeight: "500px",
            }}
          >
            {messages.map((message, idx) => {
              const lastMessage = messages.length - 1 === idx;
              return (
                <Chat
                  message={message}
                  key={v4()}
                  fromSelf={message.from.id == sender.id}
                  cref={lastMessage ? setRef : null}
                />
              );
            })}
          </div>
        </Card.Body>
        <InputGroup className="card-footer">
          <FormControl
            placeholder="Add text here"
            value={text}
            onChange={(e) => {
              setText(e.target.value);
            }}
            onKeyDown={(e: React.KeyboardEvent<HTMLInputElement>) => {
              if (e.key == "Enter") {
                sendMessage();
              }
            }}
          />
          <InputGroup.Append>
            <Button
              id="basic-addon2"
              onClick={() => {
                sendMessage();
              }}
            >
              Send
            </Button>
          </InputGroup.Append>
        </InputGroup>
      </Card>
    </Container>
  );
}

function Chat({
  message,
  fromSelf,
  cref,
}: {
  message: Message;
  fromSelf: boolean;
  cref: any;
}) {
  return (
    <Card
      className={`rounded my-2 ${
        fromSelf ? "bg-secondary align-self-end" : "bg-white float-left"
      }`}
      ref={cref}
    >
      <Card.Body>
        <small>{fromSelf ? "You" : message.from.name}</small>
        <p>{message.text}</p>
      </Card.Body>
    </Card>
  );
}

export default ChattingBox;
