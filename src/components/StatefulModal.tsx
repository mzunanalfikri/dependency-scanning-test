import React, { Fragment } from "react";
import { Button, Modal } from "react-bootstrap";

function StatefulModal({
  showModal,
  requestState,
  onAcceptModal,
  onRejectModal,
  onCloseModal,
  message,
  successMessage,
  errorMessage,
  loadingMessage,
}: ModalProps & RequestStateProps & MessageFul) {
  return (
    <Modal backdrop="static" show={showModal} centered>
      <Modal.Header>
        <Modal.Title>Konfirmasi</Modal.Title>
      </Modal.Header>

      <Modal.Body>
        {requestState === "none" ? (
          <p
            dangerouslySetInnerHTML={{
              __html: message || "Apakah anda yakin?",
            }}
          ></p>
        ) : requestState === "loading" ? (
          <p>{loadingMessage || "Loading..."}</p>
        ) : requestState === "success" ? (
          <p>{successMessage || "Operasi Sukses!"}</p>
        ) : (
          <p>
            {errorMessage ||
              "Maaf telah terjadi kesalahan, silahkan coba lagi. Bila ini berlanjut, hubungi tim db admin"}
          </p>
        )}
      </Modal.Body>

      <Modal.Footer>
        {requestState === "none" ? (
          <Fragment>
            <Button variant="primary" className="mr-1" onClick={onAcceptModal}>
              Ya
            </Button>
            <Button
              variant="danger"
              onClick={(val) => {
                if (onRejectModal) onRejectModal(val);
                if (onCloseModal) onCloseModal(val);
              }}
            >
              Tidak
            </Button>
          </Fragment>
        ) : (
          ""
        )}
      </Modal.Footer>
    </Modal>
  );
}

export default StatefulModal;
