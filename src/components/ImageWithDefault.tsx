import React, { useState } from "react";

interface ImageWithDefault {
  src?: string;
  srcAlternative: string;
  alt?: string;
  width?: string | number;
  className?: string;
}

function ImageWithDefault({
  src,
  srcAlternative,
  className,
  alt = "Sebuah Gambar",
  width = "100px",
}: ImageWithDefault) {
  const [newSrc, setNewSrc] = useState(src || srcAlternative);

  function onImgError() {
    setNewSrc(srcAlternative);
  }

  return (
    <img
      width={typeof width == "number" ? `${width}px` : width}
      style = {{borderRadius: '15px'}}
      src={newSrc}
      alt={alt}
      onError={onImgError}
      className={className}
    ></img>
  );
}

export default ImageWithDefault;
