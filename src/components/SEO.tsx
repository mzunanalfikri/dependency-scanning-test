import React from "react";
import Head from "next/head";

function SEO({ title, description, keywords }: SEOProps) {
  return (
    <Head>
      <meta
        name="viewport"
        content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"
      />
      <meta httpEquiv="Content-Type" content="text/html; charset=utf-8" />
      <title>{title} | TitipinAja!</title>
      <meta
        name="description"
        content={description || `${title} - titipin aja`}
      />
      <meta name="keywords" content={keywords || "Belanja, Titip, Murah"} />
      <link rel="icon" href="/logo_tas.svg" />
    </Head>
  );
}

export default SEO;
