import React from "react";
import styled from "styled-components";

const LoadingElmt = styled.div`
  position: fixed;
  background-color: rgba(255, 255, 255, 0.7);
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 10;
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 100px;
  color: black;
`;

function LoadingOverlay() {
  return <LoadingElmt>Loading...</LoadingElmt>;
}

export default LoadingOverlay;
