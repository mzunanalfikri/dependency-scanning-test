import PasarModel from "@/models/Pasar";
import React, { useEffect, useState } from "react";
import { Card } from "react-bootstrap";
import ImageWithDefault from "../ImageWithDefault";

interface PasarProps extends PasarModel {}

function Pasar({ name, image, address }: PasarProps) {
  return (
    <Card className="text-center w-100">
      <Card.Body>
        <ImageWithDefault
          src={image}
          srcAlternative={"/landing_page/market.svg"}
          alt={`Gambar pasar ${name}`}
          width={100}
        />
        <Card.Title className="mt-3 font-weight-bold">Pasar {name}</Card.Title>
        <Card.Text>
          <i className="fas fa-map-marker-alt"></i> {address}
        </Card.Text>
      </Card.Body>
    </Card>
  );
}

export default Pasar;
