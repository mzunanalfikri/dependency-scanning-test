import BarangModel from "@/models/Barang";
import React from "react";
import { Card, Container, Row, Button, Col } from "react-bootstrap";
import styles from "./Barang.module.scss";

function Barang({
  name,
  price,
  image,
  onDelete,
  onEdit,
  category,
}: BarangModel & Deletable & Editable) {
  const priceFormatter = new Intl.NumberFormat(["ban", "id"]);
  return (
    <Card className={`w-100 ${styles.barang}`}>
      <Card.Body>
        <img
          src={image}
          alt="gambar barang"
          className={`my-3 img-fluid ${styles.barangImg}`}
        ></img>
        <h3 className="font-weight-bold">{name}</h3>
        <p>Rp {priceFormatter.format(price)}</p>
        <p>Kategori : {category}</p>
        <Container fluid>
          <Row>
            <Col xs={6} className="m-0 p-0 pr-1">
              <Button
                variant="danger"
                className="w-100 font-weight-bold"
                onClick={onDelete}
              >
                <i className="fas fa-trash lead"></i> <br />
                Hapus
              </Button>
            </Col>
            <Col xs={6} className="m-0 p-0 pl-1">
              <Button
                variant="warning"
                className="w-100  font-weight-bold"
                onClick={onEdit}
              >
                <i className="fas fa-edit lead"></i> <br />
                Edit
              </Button>
            </Col>
          </Row>
        </Container>
      </Card.Body>
    </Card>
  );
}

export default Barang;
