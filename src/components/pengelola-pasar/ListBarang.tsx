import BarangModel from "@/models/Barang";
import React from "react";
import { Col, Form, Row } from "react-bootstrap";
import { v4 } from "uuid";
import Barang from "./Barang";

function ListBarang({
  items,
  onShowModal,
}: ModalProps & { items: BarangModel[] }) {
  return (
    <section id="barang">
      <Form.Control
        type="text"
        name="name-search"
        id="search-item"
        placeholder="Cari barang..."
        className="w-100"
      />
      <Form.Control
        as="select"
        name="category-filter"
        id="category-filter"
        placeholder="Filter kategori..."
        custom
      >
        <option value="sayuran">Sayuran</option>
        <option value="buah">Buah</option>
        <option value="daging">Daging</option>
      </Form.Control>
      <Row className="mx-auto mt-4">
        {items.map((item) => {
          const id = v4();
          return (
            <Col
              xs={12}
              md={6}
              lg={4}
              className="text-center mb-4 px-0 px-md-3"
              key={id}
            >
              <Barang
                image={item.image}
                name={item.name}
                price={item.price}
                onDelete={() => {
                  if (onShowModal)
                    onShowModal({
                      name: item.name,
                      id: item.id,
                    });
                }}
                onEdit={() => {
                  alert("editing...");
                }}
                category={["bahan makanan"]}
                id={id}
                key={id}
              />
            </Col>
          );
        })}
      </Row>
    </section>
  );
}

export default ListBarang;
