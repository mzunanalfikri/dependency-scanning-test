import firebase from "firebase";
import "firebase/storage";

var firebaseConfig = {
  apiKey: "AIzaSyD-_dLqfSOhctMiMc0uDt7Mqj6uTH-ec0Q",
  authDomain: "titip-belanja.firebaseapp.com",
  projectId: "titip-belanja",
  storageBucket: "titip-belanja.appspot.com",
  messagingSenderId: "394859598578",
  appId: "1:394859598578:web:721b51cfe10c63614cf66b",
  measurementId: "G-D3KQYZC1XP",
};

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
} else {
  firebase.app();
}

const storage = firebase.storage().ref();
const db = firebase.firestore();
const auth = firebase.auth();

function getCollection(collectionName: DbCollectionName) {
  return db.collection(collectionName);
}

export { auth, db, storage, getCollection };
export default firebase;
