import admin, { ServiceAccount } from "firebase-admin";
import serviceAccountCredential from "./serviceAccountKey.json";
const firebase = require("firebase").default;
var firebaseConfig = {
  apiKey: "AIzaSyD-_dLqfSOhctMiMc0uDt7Mqj6uTH-ec0Q",
  authDomain: "titip-belanja.firebaseapp.com",
  projectId: "titip-belanja",
  storageBucket: "titip-belanja.appspot.com",
  messagingSenderId: "394859598578",
  appId: "1:394859598578:web:721b51cfe10c63614cf66b",
  measurementId: "G-D3KQYZC1XP",
};

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
} else {
  firebase.app(); // if already initialized, use that one
}

if (!admin.apps.length) {
  admin.initializeApp({
    credential: admin.credential.cert({
      projectId: serviceAccountCredential.project_id,
      privateKey: serviceAccountCredential.private_key,
      clientEmail: serviceAccountCredential.client_email,
    }),
  });
}

async function verifyIdToken(token: string) {
  return admin
    .auth()
    .verifyIdToken(token)
    .catch((err) => {
      throw err;
    });
}
const db = admin.firestore();
const auth = admin.auth();

export default admin;
export { verifyIdToken, auth, db };
