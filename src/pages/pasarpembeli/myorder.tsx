import Layout from "@/layouts/pembeli/PembeliLayout";
import React, { useEffect, useState } from "react";
import TransactionService from "@/fe-services/transaction";
import { db } from "@/database/firebase";
import axios from "axios";
import nookies from "nookies";
import Link from "next/link";
import LoadingOverlay from "@/components/LoadingOverlay";


const Pagination = ({ postsPerPage , totalPosts , paginate} : {postsPerPage : number, totalPosts : number , paginate: any}) => {
  const pageNumbers = []

  for(let i = 1 ; i<= Math.ceil(totalPosts / postsPerPage); i++) {
    pageNumbers.push(i)
  }

  return (
    <div>
      <ul className = "pagination">
        {pageNumbers.map(number => (
          <li key = {number} className = "page-item">
            <a onClick = {() => paginate(number)} className = "page-link">
              {number}
            </a>
          </li>
        ))}
        
      </ul>
    </div>
  )

}

const Transaction = ({transaction,loading}: {transaction: Array<Object>, loading: boolean}) => {
  if(loading) {
    return(
        <div className="container mt-3">
          <div className="d-flex justify-content-center" style = {{position: 'absolute',left: '50%',top: '50%', transform: 'translate(-50%,-50%)'}}>
              <div className="spinner-border" role="status">
                  <span className="sr-only">Loading...</span>
              </div>
              <h3 className="text-center ml-3">Loading Pesanan Kamu...</h3>
          </div>
      </div>
    )
  }
  else {
    return (
      <div>
      <h2 className="ml-3 mt-4">Pesanan Anda</h2>
      {transaction.length >= 1 ? (
        transaction.map((transaction: any) => {
          return (
            <Link href={`transaction/` + transaction.id}>
              <div className="d-flex justify-content-start flex-column bd-highlight mb-3 bg-light shadow rounded">
                <div className="d-flex flex-row bd-highlight mb-1">
                  <div className="p-2 flex-fill align-self-center bd-highlight">
                    <div className="embed-responsive embed-responsive-4by3">
                      <img
                        src={transaction.pasar.image}
                        style={{ borderRadius: "15px" }}
                        className="embed-responsive-item img-thumbnail"
                      />
                    </div>
                  </div>

                  <div className="p-4 flex-fill align-self-center bd-highlight">
                    <div className="bd-highlight" style = {{fontSize: '18px'}}>
                      <b>Pasar {transaction.pasar.name} </b>
                    </div>

                    {transaction.arrBarang.map((barang: any) => {
                      return (
                        <div className="bd-highlight d-inline">
                          {barang.qty} {barang.name}
                          {transaction.arrBarang.length > 1 ? "," : ""}
                        </div>
                      );
                    })}
                    <br></br>
                    <p
                      className="bd-highlight text-muted font-italic"
                      style={{ fontSize: "12px" }}
                    >
                      {new Date(
                        transaction.date._seconds * 1000
                      ).toDateString() +
                        " at " +
                        new Date(
                          transaction.date._seconds * 1000
                        ).toLocaleTimeString()}
                    </p>
                  </div>
                </div>
                <div
                  className="d-flex flex-row bd-highlight bg-dark text-light rounded-bottom font-weight-bold"
                  style={{ borderTop: "1px solid gray" }}
                >
                  <div className="p-2 bd-highlight">
                    {" "}
                    {transaction.totalQty} barang{" "}
                  </div>
                  <div className="p-2 bd-highlight">
                    Total Harga: <br></br>
                    {transaction.totalHarga}
                  </div>
                </div>
              </div>
            </Link>
          );
        })
      ) : (
        <div className="alert alert-info" role="alert">
          Tidak ada pesanan terbaru.
        </div>
      )}
    </div>
    )
  }
}

function MyOrder() {
  const [transaction, setTransaction] = useState([]);
  const [isLoaded, setIsLoaded] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage] = useState(2);

  const indexOfLastPost = currentPage * postsPerPage
  const indexOfFirstPost = indexOfLastPost - postsPerPage
  const currentTransaction = transaction.slice(indexOfFirstPost, indexOfLastPost)


  const paginate = (pageNumber: number) => {
    setCurrentPage(pageNumber)
  }


  useEffect(() => {
    const url = "../api/auth/detail-user";

    const allbarang: any = [];
    TransactionService.getAllTransaction().then(async (transactiondata) => {
      const result = await axios.get(url);
      const getResult = await result.data;
      for (var idx in transactiondata) {
        if (
          transactiondata[idx].status == "inprocess" &&
          transactiondata[idx].pembeliId == getResult.uid
        ) {
          var totalitem: number = 0;
          const transactionReference: any = await db
            .collection("transaction")
            .doc(transactiondata[idx].id)
            .get();
          const pasarReference: any = await transactionReference
            .data()
            .idPasar.get();
          transactiondata[idx].pasar = pasarReference.data();
          for (var idxbarang in transactiondata[idx].arrBarang) {
            totalitem += transactiondata[idx].arrBarang[idxbarang].qty;
          }
          transactiondata[idx].totalQty = totalitem;
          allbarang.push(transactiondata[idx]);
        }
      }
      console.log(allbarang);
      allbarang.sort(function (a: any, b: any) {
        // Turn your strings into dates, and then subtract them
        // to get a value that is either negative, positive, or zero.
        return (
          new Date(b.date._seconds * 1000).valueOf() -
          new Date(a.date._seconds * 1000).valueOf()
        );
      });
      setTransaction(allbarang);
      setIsLoaded(true);
    });
  }, []);

    return (
      <Layout title="Pesanan Saya" className="bg-secondary">
        <div className="container">
        <Transaction transaction = {currentTransaction} loading = {!isLoaded}/>
        <Pagination postsPerPage = {postsPerPage} totalPosts = {transaction.length} paginate = {paginate}/>
        </div>
      </Layout>
    );
}

export default MyOrder;
