import PasarFeService from "@/fe-services/pasar";
import Layout from "@/layouts/pembeli/PembeliLayout";
import Item, { ItemWithQty } from "@/models/Barang";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { Container, Button } from "react-bootstrap";
import { parse, v4 } from "uuid";
import Link from "next/link";
import Pasar, { GeoPoint } from "@/models/Pasar";

const currPosition: GeoPoint = {
  _latitude: 0,
  _longitude: 0,
};

function DetailBarang() {
  const router = useRouter();
  const [totalHargaBarang, setTotalHargaBarang] = useState(0);
  const [arraybarang, setArraybarang] = useState<ItemWithQty[]>([]);
  const [currentbarang, setCurrentBarang] = useState<ItemWithQty[]>([]);
  const [idpasar, setIdPasar] = useState("");

  useEffect(() => {
    setIdPasar(window.location.pathname.split("/")[2]);
    const items = localStorage.getItem("ARRAY_BARANG");
    console.log(items);
    const currentbarang: any = [];
    console.log(JSON.parse(items as string));
    for (var idx in JSON.parse(items as string)) {
      if (
        JSON.parse(items as string)[idx].id ==
        window.location.pathname.split("/")[3]
      ) {
        currentbarang.push(JSON.parse(items as string)[idx]);
      }
    }
    console.log(currentbarang);
    if (!items) return;
    setArraybarang(JSON.parse(items));
    setCurrentBarang(currentbarang);
  }, []);

  useEffect(() => {});

  useEffect(() => {
    setTotalHargaBarang(
      arraybarang.reduce((acc, curr) => acc + curr.qty * curr.price, 0)
    );
    localStorage.setItem(
      "TOTAL_ITEM",
      String(arraybarang.reduce((acc, curr) => acc + curr.qty, 0))
    );
    localStorage.setItem("ARRAY_BARANG", JSON.stringify(arraybarang));
  }, [arraybarang]);

  useEffect(
    () => localStorage.setItem("TOTAL_PRICE", String(totalHargaBarang)),
    [totalHargaBarang]
  );

  function goBack() {
    router.back();
  }

  function getPasarId() {
    return router.query.pasarId as string;
  }

  function changeBarangQty(barangId: string, qty: number) {
    setArraybarang(
      arraybarang.map((barang) => {
        if (barang.id == barangId) {
          return {
            ...barang,
            qty,
          };
        }
        return barang;
      })
    );
  }

  function deleteItem(itemId: string) {
    setArraybarang(arraybarang.filter((barang) => barang.id != itemId));
  }

  return (
    <Layout title="DetailBarang" className="bg-secondary py-3">
      <Container>
        <button
          className="bg-transparent border-0"
          style={{ marginBottom: "3%" }}
        >
          <i className="fas fa-arrow-left fa-4x" onClick={() => goBack()}></i>
        </button>
        <div>
          {currentbarang.map((barang) => (
            <CheckoutItem
              onChangeQty={changeBarangQty}
              item={barang}
              onDeleteItem={deleteItem}
            />
          ))}
        </div>
      </Container>
    </Layout>
  );
}

interface CheckoutItemProps {
  onChangeQty: Function;
  item: ItemWithQty;
  onDeleteItem: Function;
}

function CheckoutItem({ item, onChangeQty, onDeleteItem }: CheckoutItemProps) {
  const [currAmount, setCurrAmount] = useState(item.qty);
  const router = useRouter();
  const [detailpasar, setDetailPasar] = useState<Pasar & { items: Item[] }>({
    items: [],
    id: router.query.pasarId as string,
    name: "",
    city: "",
    address: "",
    geopoint: currPosition,
  });

  function onAddItem() {
    console.log(currAmount);
    setCurrAmount(currAmount + 1);
  }

  function onDecreaseItem() {
    setCurrAmount(currAmount - 1);
  }

  async function fetchPasarAndItem(pasarid: string) {
    await PasarFeService.getPasar(pasarid).then((data) => {
      setDetailPasar(data);
    });
  }

  useEffect(() => {
    onChangeQty(item.id, currAmount);
  }, [currAmount]);

  useEffect(() => {
    fetchPasarAndItem(window.location.pathname.split("/")[2]);
  }, []);

  return (
    <div id={"detailBarang/" + `${item.id}`} className="row">
      <div className="col-20 col-sm-6 col-md-6 col-lg-6 col-xl-6">
        <div
          className="d-block p-2 bg-light text-white"
          style={{ borderRadius: "25px" }}
        >
          <img
            src={item.image}
            className="img-fluid"
            style={{ borderRadius: "25px" }}
          ></img>
        </div>
      </div>
      <div className="col-14 col-sm-6 col-md-6 col-lg-6 col-xl-6">
        <h1 className="bold m-0 p-0">{item.name}</h1>
        <p className="text-muted" style={{ textTransform: "capitalize" }}>
          {item.category}
        </p>
        <p>
          <b>Rp {item.price}</b>
        </p>
        <p> {item.description}</p>
        <p>
          {" "}
          Dibeli dari: <br></br> <b>Pasar {detailpasar.name}</b>
        </p>
        <p className="border-bottom" style={{ marginTop: "5%" }}></p>
        <br></br>
        <section>
          <div className="row">
            <div className="col-5 col-sm-5">
              <button
                id={"buttonDecrease/" + `${item.id}`}
                type="button"
                className="btn btn-primary btn-sm btn-block"
                disabled={currAmount == 0 ? true : false}
                onClick={() => onDecreaseItem()}
              >
                -
              </button>
            </div>
            <br></br>
            <br></br>
            <div className="col-2 col-sm-2" style={{ textAlign: "center" }}>
              <p>{currAmount}</p>
            </div>
            <div className="col-5 col-sm-5">
              <button
                id="buttonIncrease"
                type="button"
                className="btn btn-primary btn-sm btn-block"
                onClick={() => onAddItem()}
              >
                +
              </button>
            </div>
            <br></br>
            <br></br>
            <div className="col-sm-12">
              <Link
                href={`/pasarpembeli/${
                  window.location.pathname.split("/")[2]
                }/checkout`}
              >
                <button className="btn btn-primary btn-sm btn-block">
                  Keranjang
                </button>
              </Link>
            </div>
          </div>
        </section>
      </div>
    </div>
  );
}

export default DetailBarang;
