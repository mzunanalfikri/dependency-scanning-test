import PasarItemFeService from "@/fe-services/pasaritem";
import Layout from "@/layouts/pembeli/PembeliLayout";
import Item, { ItemWithQty } from "@/models/Barang";
import styles from "@/pages/styles/Checkout.module.scss";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { Container, Button, Modal } from "react-bootstrap";
import { v4 } from "uuid";
import RedirectClientHelper from "@/fe-services/redirect/RedirectClientHelper";
import redirectIfRoleDoesNotMatch from "@/fe-services/serverside/redirectIfRoleDoesNotMatch";
import { NextPageContext } from "next";
import TransactionService from "@/fe-services/transaction";
import Link from "next/link";
import "firebase/firestore";
import router from "next/router";
import axios from "axios";

const redirect = (url: string) => {
  router.push(url);
};

function Checkout({ redirectionInstruction }: WithRedirectionInstruction) {
  useEffect(() => {
    RedirectClientHelper(redirectionInstruction);
  }, []);

  const router = useRouter();
  const [totalHargaBarang, setTotalHargaBarang] = useState(0);
  const [deliveryPrice, setDeliveryPrice] = useState(4000);
  const [arraybarang, setArraybarang] = useState<ItemWithQty[]>([]);
  const [arraybarangfiltered, setArraybarangFiltered] = useState<ItemWithQty[]>(
    []
  );
  const [idpasar, setIdPasar] = useState("");
  const [userLocation, setUserLocation] = useState({});

  function filterCheckout(items: any) {
    const filteritems = [];
    console.log(JSON.parse(items as string));
    for (var idx in JSON.parse(items as string)) {
      if (JSON.parse(items as string)[idx].qty != 0) {
        filteritems.push(JSON.parse(items as string)[idx]);
      }
    }
    return filteritems;
  }

  useEffect(() => {
    setUserLocation({
      latitude: JSON.parse(localStorage.getItem("USER_LATITUDE") as string),
      longitude: JSON.parse(localStorage.getItem("USER_LONGITUDE") as string),
    });
    var pasarid = window.location.pathname.split("/")[2];
    setIdPasar(pasarid);
    console.log(pasarid);
    const items = localStorage.getItem("ARRAY_BARANG");
    const filteritems = filterCheckout(items);
    if (!items) return;
    setArraybarang(JSON.parse(items));
    setArraybarangFiltered(filteritems);
  }, []);

  useEffect(() => {
    setTotalHargaBarang(
      arraybarang.reduce((acc, curr) => acc + curr.qty * curr.price, 0)
    );
    localStorage.setItem(
      "TOTAL_ITEM",
      String(arraybarang.reduce((acc, curr) => acc + curr.qty, 0))
    );
    localStorage.setItem("ARRAY_BARANG", JSON.stringify(arraybarang));
    const filteredCheckout = filterCheckout(
      localStorage.getItem("ARRAY_BARANG") as string
    );
    setArraybarangFiltered(filteredCheckout);
  }, [arraybarang]);

  useEffect(
    () => localStorage.setItem("TOTAL_PRICE", String(totalHargaBarang)),
    [totalHargaBarang]
  );

  function goBack() {
    router.back();
  }

  function getPasarId() {
    return router.query.pasarId as string;
  }

  function changeBarangQty(barangId: string, qty: number) {
    setArraybarang(
      arraybarang.map((barang) => {
        if (barang.id == barangId) {
          return {
            ...barang,
            qty,
          };
        }
        return barang;
      })
    );
  }

  function deleteItem(itemId: string) {
    setArraybarang(arraybarang.filter((barang) => barang.id != itemId));
  }

  return (
    <Layout title="Checkout" className="bg-secondary py-3">
      <Container>
        <button className="bg-transparent border-0">
          <i className="fas fa-arrow-left fa-4x" onClick={() => goBack()}></i>
        </button>
        <div>
          <div className="d-flex justify-content-between align-items-center mb-3">
            <h1 className="m-0">Keranjang</h1>

            <a href={`/pasarpembeli/${router.query.pasarId as string}/detail`}>
              <button type="button" className="btn btn-success btn-md">
                + Tambah Barang
              </button>
            </a>
          </div>

          {arraybarang.map((barang) => (
            <CheckoutItem
              onChangeQty={changeBarangQty}
              item={barang}
              onDeleteItem={deleteItem}
            />
          ))}
          <div className="bg-white p-4 rounded shadow mt-4">
            <h2 style={{ borderBottom: "1px solid gray" }}>Rincian Belanja</h2>
            <div className="d-flex justify-content-between mt-2">
              <p>Total Harga:</p>
              <div className="mr-lg-4">
                Rp {totalHargaBarang.toLocaleString()}
              </div>
            </div>
            <div className="d-flex justify-content-between mt-2 border-bottom border-dark">
              <p>Total Biaya Pengiriman:</p>
              <div className="mr-lg-4">Rp {deliveryPrice.toLocaleString()}</div>
            </div>
            <div className="d-flex justify-content-between mt-4">
              <div>Total Pembayaran: </div>
              <div className="mr-lg-4">
                <strong>
                  Rp {(deliveryPrice + totalHargaBarang).toLocaleString()}
                </strong>
              </div>
            </div>
            <br />
            <div className={styles.attention}>
              <p className="text-danger">*Peringatan:</p>
              <p style={{ width: "75%" }}>
                Harga diatas adalah harga perkiraan. Harga dapat berubah apabila
                terjadi perubahan harga dari pasar yang bersangkutan,tolong
                hubungi driver untuk informasi harga yang sudah fix.
              </p>
            </div>
          </div>
        </div>
      </Container>
      <CartCheckoutBottom
        totalHarga={totalHargaBarang + deliveryPrice}
        barang={arraybarangfiltered}
        pasarid={idpasar}
        location={userLocation}
      />
    </Layout>
  );
}

interface CheckoutItemProps {
  onChangeQty: Function;
  item: ItemWithQty;
  onDeleteItem: Function;
}

function CheckoutItem({ item, onChangeQty, onDeleteItem }: CheckoutItemProps) {
  const [currAmount, setCurrAmount] = useState(item.qty);
  function onAddItem() {
    setCurrAmount(currAmount + 1);
  }

  function onDecreaseItem() {
    setCurrAmount(currAmount - 1);
  }

  useEffect(() => {
    onChangeQty(item.id, currAmount);
  }, [currAmount]);

  return (
    <div
      id={"detailBarang/" + `${item.id}`}
      className={`${
        currAmount == 0 ? styles.cartitemhide : styles.cartitem
      } d-flex p-4`}
    >
      <img src={item.image} className={styles.gambarbarang}></img>
      <div className="d-flex flex-column justify-content-between w-100 px-3">
        <div className="d-flex align-items-center justify-content-between">
          <h3 className="bold m-0 p-0">{item.name}</h3>
          <Button variant="danger">
            <i
              className="far fa-trash-alt"
              onClick={() => onDeleteItem(item.id)}
            ></i>
          </Button>
        </div>
        <div className="d-flex justify-content-between align-items-center mt-2">
          <div>Rp {item.price} / kg</div>
          <div className="d-flex align-items-center">
            <div className={styles.buttonminus}>
              <button
                id={"buttonDecrease/" + `${item.id}`}
                type="button"
                className="btn btn-success"
                disabled={currAmount == 0 ? true : false}
                onClick={() => onDecreaseItem()}
              >
                -
              </button>
            </div>
            <div id={v4()} className="mx-2">
              <strong>{currAmount}</strong>
            </div>
            <div className={styles.buttonplus}>
              <button
                id="buttonIncrease"
                type="button"
                className="btn btn-success"
                onClick={() => onAddItem()}
              >
                +
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

function CartCheckoutBottom({
  totalHarga,
  barang,
  pasarid,
  location,
}: {
  totalHarga: number;
  barang: {};
  pasarid: string;
  location: {};
}) {
  const [availableAgen, setAvailableAgen] = useState([]);

  const [showCheckoutModal, setCheckoutModal] = useState(false);
  const handleCloseCheckout = () => setCheckoutModal(false);
  const handleShowCheckout = () => setCheckoutModal(true);

  const [showCongratsModal, setCongratsModal] = useState(false);
  const handleCloseCongrats = () => setCongratsModal(false);
  const handleShowCongrats = () => setCongratsModal(true);

  const [showConfirmationModal, setConfirmationModal] = useState(false);
  const handleCloseConfirm = () => setConfirmationModal(false);
  const handleShowConfirm = () => setConfirmationModal(true);

  const [showConfirmRandomModal, setConfirmRandomModal] = useState(false);
  const handleCloseConfirmRandom = () => setConfirmRandomModal(false);
  const handleShowConfirmRandom = () => setConfirmRandomModal(true);

  const [showOptionModal, setOptionModal] = useState(false);
  const handleShowOption = () => setOptionModal(true);
  const handleCloseOption = () => {
    setOptionModal(false);
    setCheckoutModal(false);
  };

  const [checkedItems, setCheckedItems] = useState<any>({});

  const handleChangeChecbox = (event: any) => {
    setCheckedItems({
      ...checkedItems,
      [event.target.name]: event.target.checked,
    });
  };

  function CheckoutModal() {
    return (
      <Modal
        size="lg"
        show={showCheckoutModal}
        onHide={handleCloseCheckout}
        aria-labelledby="contained-modal-title-vcenter"
        backdrop="static"
        keyboard={false}
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title>Konfirmasi Pesanan</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          Anda telah menyelesaikan proses pemesanan dan akan dilanjutkan ke
          pengantaran. Pastikan barang yang dipesan sudah sesuai sebelum
          melanjutkan ke tahap berikutnya.
        </Modal.Body>
        <Modal.Footer>
          <Button variant="primary" onClick={handleCloseCheckout}>
            Batal
          </Button>
          <Button variant="primary" onClick={proceedOrder}>
            Lanjutkan
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }

  function OptionModal() {
    return (
      <Modal
        size="lg"
        show={showOptionModal}
        onHide={handleCloseOption}
        aria-labelledby="contained-modal-title-vcenter"
        keyboard={false}
        backdrop="static"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title>Pilih Agen</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>
            Anda memiliki opsi untuk memilih agen sesuai preferensi pribadi
            ataupun secara acak.
          </p>
          <div id="text-1">
            <p>Berikut adalah agen yang tersedia:</p>
          </div>
          <form>
            {availableAgen.length >= 1 ? (
              availableAgen &&
              availableAgen.map((agenn: any) => {
                return (
                  <div className="p-2">
                    <div className="d-inline p-4">
                      <input
                        type="checkbox"
                        name={agenn.uid}
                        value={agenn.uid}
                        checked={checkedItems[agenn.uid]}
                        onChange={handleChangeChecbox}
                      />
                    </div>
                    <label>{agenn.displayName}</label>
                  </div>
                );
              })
            ) : (
              <p className="text-danger">
                {" "}
                Tidak ada agen yang tersedia untuk saat ini. Coba pesan beberapa
                saat lagi.
              </p>
            )}
          </form>
          {availableAgen.length >= 1 ? (
            <p
              className="text-warning font-weight-bold"
              style={{ fontSize: "14px" }}
            >
              Pastikan agen yang Anda pilih sudah benar sebelum melanjutkan.
            </p>
          ) : (
            ""
          )}
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="primary"
            onClick={handleSelectRandom}
            className="mr-auto"
            disabled={availableAgen.length == 0}
          >
            Acak Agen
          </Button>
          <Button
            variant="primary"
            onClick={handleConfirmation}
            disabled={availableAgen.length == 0}
          >
            Pesan
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }

  function ConfirmationModal() {
    return (
      <Modal
        size="lg"
        show={showConfirmationModal}
        onHide={handleCloseConfirm}
        aria-labelledby="contained-modal-title-vcenter"
        keyboard={false}
        backdrop="static"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title>Confirmation</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>
            Apakah Anda yakin terhadap pilihan Agen? Anda tidak dapat kembali
            setelah melanjutkan pesanan
          </p>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="primary" onClick={handleCancel} className="mr-auto">
            Kembali
          </Button>
          <Button variant="primary" onClick={proceedCheckout}>
            Pesan
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }

  function ConfirmRandomModal() {
    return (
      <Modal
        size="lg"
        show={showConfirmRandomModal}
        onHide={handleCloseConfirmRandom}
        aria-labelledby="contained-modal-title-vcenter"
        keyboard={false}
        backdrop="static"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title>Confirmation</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>
            Apakah Anda yakin akan memilih Agen secara acak? Anda tidak dapat
            kembali setelah melanjutkan pesanan
          </p>
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="primary"
            onClick={handleCancelRandom}
            className="mr-auto"
          >
            Kembali
          </Button>
          <Button variant="primary" onClick={proceedRandom}>
            Pesan
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }

  function CongratulationsModal() {
    return (
      <Modal
        size="lg"
        show={showCongratsModal}
        onHide={handleCloseCongrats}
        aria-labelledby="contained-modal-title-vcenter"
        keyboard={false}
        backdrop="static"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title>Congratulations</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>
            Pesanan Anda telah diproses dan akan diteruskan ke tahap
            selanjutnya.{" "}
          </p>
          <p>Mohon menunggu selagi kami memproses pesanan Anda.</p>
          <p>Terimakasih telah berbelanja dengan TitipinAja! </p>
        </Modal.Body>
        <Modal.Footer>
          <p className="text-muted mr-auto">Copyright 2021</p>
          <p className="text-muted">TitipinAja!</p>
        </Modal.Footer>
      </Modal>
    );
  }

  // Handle confirmation for order with agen selected by user
  // Set function to prevent user selecting more or less than 1 agen
  function handleConfirmation() {
    let countChecked: number = 0;
    const arrayItems = [];
    for (var key in checkedItems) {
      if (checkedItems[key] == true) {
        countChecked += 1;
      }
    }
    if (countChecked > 1 || countChecked == 0) {
      if (document.getElementById("warning-text") == null) {
        var tag = document.createElement("p");
        tag.id = "warning-text";
        tag.className = "font-weight-bold text-danger";
        tag.style.cssText = "font-size: 12px;";
        var text = document.createTextNode(
          "Input agen tidak ditemukan atau lebih dari 1. Coba lagi"
        );
        tag.appendChild(text);
        var element = document.getElementById("text-1");
        element!.appendChild(tag);
      }
    } else {
      handleCloseOption();
      handleShowConfirm();
    }
  }

  function handleSelectRandom() {
    handleCloseOption();
    handleShowConfirmRandom();
  }

  function handleCancel() {
    handleCloseConfirm();
    handleShowOption();
  }

  function handleCancelRandom() {
    handleCloseConfirmRandom();
    handleShowOption();
  }

  // Retrieve all available agen with each corresponding data
  function proceedOrder() {
    axios.get("/api/availableagen").then(async (res) => {
      const dataAgen: any = [];
      await axios.get("/api/auth/list-all-user").then((ress) => {
        for (var idx in ress.data) {
          if (res.data.includes(ress.data[idx].uid)) {
            dataAgen.push(ress.data[idx]);
          }
        }
      });
      console.log(dataAgen);
      setAvailableAgen(dataAgen);
    });
    setOptionModal(true);
  }

  // Handle final confirmation for order with agen selected by system(random)
  function proceedRandom() {
    handleCloseConfirmRandom();
    handleShowCongrats();
    TransactionService.addTransaction({
      userLocation: location,
      arrBarang: barang,
      totalHarga: totalHarga,
      idPasar: pasarid,
    }).then(handleRedirect);
  }

  // Handle confirmation for order with agen selected by user
  function proceedCheckout() {
    const selectedAgen: any = [];
    for (var idx in Object.keys(checkedItems)) {
      if (checkedItems[Object.keys(checkedItems)[idx]] == true) {
        selectedAgen.push(Object.keys(checkedItems)[idx]);
      }
    }
    console.log(selectedAgen[0]);
    TransactionService.addTransactionWithSelectedAgen({
      userLocation: location,
      arrBarang: barang,
      totalHarga: totalHarga,
      idPasar: pasarid,
      idAgen: selectedAgen[0],
    }).then(handleRedirect);
    console.log(selectedAgen);
    handleCloseConfirm();
    handleShowCongrats();
  }

  // Handle redirection after confirming order
  function handleRedirect(res: any) {
    if (res.status === 201) {
      setTimeout(function () {
        redirect("../agennotfound");
      }, 5000);
    } else if (res.status === 200) {
      setTimeout(function () {
        var idtransaction = "../transaction/" + res.message;
        console.log(idtransaction);
        console.log(res);
        redirect(idtransaction);
      }, 5000);
      localStorage.clear();
    }
  }

  return (
    <div className={totalHarga > 0 ? styles.proceed : styles.hideproceed}>
      <div>
        <p className="mb-0">Lanjutkan</p>
        <small>Klik button di kanan</small>
      </div>
      <div>
        <div className="d-inline">
          Rp {totalHarga.toLocaleString()}
          {/* <Link href={`/pasarpembeli/${pasarid}/proceedcheckout`}> */}
          <button
            type="button"
            className="btn btn-primary btn-block"
            onClick={() => handleShowCheckout()}
          >
            Pesan
          </button>
          {/* </Link> */}
          <CheckoutModal />
          <OptionModal />
          <CongratulationsModal />
          <ConfirmationModal />
          <ConfirmRandomModal />
        </div>
      </div>
    </div>
  );
}

export default Checkout;

export async function getServerSideProps(context: NextPageContext) {
  // kalo bukan visitor, gaboleh ke rute ini
  const redirectionInstruction = await redirectIfRoleDoesNotMatch(
    context,
    "pembeli"
  );
  return {
    props: { redirectionInstruction },
  };
}
