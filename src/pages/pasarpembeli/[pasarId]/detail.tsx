import PasarFeService from "@/fe-services/pasar";
import RedirectClientHelper from "@/fe-services/redirect/RedirectClientHelper";
import redirectIfRoleDoesNotMatch from "@/fe-services/serverside/redirectIfRoleDoesNotMatch";
import Layout from "@/layouts/pembeli/PembeliLayout";
import Item, { ItemWithQty } from "@/models/Barang";
import Pasar, { GeoPoint } from "@/models/Pasar";
import styles from "@/pages/styles/DetailPasar.module.scss";
import { distanceInKmBetweenEarthCoordinates } from "@/util/geolocation";
import { NextPageContext } from "next";
import Link from "next/link";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { Button } from "react-bootstrap";
import { v4 } from "uuid";

const currPosition: GeoPoint = {
  _latitude: 0,
  _longitude: 0,
};

function detail({ redirectionInstruction }: WithRedirectionInstruction) {
  useEffect(() => {
    RedirectClientHelper(redirectionInstruction);
  }, []);
  const router = useRouter();

  const [detailpasar, setDetailPasar] = useState<Pasar & { items: Item[] }>({
    items: [],
    id: router.query.pasarId as string,
    name: "",
    city: "",
    address: "",
    geopoint: currPosition,
  });
  const [totalItemOrdered, setTotalItemOrdered] = useState(0);
  const [totalprice, setTotalPrice] = useState(0);
  const [arraybarang, setArrayBarang] = useState<ItemWithQty[]>([]);
  const [idpasar, setIdPasar] = useState("");
  const [userGeopoint, setUserGeopoint] = useState(currPosition);
  const [tempstoragee, setTemporaryStorage] = useState();

  function goBack() {
    router.back();
  }

  async function fetchPasarAndItem(pasarid: string) {
    await PasarFeService.getPasar(pasarid).then((data) => {
      setDetailPasar(data);
    });
  }

  function initStorageNotNull() {
    PasarFeService.getPasar(window.location.pathname.split("/")[2]).then(
      (data) => {
        console.log(data.items);
        const tempstorage: any = [];
        for (var idx in data.items) {
          tempstorage.push(data.items[idx]);
          tempstorage[idx].qty = 0;
        }
        console.log(tempstorage);
        localStorage.setItem("ARRAY_BARANG", JSON.stringify(tempstorage));
        location.reload();
      }
    );
  }

  function reset() {
    initStorageNotNull();
    setArrayBarang([]);
    setTotalPrice(0);
    setTotalItemOrdered(0);
    localStorage.removeItem("CURRENT_PASAR");
    localStorage.removeItem("TOTAL_ITEM");
    localStorage.removeItem("TOTAL_PRICE");
  }

  useEffect(() => {
    console.log(localStorage.getItem("ARRAY_BARANG"));
    if (localStorage.getItem("ARRAY_BARANG") != null) {
      setArrayBarang(
        JSON.parse(localStorage.getItem("ARRAY_BARANG") as string)
      );
    } else {
      initStorageNotNull();
    }
    // console.log(JSON.parse(localStorage.getItem("ARRAY_BARANG") as string)[0].qty)
    console.log(localStorage.getItem("TOTAL_ITEM"));
    console.log(localStorage.getItem("TOTAL_PRICE"));

    setTotalItemOrdered(+localStorage.getItem("TOTAL_ITEM")!);
    setTotalPrice(+localStorage.getItem("TOTAL_PRICE")!);

    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition(
        function (position) {
          var crd = position.coords;
          let userLatitude = crd.latitude;
          let userLongitude = crd.longitude;
          currPosition._latitude = userLatitude;
          currPosition._longitude = userLongitude;
          setUserGeopoint({
            _latitude: userLatitude,
            _longitude: userLongitude,
          });
          localStorage.setItem("USER_LATITUDE", String(userLatitude));
          localStorage.setItem("USER_LONGITUDE", String(userLongitude));
        },
        (err) => {}
      );
    }

    var pasarid = window.location.pathname.split("/")[2];
    setIdPasar(pasarid);

    fetchPasarAndItem(pasarid);
  }, []);

  function addItem(item: Item) {
    const idx = arraybarang.findIndex((val) => {
      return val.id == item.id;
    });
    console.log(idx);
    if (idx !== -1) {
      setArrayBarang(
        arraybarang.map((barang, barangIdx) => {
          if (barangIdx == idx) {
            const newBarang = { ...barang };
            newBarang.qty += 1;
            return newBarang;
          }
          return barang;
        })
      );
    } else {
      console.log("setting array barang...");
      setArrayBarang([...arraybarang, { ...item, qty: 1 }]);
    }
  }

  function onClickedAdd(item: Item) {
    setTotalPrice(item.price + totalprice);
    setTotalItemOrdered(totalItemOrdered + 1);
    addItem(item);
  }

  useEffect(() => {
    localStorage.setItem("ARRAY_BARANG", JSON.stringify(arraybarang));
    console.log(arraybarang);
    console.log(localStorage.getItem("ARRAY_BARANG"));
  }, [arraybarang]);

  useEffect(
    () => localStorage.setItem("TOTAL_ITEM", String(totalItemOrdered)),
    [totalItemOrdered]
  );
  useEffect(() => localStorage.setItem("TOTAL_PRICE", String(totalprice)), [
    totalprice,
  ]);

  return (
    <Layout title="Detail" className="bg-secondary">
      <a href="/pasarpembeli">
        <i
          className="fas fa-arrow-left fa-4x"
          style={{ marginLeft: "1%", marginTop: "1%", marginBottom: "2%" }}
          onClick={() => goBack()}
        ></i>{" "}
      </a>
      <div key={v4()}>
        <div className="jumbotron">
          <div className="container">
            <div className="row">
              <div className="col-sm">
                <div className="row">
                  <h1 className="display-4">Pasar {detailpasar.name}</h1>
                </div>
                <div className="row">
                  <div className="lead">
                    <i className="fas fa-map-marker-alt"></i> Jarak <br></br>
                    <p>
                      {(function () {
                        const geopoint = detailpasar.geopoint as FirebaseFirestore.GeoPoint;
                        // console.log("GEOPOINT PASAR :");
                        // console.log(geopoint);
                        return distanceInKmBetweenEarthCoordinates(
                          userGeopoint._latitude,
                          userGeopoint._longitude,
                          geopoint.latitude,
                          geopoint.longitude
                        );
                      })()}
                      km
                    </p>
                  </div>
                </div>
                <div className="row">
                  <p className="lead">
                    <i className="far fa-clock"></i> Waktu Operasi <br></br>
                    <span className={styles.distvalue}></span>
                  </p>
                </div>
                <div className="row">
                  <p className="lead">
                    <i className="far fa-address-card"></i> Alamat
                    <span>{detailpasar.address}</span>
                  </p>
                </div>
              </div>
              <div className="col-sm">
                <img
                  className="img-thumbnail"
                  src={detailpasar.image}
                  style={{ borderRadius: "25px" }}
                ></img>
              </div>
            </div>
          </div>
        </div>
        <div
          className="input-group"
          style={{
            marginTop: "2%",
            marginLeft: "5%",
            marginBottom: "2%",
          }}
        >
          <div className="form-outline">
            <input
              type="search"
              id="form1"
              placeholder="Cari sesuatu"
              className="form-control"
            />
          </div>
          <button type="button" className="btn btn-primary">
            <i className="fas fa-search"></i>
          </button>
        </div>
      </div>
      <div className="container">
        <div className="row">
          {/* <div className="card-group"> */}
          {detailpasar.items &&
            detailpasar.items.map((barangpasar) => {
              return (
                <div
                  className="col-auto col-sm-auto col-md-auto col-lg-auto col-xl-auto"
                  style={{ marginBottom: "2%" }}
                  key={v4()}
                >
                  <div
                    className="card h-100 mb-3"
                    style={{ borderRadius: "25px" }}
                  >
                    <div className="embed-responsive embed-responsive-4by3">
                      <a
                        href={
                          "/pasarpembeli/" +
                          idpasar +
                          "/" +
                          barangpasar.id +
                          "/detailbarang"
                        }
                      >
                        <img
                          src={barangpasar.image}
                          className="card-img-top embed-responsive-item"
                          style={{ borderRadius: "15px" }}
                        ></img>
                      </a>
                    </div>

                    <div className="card-body">
                      <b className="card-title">{barangpasar.name}</b>
                      <p className="card-text">Rp {barangpasar.price},00</p>

                      <button
                        className="btn btn-primary btn-sm btn-block"
                        style={{ borderRadius: "50px" }}
                        onClick={() => onClickedAdd(barangpasar)}
                      >
                        Tambah
                      </button>
                    </div>
                  </div>
                </div>
              );
            })}
        </div>
        {/* </div> */}
      </div>

      <div
        id="summaryorder"
        className={
          totalItemOrdered > 0
            ? `${styles.summaryorder}`
            : `${styles.hidesummary}`
        }
      >
        <div className="d-flex justify-content-between">
          <div>
            <i className="fas fa-shopping-cart mr-3"></i>
            <b>{totalItemOrdered}</b> barang telah dipesan
          </div>
          <div>
            Total : <b>{totalprice}</b>
          </div>
        </div>
        <div className="d-flex">
          <Link href={`/pasarpembeli/${idpasar}/checkout`}>
            <Button variant="primary">Keranjang</Button>
          </Link>
          <Button
            variant="danger"
            onClick={() => {
              reset();
            }}
            className="ml-auto"
          >
            Reset
          </Button>
        </div>
      </div>
    </Layout>
  );
}

export default detail;

export async function getServerSideProps(context: NextPageContext) {
  // kalo bukan visitor, gaboleh ke rute ini
  const redirectionInstruction = await redirectIfRoleDoesNotMatch(
    context,
    "pembeli"
  );
  return {
    props: { redirectionInstruction },
  };
}
