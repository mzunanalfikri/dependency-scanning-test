import Layout from "@/layouts/pembeli/PembeliLayout";
import React, { useEffect, useState } from "react";
import { v4 } from "uuid";

function Price() {

  function cancelOrder() {
    alert("Order canceled");
  }

  function resetStorage() {
    localStorage.clear();
  }

  return (
    <Layout title="Proceed Checkout" className="bg-secondary">
      <div key={v4()}>
        <div
          className="text-center"
          style={{
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%,-50%)",
          }}
        >
          <i
            className="fas fa-check-double fa-8x"
            style={{
              marginLeft: "1%",
              marginTop: "1%",
              marginBottom: "1%",
              color: "green",
            }}
          ></i>
          <h2>Terimakasih! Order kamu sedang diproses.</h2>
          <h6> Mohon menunggu selagi kami mencarikan kamu agen.</h6>
        </div>
      </div>
    </Layout>
  );
}

export default Price;
