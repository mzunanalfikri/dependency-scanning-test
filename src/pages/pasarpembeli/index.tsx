import styles from "@/pages/styles/PasarPembeli.module.scss";
import Layout from "@/layouts/pembeli/PembeliLayout";
import { distanceInKmBetweenEarthCoordinates } from "@/util/geolocation";
import Link from "next/link";
import PasarFeService from "@/fe-services/pasar";
import React, { useEffect, useState } from "react";
import ImageWithDefault from "@/components/ImageWithDefault";
import Pasar, { GeoPoint } from "@/models/Pasar";
import { v4 } from "uuid";
import { NextPageContext } from "next";
import redirectIfRoleDoesNotMatch from "@/fe-services/serverside/redirectIfRoleDoesNotMatch";
import RedirectClientHelper from "@/fe-services/redirect/RedirectClientHelper";
import axios from "axios";

function pasarpembeli({ redirectionInstruction }: WithRedirectionInstruction) {
  useEffect(() => {
    RedirectClientHelper(redirectionInstruction);
  }, []);
  const [pasar, setPasar] = useState<Pasar[] | null>(null);
  const [userData, setUserData] = useState({
    uid: "",
    displayName: "",
    disabled: false,
    email: "",
    emailVerified: false,
    metadata: {},
    phoneNumber: "",
    providerData: [],
    tokensValidAfterTime: new Date(),
  });
  const [isLoaded, setIsLoaded] = useState(false);
  useEffect(() => {
    const url = "./api/auth/detail-user";
    navigator.geolocation.getCurrentPosition(function (position) {
      let userLatitude = position.coords.latitude;
      let userLongitude = position.coords.longitude;
      localStorage.setItem("USER_LATITUDE", String(userLatitude));
      localStorage.setItem("USER_LONGITUDE", String(userLongitude));
    });

    async function fetchUserData(url: string) {
      const result = await axios.get(url);
      const getResult = await result.data;
      console.log(getResult);
      setUserData(getResult);
    }
    fetchUserData(url);

    PasarFeService.getAllPasar().then((data) => {
      console.log(data);
      setPasar(data);
      setIsLoaded(true);
    });
  }, []);

  if (!isLoaded) {
    return (
      <Layout title="MyProfile" className="bg-secondary">
        <div className="container mt-3">
          <div
            className="d-flex justify-content-center"
            style={{
              position: "absolute",
              left: "50%",
              top: "50%",
              transform: "translate(-50%,-50%)",
            }}
          >
            <div className="spinner-border" role="status">
              <span className="sr-only">Loading...</span>
            </div>
            <h3 className="text-center ml-3">Loading Pasar...</h3>
          </div>
        </div>
      </Layout>
    );
  } else {
    return (
      <Layout title="PasarPembeli" className="bg-secondary">
        <div className="container">
          <h2 className="mt-4">
            Selamat Datang, {isLoaded ? userData.displayName : "User"}
          </h2>
          <h3 className="mt-1">Dimana Anda ingin berbelanja?</h3>
          <div className="input-group">
            <div className="form-outline" style={{ marginLeft: "30px" }}>
              <input
                type="search"
                id="form1"
                placeholder="Cari sesuatu"
                className="form-control"
              />
            </div>
            <button type="button" className="btn btn-primary">
              <i className="fas fa-search"></i>
            </button>
          </div>
          <br></br>
          <div className="container">
            <div className="row">
              {pasar &&
                pasar.map((pasarr) => {
                  const geopoint = pasarr.geopoint as GeoPoint;

                  return (
                    <Link
                      href={"/pasarpembeli/" + `${pasarr.id}` + "/detail"}
                      passHref
                      key={v4()}
                    >
                      <div className="col-6 col-xs-4 col-sm-4 col-md-3 col-lg-2 text-center mb-3 ">
                        <div
                          id={pasarr.id}
                          className="d-flex justify-content-start flex-column bd-highlight h-100 shadow bg-light mb-3"
                          style={{ borderRadius: "25px" }}
                          onClick={() => {
                            var x = document.getElementById(pasarr.id);
                            if (!x) return;
                            if (x.id != undefined) {
                              if (
                                x.id != localStorage.getItem("CURRENT_PASAR")
                              ) {
                                localStorage.clear();
                              }
                              localStorage.setItem("CURRENT_PASAR", pasarr.id);
                            } else {
                              alert("Id pasar not exist.");
                            }
                          }}
                        >
                          <div className="p-2 bd-highlight">
                            <div className="embed-responsive embed-responsive-4by3">
                              <ImageWithDefault
                                src={pasarr.image}
                                className="embed-responsive-item"
                                srcAlternative="/landing_page/market.svg"
                                alt="Gambar Pasar"
                              />
                            </div>
                          </div>
                          <div className="p-2 bd-highlight">
                            <b key="{pasarr.name}">Pasar {pasarr.name}</b>
                            <br></br>
                            <i className="fas fa-map-marker-alt"> </i>{" "}
                            {distanceInKmBetweenEarthCoordinates(
                              Number(localStorage.getItem("USER_LATITUDE")),
                              Number(localStorage.getItem("USER_LONGITUDE")),
                              geopoint._latitude,
                              geopoint._longitude
                            )}{" "}
                            km
                            <br></br>
                            {pasarr.city}
                          </div>
                        </div>
                      </div>
                    </Link>
                  );
                })}
            </div>
          </div>
        </div>
      </Layout>
    );
  }
}

export default pasarpembeli;

export async function getServerSideProps(context: NextPageContext) {
  // kalo bukan visitor, gaboleh ke rute ini
  const redirectionInstruction = await redirectIfRoleDoesNotMatch(
    context,
    "pembeli"
  );
  return {
    props: { redirectionInstruction },
  };
}
