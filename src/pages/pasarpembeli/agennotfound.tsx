import styles from "@/pages/styles/PasarPembeli.module.scss";
import Layout from "@/layouts/pembeli/PembeliLayout";
import Link from "next/link";
import React, { useEffect, useState } from "react";

function AgenNotFound() {
  return (
    <Layout title="Agen Not Found" className="bg-secondary">
      <div
        className="container"
        style={{
          position: "absolute",
          left: "50%",
          top: "50%",
          transform: "translate(-50%,-50%)",
        }}
      >
        <div className="text-center">
          <p>
            <i className="far fa-frown fa-8x"></i>
          </p>
          <h3>
            Maaf, semua agen sedang sibuk. <br></br> Coba beberapa saat lagi
          </h3>
          <Link href={`/pasarpembeli/`}>
            <button type="button" className="btn btn-primary">
              Order
            </button>
          </Link>
        </div>
      </div>
    </Layout>
  );
}

export default AgenNotFound;
