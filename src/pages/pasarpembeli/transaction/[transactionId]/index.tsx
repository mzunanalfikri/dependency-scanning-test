import { db } from "@/database/firebase";
import RedirectClientHelper from "@/fe-services/redirect/RedirectClientHelper";
import redirectIfRoleDoesNotMatch from "@/fe-services/serverside/redirectIfRoleDoesNotMatch";
import Layout from "@/layouts/pembeli/PembeliLayout";
import axios from "axios";
import { NextPageContext } from "next";
import React, { useEffect, useState } from "react";

function TransactionDetail({
  redirectionInstruction,
}: WithRedirectionInstruction<AuthUserData>) {
  useEffect(() => {
    RedirectClientHelper(redirectionInstruction);
  }, []);
  const [idtransaction, setIdTransaction] = useState("");
  const [loadingStatus, setLoadingStatus] = useState(false);
  const [data, setData] = useState({
    arrBarang: [
      {
        name: "",
        qty: 0,
      },
    ],
    totalHarga: 0,
    id: "",
    date: {
      _seconds: 0,
      _nanosecond: 0,
    },
    agen: {
      customClaims: {
        role: "",
        isAccepted: false,
        isActive: false,
        order: {
          status: "",
          transactionId: "",
        },
      },
      displayName: "",
      phoneNumber: "",
      uid: "",
    },
    agenId: "",
    pembeliLocation: "",
    status: "",
    pasar: {
      name: "",
    },
  });

  useEffect(() => {
    const transactionId = window.location.pathname.split("/")[3];
    setIdTransaction(transactionId);
    const url = "/api/detailtransaction";
    const fetchData = async (url: string, transactionId: string) => {
      const result = await axios.get(url, {
        params: {
          id: transactionId,
        },
      });
      const transactionReference: any = await db
        .collection("transaction")
        .doc(result.data.id)
        .get();
      const pasarReference: any = await transactionReference
        .data()
        .idPasar.get();
      result.data.pasar = pasarReference.data();
      setData(result.data);
      setLoadingStatus(true);
    };
    fetchData(url, transactionId);
  }, []);

  if (!loadingStatus) {
    return (
      <Layout title="MyProfile" className="bg-secondary">
        <div className="container mt-3">
          <div
            className="d-flex justify-content-center"
            style={{
              position: "absolute",
              left: "50%",
              top: "50%",
              transform: "translate(-50%,-50%)",
            }}
          >
            <div className="spinner-border" role="status">
              <span className="sr-only">Loading...</span>
            </div>
            <h3 className="text-center ml-3">Loading Transaction...</h3>
          </div>
        </div>
      </Layout>
    );
  } else {
    return (
      <Layout title="Transaction Detail" className="bg-secondary">
        <div className="container">
          <div className="d-flex flex-column bg-light rounded shadow bd-highlight mb-3 mt-4">
            <div className="p-2 bd-highlight rounded-top bg-dark text-white text-center">
              <h2>Transaction Detail</h2>
            </div>
            <div className="p-2 bd-highlight">
              Tempat : <b>Pasar {data.pasar.name}</b>
            </div>
            {data.arrBarang.map((barangg) => {
              return (
                <div
                  className="p-2 bd-highlight text-muted"
                  style={{ fontSize: "14px" }}
                >
                  {barangg.name} {barangg.qty}
                </div>
              );
            })}
            <div className="p-2 rounded bd-highlight">
              Total Pembayaran: <b>{data.totalHarga.toLocaleString()}</b>
            </div>
            <div
              className="p-2 bd-highlight font-italic rounded-bottom bg-info text-dark"
              style={{ fontSize: "12px" }}
            >
              {data.id}
              <br />
              {new Date(data.date._seconds * 1000).toDateString() +
                " at " +
                new Date(data.date._seconds * 1000).toLocaleTimeString()}
            </div>
          </div>

          <div className="d-flex flex-column bg-light rounded shadow bd-highlight mb-3 mt-5">
            <div className="p-2 bd-highlight rounded-top bg-dark text-light text-center">
              <h2>Agen Detail</h2>
            </div>
            <div className="p-2 bd-highlight">
              Nama : <b>{data.agen.displayName}</b>
            </div>
            <div className="p-2 bd-highlight">
              Kontak :{" "}
              <b>
                <i>{data.agen.phoneNumber}</i>
              </b>
            </div>
            <div className="p-2 text-danger" style={{ fontSize: "14px" }}>
              Peringatan!<br></br> Anda tidak diperkenankan menggunakan
              informasi yang diberikan untuk kepentingan pribadi.
            </div>
            <div
              className="p-2 bd-highlight rounded-bottom font-italic bg-info text-dark"
              style={{ fontSize: "12px" }}
            >
              {data.agen.uid}
            </div>
          </div>
        </div>
      </Layout>
    );
  }
}

export async function getServerSideProps(context: NextPageContext) {
  const redirectionInstruction = await redirectIfRoleDoesNotMatch(
    context,
    "pembeli"
  );
  return {
    props: {
      redirectionInstruction,
    },
  };
}

export default TransactionDetail;
