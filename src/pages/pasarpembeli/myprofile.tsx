import StatefulInput from "@/components/StatefulInput";
import { storage } from "@/database/firebase";
import Layout from "@/layouts/pembeli/PembeliLayout";
import axios from "axios";
import React, { useEffect, useState } from "react";
import { Accordion, Button, Card, Modal } from "react-bootstrap";
import { v4 } from "uuid";

function MyProfile() {
  const [imageUrl, setImageUrl] = useState<Uint8Array | ArrayBuffer | Blob>(
    new Uint8Array() || { name: "" }
  );
  const [imageName, setImageName] = useState("");
  const [image, setImage] = useState("");
  const [isImageLoaded, setImageLoaded] = useState(false);
  const [showConfirmationModal, setConfirmationModal] = useState(false);
  const handleCloseConfirmation = () => setConfirmationModal(false);
  const handleShowConfirmation = () => setConfirmationModal(true);

  const [newDisplayName, setNewDisplayName] = useState("");
  const [newPhoneNumber, setNewPhoneNumber] = useState("");
  const [newPassword, setNewPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");

  const [isLoaded, setIsLoaded] = useState(false);

  const [userData, setUserData] = useState({
    disabled: false,
    displayName: "",
    email: "",
    emailVerified: false,
    metadata: {
      lastSignInTime: "",
      creationTime: "",
    },
    phoneNumber: "",
    providerData: [],
    tokensValidAfterTime: "",
    uid: "",
    photoURL: "",
  });

  useEffect(() => {
    setNewDisplayName(userData.displayName);
    setNewPhoneNumber(userData.phoneNumber);
  }, [userData]);

  function CheckoutModal() {
    return (
      <Modal
        size="lg"
        show={showConfirmationModal}
        onHide={handleCloseConfirmation}
        aria-labelledby="contained-modal-title-vcenter"
        backdrop="static"
        keyboard={false}
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title>Konfirmasi Ubah Profil</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          Anda akan mengubah detail dari profil Anda seperti yang sudah diinput
          sebelumnya, pastikan bahwa data yang Anda isi sudah sesuai sebelum
          kami memproses ke langkah selanjutnya.
        </Modal.Body>
        <Modal.Footer>
          <Button variant="primary" onClick={handleCloseConfirmation}>
            Batal
          </Button>
          <Button variant="primary" onClick={handleFinishModal}>
            Lanjutkan
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }

  function handleFinishModal() {
    handleSubmit();
    handleCloseConfirmation();
  }

  function validateEmail(email: any) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  function validatePassword() {
    let isValidConfirmPassword = false;
    const resultConfirmPassword = document.getElementById(
      "resultConfirmPassword"
    );
    if (newPassword != confirmPassword) {
      resultConfirmPassword!.innerHTML =
        "Pastikan password yang Anda ketik ulang sudah sama.";
      resultConfirmPassword!.className = "font-weight-bold text-danger";
    } else if (newPassword.length > 10 && newPassword == confirmPassword) {
      isValidConfirmPassword = true;
    }
    return isValidConfirmPassword;
  }

  function validateName() {
    let isNameFilled = false;
    const resultName = document.getElementById("resultName");
    if (newDisplayName.length != 0) {
      isNameFilled = true;
    }
    return isNameFilled;
  }

  function validatePhoneNumber(phonenumber: any) {
    const re = /(\+62 ((\d{3}([ -]\d{3,})([- ]\d{4,})?)|(\d+)))|(\(\d+\) \d+)|\d{3}( \d+)+|(\d+[ -]\d+)|\d+/i;
    return re.test(String(phonenumber).toLowerCase());
  }

  function validatePhone() {
    let isValidPhoneNumber = false;
    const resultPhone = document.getElementById("resultPhoneNumber");
    if (validatePhoneNumber(newPhoneNumber)) {
      isValidPhoneNumber = true;
    } else if (
      newPhoneNumber.length != 0 &&
      !validatePhoneNumber(newPhoneNumber)
    ) {
      resultPhone!.innerHTML =
        "Nomor HP salah. Pastikan Anda menuliskan format nomor HP yang benar.";
      resultPhone!.className = "font-weight-bold text-danger";
    }
    return isValidPhoneNumber;
  }

  async function handleSubmit() {
    let isNameFilled = validateName();
    let isValidConfirmPassword = validatePassword();
    let isValidPhoneNumber = validatePhone();
    if (image.length > 0) {
      const putImageToStorage = async () => {
        const imgPath = `user/${imageName}_${v4()}`;
        const itemRef = storage.child(imgPath);
        await itemRef.put(imageUrl);
        return itemRef;
      };
      const itemRef = await putImageToStorage();
      const getDownloadURL = async () => {
        const itemURL = await itemRef.getDownloadURL();
        console.log(itemURL);
        return itemURL;
      };

      const photoURL = await getDownloadURL();
      const newImage = await axios.post("/api/auth/detail-user", {
        newDP: photoURL,
      });
      console.log(newImage);
      const resultImage = document.getElementById("resultImage");
      resultImage!.innerHTML =
        "Anda berhasil mengganti foto profil Anda.Tekan tombol dibawah foto untuk menyimpan";
      resultImage!.className = "text-success font-weight-bold";
    }
    if (image.length > 0) {
      const saveChanges = document.getElementById(
        "savechanges"
      ) as HTMLButtonElement;
      saveChanges!.disabled = false;
    }
    if (isNameFilled) {
      const changeName = await axios.post("/api/auth/detail-user", {
        newName: newDisplayName,
      });
      console.log(changeName);
      setNewDisplayName("");
      const resultDisplayName = document.getElementById("resultName");
      resultDisplayName!.innerHTML =
        "Anda berhasil mengganti display name Anda.";
      resultDisplayName!.className = "text-success font-weight-bold";
    }
    if (isValidConfirmPassword) {
      const resultConfirmPassword = document.getElementById(
        "resultConfirmPassword"
      );
      const changePassword = await axios.post("/api/auth/detail-user", {
        newPassword: newPassword,
      });
      setNewPassword("");
      setConfirmPassword("");
      resultConfirmPassword!.innerHTML =
        "Anda berhasil mengganti password Anda.";
      resultConfirmPassword!.className = "text-success font-weight-bold";
    }
    if (isValidPhoneNumber) {
      const resultPhoneNumber = document.getElementById("resultPhoneNumber");
      const changePhoneNumber = await axios.post("/api/auth/detail-user", {
        newPhoneNumber: newPhoneNumber,
      });
      setNewPhoneNumber("");
      resultPhoneNumber!.innerHTML = "Anda berhasil mengganti nomor HP Anda.";
      resultPhoneNumber!.className = "text-success font-weight-bold";
    }
  }

  function handleImage(event: any) {
    if (event.target.files && event.target.files[0]) {
      console.log(event.target.files[0]);
      setImageUrl(event.target.files[0]);
      setImageName(event.target.files[0].name);
      let reader = new FileReader();
      reader.onload = (e) => {
        setImage(e.target!.result as string);
      };
      reader.readAsDataURL(event.target.files[0]);
      setImageLoaded(true);
    }
  }

  useEffect(() => {
    const url = "/api/auth/detail-user";
    async function fetchUserData(url: string) {
      const result = await axios.get(url);
      const getResult = await result.data;
      console.log(getResult);
      setUserData(getResult);
      setIsLoaded(true);
    }
    fetchUserData(url);
  }, []);

  if (!isLoaded) {
    return (
      <Layout title="MyProfile" className="bg-secondary">
        <div className="container mt-3">
          <div
            className="d-flex justify-content-center"
            style={{
              position: "absolute",
              left: "50%",
              top: "50%",
              transform: "translate(-50%,-50%)",
            }}
          >
            <div className="spinner-border" role="status">
              <span className="sr-only">Loading...</span>
            </div>
            <h3 className="text-center ml-3">Loading and Saving Profile...</h3>
          </div>
        </div>
      </Layout>
    );
  }

  return (
    <Layout title="MyProfile" className="bg-secondary">
      <div className="container mt-3 mb-3">
        <h2 className="text-center">Edit Profil</h2>
        <div className="text-center">
          <img
            src={userData.photoURL}
            height="100"
            width="100"
            className="rounded-circle"
          />
        </div>
        <div className="text-center mt-3">
          <a href="/pasarpembeli/myprofile">
            <button
              type="button"
              className="btn btn-sm btn-primary"
              id="savechanges"
              disabled
            >
              Save Changes
            </button>
          </a>
        </div>
        <div className="container mt-5">
          <div className="form-group row">
            <label className="col-4 col-xs-4 col-sm-4 col-md-4 col-lg-2 col-form-label">
              Foto Profil
            </label>
            <div className="col-8 col-xs-8 col-sm-8 col-md-8 col-lg-10">
              <input
                id="myImage"
                type="file"
                name="myImage"
                accept="image/*"
                onChange={handleImage}
              />
              {isImageLoaded && image.length != 0 ? (
                <div className="p-2">
                  <img id="target" height="100" src={image} />
                </div>
              ) : (
                ""
              )}
              <p id="resultImage"></p>
            </div>
          </div>
          <div className="form-group row">
            <label className="col-4 col-xs-4 col-sm-4 col-md-4 col-lg-2 col-form-label">
              Nama Profil
            </label>
            <div className="col-8 col-xs-8 col-sm-8 col-md-8 col-lg-10">
              <input
                value={newDisplayName}
                required
                type="text"
                className="form-control"
                id="displayname"
                aria-describedby="displayName"
                placeholder="Masukkan display name baru"
                onChange={(e) => {
                  setNewDisplayName(e.target.value);
                }}
              />
              <p id="resultName"></p>
            </div>
          </div>
          <div className="form-group row">
            <label className="col-4 col-xs-4 col-sm-4 col-md-4 col-lg-2 col-form-label">
              Nomor HP
            </label>
            <div className="col-8 col-xs-8 col-sm-8 col-md-8 col-lg-10">
              <input
                value={newPhoneNumber}
                required
                type="text"
                className="form-control"
                id="phonenumber"
                aria-describedby="phonenumber"
                placeholder="Masukkan nomor HP baru"
                onChange={(e) => {
                  setNewPhoneNumber(e.target.value);
                }}
              />
              <p id="resultPhoneNumber"></p>
            </div>
          </div>
          <Accordion className="w-100 bg-primary">
            <Card className="w-100">
              <Accordion.Toggle eventKey="0" as={Button}>
                Klik bila anda ingin mengubah password
              </Accordion.Toggle>
              <Accordion.Collapse eventKey="0">
                <div className="container">
                  <StatefulInput
                    state={newPassword}
                    type="password"
                    label="Password"
                    name="password"
                    setState={setNewPassword}
                  />
                  <StatefulInput
                    state={confirmPassword}
                    type="password"
                    label="Confirm Password"
                    name="confirm-password"
                    setState={setConfirmPassword}
                  />
                </div>
              </Accordion.Collapse>
            </Card>
          </Accordion>
          <button
            type="submit"
            className="btn btn-block btn-primary mt-4"
            onClick={handleShowConfirmation}
            disabled={
              newDisplayName.length == 0 &&
              newPassword.length == 0 &&
              newPhoneNumber.length == 0 &&
              image.length == 0
            }
          >
            Ubah Profil
          </button>
        </div>
      </div>
      <CheckoutModal />
    </Layout>
  );
}

export default MyProfile;
