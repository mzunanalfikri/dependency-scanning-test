import ClosableAlert from "@/components/ClosableAlert";
import { useAuth } from "@/contexts/AuthContext";
import { auth } from "@/database/firebase";
import RedirectClientHelper from "@/fe-services/redirect/RedirectClientHelper";
import redirectIfRoleDoesNotMatch from "@/fe-services/serverside/redirectIfRoleDoesNotMatch";
import VisitorLayout from "@/layouts/visitor/VisitorLayout";
import { NextPageContext } from "next";
import router from "next/router";
import React, { useEffect, useState } from "react";
import { Container } from "react-bootstrap";
import nookies, { destroyCookie } from "nookies";

function Logout({ redirectionInstruction }: WithRedirectionInstruction) {
  const [requestState, setRequestState] = useState<RequestState>("none");
  useEffect(() => {
    RedirectClientHelper(redirectionInstruction);
  }, []);

  useEffect(() => {
    async function trySigningOut() {
      try {
        setRequestState("loading");
        await auth.signOut();
        setRequestState("success");
        setTimeout(() => {
          router.push("/login");
        }, 1000);
      } catch (err) {
        setRequestState("error");
      }
    }
    trySigningOut();
  }, []);

  return (
    <VisitorLayout title="logout" className="flex-center bg-secondary">
      <Container>
        <h1>Logging out</h1>
        {requestState == "success" ? (
          <ClosableAlert message="Berhasil logout" title="Sukses" />
        ) : requestState == "error" ? (
          <ClosableAlert
            message="Gagal logout"
            title="Failure"
            variant="danger"
          />
        ) : (
          <p>tunggu sebentar...</p>
        )}
      </Container>
    </VisitorLayout>
  );
}

export default Logout;

export async function getServerSideProps(context: NextPageContext) {
  const redirectionInstruction = await redirectIfRoleDoesNotMatch(context, [
    "admin",
    "agen",
    "pembeli",
    "pengelola-pasar",
  ]);
  return {
    props: { redirectionInstruction },
  };
}
