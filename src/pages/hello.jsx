import React, { useState, useEffect } from "react";
import Layout from "../layouts/visitor/VisitorLayout";
import axios from "axios";
import { Button, Form } from "react-bootstrap";
import VisitorLayout from "../layouts/visitor/VisitorLayout";

function Hello(props) {
  return (
    <VisitorLayout title="Hello">
      <Button
        onClick={() => {
          axios.get("/api/transaction").then((response) => {
            console.log(response.data);
          });
        }}
      >
        get data from api
      </Button>
      <Button
        onClick={() => {
          // anggep dia handle transaksi
          axios.post("/api/transaction", {
            uid: "dasjoidjasqwo",
            arrBarang: [],
            totalHarga: 10000,
          });
        }}
      >
        post data to api
      </Button>
    </VisitorLayout>
  );
}

export async function getServerSideProps() {
  return {
    props: {
      nama: "hello world",
    },
  };
}

export default Hello;
