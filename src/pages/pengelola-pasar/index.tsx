import ListBarang from "@/components/pengelola-pasar/ListBarang";
import Pasar from "@/components/pengelola-pasar/Pasar";
import StatefulModal from "@/components/StatefulModal";
import { db as adminDb } from "@/database/admin";
import RedirectClientHelper from "@/fe-services/redirect/RedirectClientHelper";
import redirectIfRoleDoesNotMatch from "@/fe-services/serverside/redirectIfRoleDoesNotMatch";
import PengelolaPasarLayout from "@/layouts/pengelola-pasar/DELayout";
import Item from "@/models/Barang";
import PasarModel from "@/models/Pasar";
import axios from "axios";
import { NextPageContext } from "next";
import Link from "next/link";
import React, { useEffect, useState } from "react";
import { Button } from "react-bootstrap";

interface Props {
  market: string;
  items: string;
  name: string;
  marketId: string;
}

function PengelolaPasar(props: Props & WithRedirectionInstruction) {
  useEffect(() => {
    console.log("redirection, pengelola pasar");
    RedirectClientHelper(props.redirectionInstruction);
  }, []);
  const [showModal, setShowModal] = useState(false);
  const [requestState, setRequestState] = useState<RequestState>("none");
  const [item, setItem] = useState<Item>({
    name: "",
    price: 0,
    image: "",
    category: ["none"],
  });
  const [items, setItems] = useState<Item[]>(JSON.parse(props.items));

  useEffect(() => {
    console.log(props.items);
  }, []);

  const deleteItem = async () => {
    await axios.delete("/api/pengelola-pasar/market/item", {
      headers: {},
      data: {
        marketId: props.marketId,
        itemId: item.id,
      },
    });
    // delete barang di front end
    setItems(items.filter((val) => val.id !== item.id));
    return true;
  };
  const showItemDeleteModal = ({ name, id }: Item) => {
    setItem({
      ...item,
      name,
      id,
    });
    setShowModal(true);
  };
  const onAcceptModal = async () => {
    if (!item.id) {
      return;
    }
    setRequestState("loading");
    await deleteItem();
    setTimeout(() => setRequestState("success"), 200);
  };
  const onRejectModal = () => {
    setShowModal(false);
  };

  useEffect(() => {
    if (requestState == "success") {
      setTimeout(() => {
        setShowModal(false);
      }, 1000);
    }
  }, [requestState]);

  useEffect(() => {
    if (showModal == false) {
      setTimeout(() => setRequestState("none"), 1200);
    }
  }, [showModal]);

  return (
    <PengelolaPasarTemplate
      showModal={showModal}
      requestState={requestState}
      onShowModal={showItemDeleteModal}
      onAcceptModal={onAcceptModal}
      onRejectModal={onRejectModal}
      itemShown={item}
      market={JSON.parse(props.market)}
      name={props.name}
      items={items}
    />
  );
}

function PengelolaPasarTemplate({
  showModal,
  requestState,
  onShowModal,
  onAcceptModal,
  onRejectModal,
  items,
  itemShown,
  market,
  name,
}: ModalProps &
  RequestStateProps & {
    itemShown: Item;
    market: PasarModel;
    items: Item[];
    name: string;
  }) {
  return (
    <PengelolaPasarLayout title="Pengelola Pasar">
      <h1 className="font-weight-bold">Selamat datang, {name}!</h1>

      <section>
        <h2 className="mr-4">Pasar Anda: </h2>
        <Pasar {...market} />
      </section>
      <div className="d-flex justify-content-between">
        <h2>List barang di pasar anda</h2>
        <div>
          <Link passHref href={`/pengelola-pasar/market/${market.id}/tambah`}>
            <Button variant="primary" className="text-center font-weight-bold">
              <i className="fas fa-plus mr-2"></i>
              Tambah barang
            </Button>
          </Link>
        </div>
      </div>
      <ListBarang
        showModal={showModal}
        onShowModal={onShowModal}
        items={items}
      />
      <StatefulModal
        message={`Apakah anda yakin ingin menghapus barang "${itemShown.name}?"`}
        showModal={showModal}
        requestState={requestState}
        loadingMessage="Menghapus..."
        successMessage="Barang telah berhasil dihapus"
        onAcceptModal={onAcceptModal}
        onRejectModal={onRejectModal}
      />
    </PengelolaPasarLayout>
  );
}

export async function getServerSideProps(context: NextPageContext) {
  const redirectionInstruction = await redirectIfRoleDoesNotMatch(
    context,
    "pengelola-pasar"
  );
  const props: WithRedirectionInstruction = {
    redirectionInstruction,
  };

  if (!redirectionInstruction.data?.role) {
    return { props };
  }
  if (!redirectionInstruction.data?.marketId) {
    return { props };
  }
  const marketId = redirectionInstruction.data?.marketId as string;
  const marketCollection = adminDb.collection("market");
  const marketDoc = await marketCollection.doc(marketId).get();
  const marketItems = (
    await marketCollection.doc(marketId).collection("items").get()
  ).docs;

  const marketData: any = marketDoc.data();
  const market: PasarModel = {
    id: marketId,
    ...marketData,
  };

  const itemsData: Item[] = [];
  for (const item of marketItems) {
    const itemDocRef = item.data()
      .item as firebase.default.firestore.DocumentReference;
    const itemDoc = await itemDocRef.get();
    const itemData: Item = itemDoc.data() as Item;
    itemsData.push({
      id: item.id,
      ...itemData,
      price: item.data().price,
      name: itemDoc.id,
    });
  }

  const fullProps: Props & WithRedirectionInstruction = {
    market: JSON.stringify(market),
    items: JSON.stringify(itemsData),
    name: redirectionInstruction.data?.name ?? "",
    marketId,
    ...props,
  };
  return {
    props: fullProps,
  };
}

export default PengelolaPasar;
