import RedirectClientHelper from "@/fe-services/redirect/RedirectClientHelper";
import redirectIfRoleDoesNotMatch from "@/fe-services/serverside/redirectIfRoleDoesNotMatch";
import { NextPageContext } from "next";
import React from "react";
import { useEffect } from "react";

function Recommend({ redirectionInstruction }: WithRedirectionInstruction) {
  useEffect(() => {
    RedirectClientHelper(redirectionInstruction);
  }, []);
  return <div></div>;
}

export default Recommend;
export async function getServerSideProps(context: NextPageContext) {
  // kalo bukan visitor, gaboleh ke rute ini
  const redirectionInstruction = await redirectIfRoleDoesNotMatch(
    context,
    "pengelola-pasar"
  );
  return {
    props: { redirectionInstruction },
  };
}
