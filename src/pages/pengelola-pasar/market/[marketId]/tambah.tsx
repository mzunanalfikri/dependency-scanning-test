import StatefulInput from "@/components/StatefulInput";
import StatefulModal from "@/components/StatefulModal";
import TambahBarangFeService from "@/fe-services/item/tambahBarangService";
import RedirectClientHelper from "@/fe-services/redirect/RedirectClientHelper";
import redirectIfRoleDoesNotMatch from "@/fe-services/serverside/redirectIfRoleDoesNotMatch";
import PengelolaPasarLayout from "@/layouts/pengelola-pasar/DELayout";
import { ItemCategory } from "@/models/Barang";
import { NextPageContext } from "next";
import router from "next/router";
import React, { useEffect, useState } from "react";
import { useRef } from "react";
import { Button, Col, Form, Row } from "react-bootstrap";

function AddItem({
  redirectionInstruction,
}: WithRedirectionInstruction<AuthUserData>) {
  useEffect(() => {
    RedirectClientHelper(redirectionInstruction);
  }, []);
  const marketId = redirectionInstruction.data?.marketId as string;
  const [showModal, setShowModal] = useState(false);
  const [requestState, setRequestState] = useState<RequestState>("none");
  const [item, setItem] = useState<ItemInput>({
    name: "",
    price: 0,
    category: "",
    description: "",
  });
  const [image, setImage] = useState<File | null>();

  useEffect(() => {
    if (requestState == "success") {
      router.push("/pengelola-pasar");
    }
  }, [requestState]);

  const addItem = async () => {
    setRequestState("loading");

    if (!image) {
      alert("No image found");
      return;
    }
    try {
      let imgUrl;
      const new_item = await TambahBarangFeService.getItem(item.name);
      if (!new_item) {
        imgUrl = await TambahBarangFeService.uploadImage(image);
      } else {
        imgUrl = new_item.image;
      }

      // then create the item
      await TambahBarangFeService.createItem({
        name: item.name,
        price: item.price,
        category: item.category.split(",") as ItemCategory[],
        image: imgUrl,
        description: item.description,
        id: item.name,
        marketId,
      });
      setRequestState("success");
    } catch (err) {
      alert("an error occured, more detail on console");
      setRequestState("error");
    }
    // add the image first
  };

  const onRejectModal = () => {
    setShowModal(false);
  };

  useEffect(() => {
    if (requestState === "success") {
      setTimeout(() => {
        setShowModal(false);
      }, 1000); // wait 1 sec before closing the modal
    }
  }, [requestState]);

  useEffect(() => {
    if (showModal === false) {
      setTimeout(() => {
        setRequestState("none");
      }, 200); // add delay because there is animation
    }
  }, [showModal]);

  return (
    <PengelolaPasarLayout title="tambah barang">
      <TambahTemplate
        onClick={() => {
          addItem();
        }}
        setItem={setItem}
        setImage={setImage}
      />

      <StatefulModal
        message={`Apakah anda yakin ingin menambahkan barang dengan detail: <br/> Nama: ${item.name},<br/>
        Harga: ${item.price},<br/>
        Kategori: ${item.category}?`}
        showModal={showModal}
        requestState={requestState}
        loadingMessage="Menyimpan..."
        successMessage="Barang telah berhasil disimpan"
        onAcceptModal={addItem}
        onRejectModal={onRejectModal}
      />
    </PengelolaPasarLayout>
  );
}

interface AddTemplateProps extends Clickable {
  setItem: React.Dispatch<ItemInput>;
  setImage: React.Dispatch<any>;
}

interface ItemInput {
  name: string;
  price: number;
  category: string;
  description: string;
}

function TambahTemplate({ onClick, setItem, setImage }: AddTemplateProps) {
  const [name, setName] = useState("");
  const [price, setPrice] = useState(0);
  const [category, setCategory] = useState("makanan");
  const [description, setDescription] = useState("");

  useEffect(() => {
    setItem({
      name,
      price,
      category,
      description,
    });
  }, [name, price, category, description]);

  return (
    <>
      <h1>Tambah Barang</h1>
      <p>Masukkan detail info barang</p>
      <StatefulInput
        state={name}
        label="Nama"
        name="Nama Barang"
        setState={setName}
      />
      <StatefulInput
        type="number"
        state={price}
        name="Harga"
        setState={setPrice}
        label="Harga"
      />
      <Form.Group controlId="Control select">
        <Form.Label className="font-weight-bold">Kategori</Form.Label>
        <Form.Control
          as="select"
          onChange={(e) => {
            setCategory(e.target.value);
          }}
        >
          <option value="makanan">Makanan</option>
          <option value="minuman">Minuman</option>
          <option value="buah-buahan">Buah-buahan</option>
          <option value="sayuran">Sayuran</option>
          <option value="bahan makanan">Bahan Makanan</option>
          <option value="peralatan">Peralatan</option>
          <option value="bahan bangunan">Bahan Bangunan</option>
          <option value="perhiasan">Hiasan</option>
          <option value="perabotan">Perabotan</option>
          <option value="other">Lainnya</option>
        </Form.Control>
      </Form.Group>
      <StatefulInput
        state={description}
        textarea
        name="Deskripsi"
        setState={setDescription}
        label="Deskripsi"
      />
      <Form.Group>
        <Form.File
          id="file"
          label="Gambar"
          name="image"
          onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
            if (!!e.target.files) {
              setImage(e.target.files[0]);
            }
          }}
        ></Form.File>
      </Form.Group>
      <Row className="mt-5">
        <Col className="d-flex">
          <Button
            variant="primary"
            className="font-weight-bold text-white mx-auto"
            onClick={onClick}
          >
            <i className="fas fa-plus mr-1"></i>
            Tambah Barang
          </Button>
        </Col>
      </Row>
    </>
  );
}

export default AddItem;

export async function getServerSideProps(context: NextPageContext) {
  // kalo bukan visitor, gaboleh ke rute ini
  const redirectionInstruction = await redirectIfRoleDoesNotMatch(
    context,
    "pengelola-pasar"
  );
  return {
    props: { redirectionInstruction },
  };
}
