import RedirectClientHelper from "@/fe-services/redirect/RedirectClientHelper";
import redirectIfAuthenticated from "@/fe-services/serverside/RedirectIfAuthenticated";
import VisitorLayout from "@/layouts/visitor/VisitorLayout";
import { NextPageContext } from "next";
import Link from "next/link";
import router from "next/router";
import React, { useEffect } from "react";
import { Button } from "react-bootstrap";
import nookies from "nookies";
import { auth } from "@/database/admin";

/**
 * redirecting the user to their respective home page based on their role
 * will get a person's role from serverside and redirect.
 *
 * if he is a visitor, if there is any idtoken in the site, delete it
 * so that it will not do hellish loop
 */

function Redirect({
  redirectionInstruction,
}: WithRedirectionInstruction<AuthUserData>) {
  useEffect(() => {
    RedirectClientHelper(redirectionInstruction);
  }, []);

  return (
    <VisitorLayout title="Redirecting..." className="flex-center bg-secondary">
      <h1>Please wait for a bit...</h1>
      <h2>We are currently redirecting you</h2>
      <p>If there is no changes, you can go back to home</p>
      <Link href="/">
        <Button variant="primary">Home</Button>
      </Link>
    </VisitorLayout>
  );
}

export default Redirect;

export async function getServerSideProps(context: NextPageContext) {
  const redirectionInstruction = await redirectIfAuthenticated(context);
  if (redirectionInstruction.data && redirectionInstruction.data.uid) {
    console.log(redirectionInstruction);
  }
  return {
    props: { redirectionInstruction },
  };
}
