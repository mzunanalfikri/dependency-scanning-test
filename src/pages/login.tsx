import ClosableAlert from "@/components/ClosableAlert";
import StatefulInput from "@/components/StatefulInput";
import { useAuth } from "@/contexts/AuthContext";
import RedirectClientHelper from "@/fe-services/redirect/RedirectClientHelper";
import redirectIfAuthenticated from "@/fe-services/serverside/RedirectIfAuthenticated";
import Layout from "@/layouts/visitor/VisitorLayout";
import { NextPageContext } from "next";
import router from "next/router";
import React, { FormEvent, useEffect, useState } from "react";
import { Button, Form } from "react-bootstrap";
import styles from "./styles/Login.module.scss";

function Login({ redirectionInstruction }: WithRedirectionInstruction) {
  // redirect if user has logged in
  useEffect(() => {
    RedirectClientHelper(redirectionInstruction);
  }, []);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [loginState, setLoginState] = useState<RequestState>("none");
  const auth = useAuth();

  async function login(e: FormEvent<HTMLFormElement>) {
    e.preventDefault();
    try {
      setLoginState("loading");
      await auth.signin(email, password);
      setLoginState("success");
      router.push("/redirect");
    } catch (err) {
      setLoginState("error");
    }
  }

  function showError() {
    alert("Login gagal, periksa kembali email atau password");
    setLoginState("none");
  }

  return (
    <Layout title="Login" className="bg-secondary flex-center">
      <h1>
        <i className="fas fa-user mr-3"></i>
        Login
      </h1>
      <Form className={styles.form} onSubmit={login}>
        {loginState == "loading" ? (
          <p className="text-danger">Loading...</p>
        ) : loginState == "success" ? (
          <ClosableAlert
            message="Telah berhasil login"
            title="Login Sukses!"
            variant="success"
          />
        ) : (
          loginState == "error" && showError()
        )}
        <StatefulInput
          type="email"
          inputClass={styles.input}
          name="Email"
          state={email}
          setState={setEmail}
          icon={<i className={`fas fa-envelope ${styles.icon}`}></i>}
        />
        <StatefulInput
          type="password"
          inputClass={styles.input}
          name="Password"
          state={password}
          setState={setPassword}
          icon={<i className={`fas fa-key ${styles.icon}`}></i>}
        />
        <Button variant="primary" type="submit" className={styles.submit}>
          Submit
        </Button>
        <Form.Group controlId="question" className={styles.question}>
          <Form.Label>
            Belum punya akun?{" "}
            <a href="/signup">
              <b>Daftar</b>
            </a>
          </Form.Label>
        </Form.Group>
      </Form>
    </Layout>
  );
}

export default Login;

export async function getServerSideProps(context: NextPageContext) {
  const redirectionInstruction = await redirectIfAuthenticated(context);
  return {
    props: { redirectionInstruction },
  };
}
