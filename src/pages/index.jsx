import styles from "./styles/Home.module.scss";
import Layout from "@/layouts/visitor/VisitorLayout";
import Link from "next/link";
import { Button } from "react-bootstrap";

export default function Home() {
  return (
    <Layout title="Home">
      <main className="main">
        <section className={`${styles.introduction}`}>
          <div className={`${styles.introductionText}`}>
            <h1>
              Bingung belanja pas pandemi? Titipin Aja!
              <br />
            </h1>

            <Link href="/login">
              <Button className="text-lg">Belanja sekarang!</Button>
            </Link>
          </div>
          <img className="img-fluid" src="/landing_page/pasar.svg" />
        </section>

        <section className={`${styles.tatacara} pt-5`}>
          <div className={`${styles.how} py-5`}>Bagaimana Cara Kerjanya?</div>
          <div className={styles.cara1}>
            <div className={styles.imgcara}>
              <img
                src="/landing_page/market.svg"
                className={styles.imgmarket}
              />
            </div>
            <div className={styles.penjelasan}>
              <p className={styles.judulcara}>Pilih Pasar</p>
              <p className={styles.deskripsicara}>
                Pilih pasar yang kamu inginkan setelah anda masuk ke dalam
                aplikasi
              </p>
            </div>
          </div>
          <div className={styles.cara2}>
            <div className={styles.imgcara}>
              <img src="/landing_page/cart.svg" className={styles.imgcart} />
            </div>
            <div className={styles.penjelasan}>
              <p className={styles.judulcara}>Pilih Barang</p>
              <p className={styles.deskripsicara}>
                Tentukan barang belanjaan yang ingin dibeli. Ingat, barang
                belanjaan tidak boleh berupa barang mewah! Jangan lupa menekan
                tombol order
              </p>
            </div>
          </div>
          <div className={styles.cara3}>
            <div className={styles.imgcara}>
              <img
                src="/landing_page/hourglass.svg"
                className={styles.imghourglass}
              />
            </div>
            <div className={styles.penjelasan}>
              <p className={styles.judulcara}>Tunggu Barang Sampai</p>
              <p className={styles.deskripsicara}>
                Pengantar kami telah memroses permintaan anda, cukup tunggu dan
                barang akan segera diantarkan ke rumah anda. Bila ada perubahan,
                jangan khawatir! Kami memiliki fitur chat sehingga komunikasi
                mudah
              </p>
            </div>
          </div>
          <div className={styles.cara4}>
            <div className={styles.imgcara}>
              <img src="/landing_page/box.svg" className={styles.imgbox} />
            </div>
            <div className={styles.penjelasan}>
              <p className={styles.judulcara}>Barang Sampai</p>
              <p className={styles.deskripsicara}>
                Jangan lupa untuk membayar dan mengucapkan terimakasih kepada
                pengantar kami
              </p>
            </div>
          </div>
        </section>
      </main>
    </Layout>
  );
}
