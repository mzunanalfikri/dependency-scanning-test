import AuthProvider from "@/contexts/AuthContext";
import { SocketProvider } from "@/contexts/SocketProvider";
import { AppProps } from "next/dist/next-server/lib/router/router";
import React from "react";
import "styles/index.scss";
import "styles/_variables.scss";

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <AuthProvider>
      <SocketProvider>
        <Component {...pageProps} />
      </SocketProvider>
    </AuthProvider>
  );
}

export default MyApp;
