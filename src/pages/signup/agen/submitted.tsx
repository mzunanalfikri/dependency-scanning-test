import VisitorLayout from "@/layouts/visitor/VisitorLayout";
import Link from "next/link";
import React from "react";
import { Button, Container } from "react-bootstrap";

function FinishedSignUp() {
  return (
    <VisitorLayout
      title="Finished Sign Up!"
      description="You have finished your sign up process"
      className="bg-secondary flex-center"
    >
      <Container className="flex-center flex-column">
        <h1 className="text-center">Terimakasih atas aplikasi anda</h1>
        <p>Harap tunggu hingga satu minggu sampai HR kami menghubungi anda</p>
        <Link href="/" passHref>
          <Button variant="primary">Go back</Button>
        </Link>
      </Container>
    </VisitorLayout>
  );
}

export default FinishedSignUp;
