import React, { useState, useEffect } from "react";
import Layout from "@/layouts/visitor/VisitorLayout";
import { Container, Form } from "react-bootstrap";
import { Button } from "react-bootstrap";
import { useRouter } from "next/router";
import styles from "@/pages/styles/Signup.module.scss";
import StatefulInput from "@/components/StatefulInput";
import axios from "axios";
import ClosableAlert from "@/components/ClosableAlert";
import LoadingOverlay from "@/components/LoadingOverlay";
import RedirectClientHelper from "@/fe-services/redirect/RedirectClientHelper";
import { NextPageContext } from "next";
import redirectIfRoleDoesNotMatch from "@/fe-services/serverside/redirectIfRoleDoesNotMatch";

const maxProposalChar = 500;

function SignupAgen({ redirectionInstruction }: WithRedirectionInstruction) {
  useEffect(() => {
    RedirectClientHelper(redirectionInstruction);
  }, []);
  const [email, setEmail] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [fullName, setFullName] = useState("");
  const [signupState, setSignupState] = useState<RequestState>("none");
  const [error, setError] = useState({ title: "", message: "" });
  const [proposal, setProposal] = useState("");
  const [proposalWordLeft, setProposalWordLeft] = useState(maxProposalChar);
  const router = useRouter();

  async function register(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();
    try {
      if (proposalWordLeft <= 0) {
        alert("Proposal melebihi 1000 huruf, harap dikurangi");
        return;
      }
      if (phoneNumber.length > 20) {
        alert("Harap masukan nomor telepon yang valid");
        return;
      }
      setSignupState("loading");
      const response = await axios.post("/api/agen/proposal", {
        email,
        phoneNumber,
        proposal,
        fullName,
      });
      console.log(response);
      setSignupState("success");
      router.push("/signup/agen/submitted");
    } catch (err) {
      console.log(err.response.data);
      setSignupState("error");
      setError({
        title: "Error",
        message: err.response.data.message,
      });
    }
  }

  useEffect(() => {
    setProposalWordLeft(maxProposalChar - proposal.length);
  }, [proposal]);

  return (
    <Layout title="SignupAgen" className="bg-secondary flex-center">
      {!!error.title && !!error.message ? (
        <ClosableAlert
          variant="danger"
          message={error.message}
          title={error.title}
        />
      ) : (
        ""
      )}
      {signupState === "loading" ? <LoadingOverlay /> : ""}
      <Container className="flex-center">
        <Form onSubmit={register} className={styles.form}>
          <img src="/login/deliveryman.png" className={styles.imgclient}></img>
          <Form.Group controlId="question" className={styles.question}>
            <Form.Label>
              <b>Daftar Sebagai Agen</b>
            </Form.Label>
          </Form.Group>
          <StatefulInput
            state={fullName}
            setState={setFullName}
            icon={<i className={`fas fa-user ${styles.icon}`}></i>}
            name="Full-Name"
            type="text"
            placeholder="Nama Lengkap"
            inputClass={styles.input}
          />
          <StatefulInput
            state={email}
            setState={setEmail}
            icon={<i className={`fas fa-envelope ${styles.icon}`}></i>}
            name="email"
            type="email"
            placeholder="Email"
            inputClass={styles.input}
          />
          <StatefulInput
            state={phoneNumber}
            setState={setPhoneNumber}
            icon={<i className={`fas fa-phone ${styles.icon}`}></i>}
            name="phone number"
            type="text"
            placeholder="Nomor Telepon"
            inputClass={styles.input}
          />
          <StatefulInput
            state={proposal}
            setState={setProposal}
            icon={<i className={`fas fa-list ${styles.icon}`}></i>}
            name="proposal"
            textarea
            placeholder="Proposal"
            inputClass={styles.input}
          />
          <b className="text-left text-danger">
            *Tersisa {proposalWordLeft} huruf untuk proposal anda
          </b>
          <Button variant="primary" type="submit" className={styles.submit}>
            Daftar
          </Button>
        </Form>
      </Container>
    </Layout>
  );
}

export default SignupAgen;

export async function getServerSideProps(context: NextPageContext) {
  // kalo bukan visitor, gaboleh ke rute ini
  const redirectionInstruction = await redirectIfRoleDoesNotMatch(
    context,
    "visitor"
  );
  return {
    props: { redirectionInstruction },
  };
}
