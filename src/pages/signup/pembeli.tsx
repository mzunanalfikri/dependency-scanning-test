import ClosableAlert from "@/components/ClosableAlert";
import LoadingOverlay from "@/components/LoadingOverlay";
import StatefulInput from "@/components/StatefulInput";
import RedirectClientHelper from "@/fe-services/redirect/RedirectClientHelper";
import redirectIfRoleDoesNotMatch from "@/fe-services/serverside/redirectIfRoleDoesNotMatch";
import Layout from "@/layouts/visitor/VisitorLayout";
import styles from "@/pages/styles/Signup.module.scss";
import axios from "axios";
import { NextPageContext } from "next";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { Button, Container, Form } from "react-bootstrap";
import { v4 } from "uuid";

const authAxios = axios.create({
  baseURL: "/api/auth",
});

function SignupPembeli({ redirectionInstruction }: WithRedirectionInstruction) {
  useEffect(() => {
    RedirectClientHelper(redirectionInstruction);
  }, []);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [username, setUsername] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [address, setAddress] = useState("");
  const [signupState, setSignupState] = useState<RequestState>("none");
  const [errors, setErrors] = useState<
    { title: string; message: string; id: string }[]
  >([]);
  const router = useRouter();

  async function createUser(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();
    if (confirmPassword !== password) {
      setErrors((prevErrors) => [
        ...prevErrors,
        {
          title: "Error",
          message: "password does not match",
          id: v4(),
        },
      ]);
      return;
    }
    try {
      setSignupState("loading");
      const response = await authAxios.post("/signup", {
        email,
        username,
        password,
        phoneNumber,
        address,
      });
      console.log(response);
      setSignupState("success");
      // kalo berhasil daftar, pindah ke home page
      router.push("/");
    } catch (err) {
      setSignupState("error");
      setErrors((prevErrors) => [
        ...prevErrors,
        {
          title: "Error",
          message: err.response.data.message,
          id: v4(),
        },
      ]);
    }
  }

  return (
    <Layout title="Daftar" className="bg-secondary flex-center">
      {errors.length != 0 &&
        errors.forEach((error) => (
          <ClosableAlert
            variant="danger"
            message={error.message}
            title={error.title}
            onClose={() => {
              setErrors((prevErrors) => {
                return prevErrors.filter((err) => {
                  err.id != error.id;
                });
              });
            }}
          />
        ))}
      {signupState === "loading" ? <LoadingOverlay /> : ""}
      <Container className="flex-center my-5">
        <Form onSubmit={createUser} className={styles.form}>
          <Form.Group controlId="question" className={styles.question}>
            <Form.Label className="">
              <img src="/login/client.png" className={styles.imgclient}></img>
              <h2 className="ml-md-3">Daftar Sebagai Pembeli</h2>
            </Form.Label>
          </Form.Group>
          <StatefulInput
            state={username}
            setState={setUsername}
            icon={<i className={`fas fa-user-circle ${styles.icon}`}></i>}
            name="username"
            type="text"
            placeholder="Username"
            inputClass={styles.input}
          />
          <StatefulInput
            state={email}
            setState={setEmail}
            icon={<i className={`fas fa-envelope ${styles.icon}`}></i>}
            name="email"
            type="email"
            placeholder="Email"
            inputClass={styles.input}
          />
          <StatefulInput
            state={phoneNumber}
            setState={setPhoneNumber}
            icon={<i className={`fas fa-phone ${styles.icon}`}></i>}
            name="phone number"
            type="text"
            placeholder="Nomor Telepon"
            inputClass={styles.input}
          />
          <StatefulInput
            state={address}
            setState={setAddress}
            icon={<i className={`fas fa-home ${styles.icon}`}></i>}
            name="address"
            type="text"
            textarea
            placeholder="Alamat"
            inputClass={styles.input}
          />
          <StatefulInput
            state={password}
            setState={setPassword}
            icon={<i className={`fas fa-key ${styles.icon}`}></i>}
            name="Password"
            type="password"
            placeholder="Password"
            inputClass={styles.input}
          />
          <StatefulInput
            state={confirmPassword}
            setState={setConfirmPassword}
            icon={<i className={`fas fa-key ${styles.icon}`}></i>}
            name="Confirm-Password"
            type="password"
            placeholder="Confirm Password"
            inputClass={styles.input}
          />
          <Button variant="primary" type="submit" className={styles.submit}>
            Daftar
          </Button>
        </Form>
      </Container>
    </Layout>
  );
}

export default SignupPembeli;

export async function getServerSideProps(context: NextPageContext) {
  // kalo bukan visitor, gaboleh ke rute ini
  const redirectionInstruction = await redirectIfRoleDoesNotMatch(
    context,
    "visitor"
  );
  return {
    props: { redirectionInstruction },
  };
}
