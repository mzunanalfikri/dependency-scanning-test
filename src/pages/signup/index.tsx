import React from "react";
import Layout from "@/layouts/visitor/VisitorLayout";
import { Form } from "react-bootstrap";
import { Button } from "react-bootstrap";
import styles from "@/pages/styles/SignupChoice.module.scss";
import Link from "next/link";
import redirectIfRoleDoesNotMatch from "@/fe-services/serverside/redirectIfRoleDoesNotMatch";
import { NextPageContext } from "next";
import { useEffect } from "react";
import RedirectClientHelper from "@/fe-services/redirect/RedirectClientHelper";

function Signup({ redirectionInstruction }: WithRedirectionInstruction) {
  useEffect(() => {
    RedirectClientHelper(redirectionInstruction);
  }, []);

  return (
    <Layout title="Pilihan Daftar" className="bg-secondary">
      <div className={styles.content}>
        <Form.Group controlId="question" className={styles.question}>
          <Form.Label>Ingin mendaftar sebagai apa?</Form.Label>
        </Form.Group>

        <Link href="/signup/pembeli" passHref>
          <Button variant="primary" type="submit" className={styles.submit}>
            Pembeli
          </Button>
        </Link>
        <br></br>
        <Link href="/signup/agen" passHref>
          <Button variant="primary" type="submit" className={styles.submit}>
            Agen
          </Button>
        </Link>
      </div>
    </Layout>
  );
}

export default Signup;

export async function getServerSideProps(context: NextPageContext) {
  // kalo bukan visitor, gaboleh ke rute ini
  const redirectionInstruction = await redirectIfRoleDoesNotMatch(
    context,
    "visitor"
  );
  console.log(redirectionInstruction);
  return {
    props: { redirectionInstruction },
  };
}
