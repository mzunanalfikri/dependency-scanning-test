import StatefulModal from "@/components/StatefulModal";
import RedirectClientHelper from "@/fe-services/redirect/RedirectClientHelper";
import redirectIfRoleDoesNotMatch from "@/fe-services/serverside/redirectIfRoleDoesNotMatch";
import AgenLayout from "@/layouts/agen/AgenLayout";
import axios from "axios";
import { NextPageContext } from "next";
import React, { useEffect, useState } from "react";
import styles from "../styles/Agen.module.scss";

const publicVapidKey =
  "BMdz3IOWzogFjM9mXj02Dce-mGGlfhMx-8uh0A-Mae4aGIckXzd-7f3HtHwDOy0z9D9IGVR60bG0HXFOC7-CPcY";

function AgenDashboard({
  redirectionInstruction,
}: WithRedirectionInstruction<AuthUserData>) {
  useEffect(() => {
    RedirectClientHelper(redirectionInstruction);
  }, []);
  const [status, setStatus] = useState("");
  const [isAccepted, setIsAccepted] = useState(false);
  const [detailTransaction, setDetailTransaction] = useState<any | null>(null);
  var transactionComponent = (
    <div>
      <p className="text-center">Loading ...</p>
    </div>
  );

  // confirmation modal
  const [showModal, setShowModal] = useState(false);
  const [requestState, setRequestState] = useState<RequestState>("none");
  const [isLoaded, setIsLoaded] = useState(false);
  const onRejectModal = () => {
    setShowModal(false);
  };
  const closeOrder = async () => {
    setRequestState("loading");

    // request close order
    await axios.post("/api/agen/finish-order");
    setRequestState("success");

    setTimeout(() => window.location.reload(), 1000);
  };

  useEffect(() => {
    console.log("==================");
    axios.get("/api/auth/detail-user").then((data) => {
      const isAcc = data.data.customClaims.isAccepted;
      const isActive = data.data.customClaims.isActive;
      setIsAccepted(isAcc);
      if (isActive) setStatus("Siap Mengantar");
      else setStatus("Istirahat");
    });

    //get detail transaction
    axios.get("/api/agen/detail-transaction").then((data) => {
      setDetailTransaction(data.data);
      console.log("detil transaksi : ", data.data);
      setIsLoaded(true);
    });
  }, []);

  if (!isLoaded) {
    return (
      <AgenLayout title="Histori transasksi">
        <br /> <br /> <br /> <br />
        <br />
        <br />
        <div className="d-flex justify-content-center">
          <div className="spinner-border" role="status">
            <span className="sr-only">Loading...</span>
          </div>
        </div>
        <h3 className="text-center">Loading...</h3>
      </AgenLayout>
    );
  }

  if (!isAccepted) {
    return (
      <AgenLayout title="Agen">
        <br />
        <h2 className="text-center">Proposal anda belum disetujui.</h2>
      </AgenLayout>
    );
  }

  const cekStatus = async function () {
    axios.post("/api/agen/change-status").then((data) => {
      console.log("===== respon change ====");
      console.log(data);
    });
    if (status == "Istirahat") {
      setStatus("Siap Mengantar");
    } else {
      setStatus("Istirahat");
    }
  };

  if ("serviceWorker" in navigator) {
    navigator.serviceWorker.register("/sw.js", {
      scope: "/",
    });
  }

  Notification.requestPermission().then((res) => {
    if (res === "granted") {
      subscribe();
    }
  });
  // var notification = new Notification("Hi there!");

  // cek buat
  if (detailTransaction) {
    if (redirectionInstruction.data?.uid && redirectionInstruction.data.name) {
      transactionComponent = getDetailTransactionComponent(detailTransaction, {
        id: redirectionInstruction.data.uid,
        name: redirectionInstruction.data.name,
      });
    }
  } else {
    transactionComponent = (
      <div>
        <p className="text-center">Belum ada pesanan untuk kamu.</p>
      </div>
    );
  }

  return (
    <AgenLayout title="Agen">
      <br />
      <div className="text-center">
        <h4>
          <b>Status : {status}</b>
        </h4>
        <button className="btn btn-primary" onClick={() => cekStatus()}>
          Ubah Status
        </button>
      </div>

      <br />
      <h4>Daftar Pesanan :</h4>
      <div className={styles.daftarpesanan}>
        <div className={styles.list}>
          {transactionComponent}
          {detailTransaction && (
            <div className="text-center">
              <button
                className={`${styles.button} btn btn-primary`}
                onClick={() => setShowModal(true)}
              >
                Order Selesai
              </button>
            </div>
          )}
        </div>
      </div>
      <StatefulModal
        message="Apakah anda ingin menyelesaikan pesanan ini? <br> Pastikan pembeli sudah menerima barang dengan baik."
        showModal={showModal}
        requestState={requestState}
        loadingMessage="Menyelesaikan pesanan..."
        successMessage="Pesanan telah selesai."
        onAcceptModal={closeOrder}
        onRejectModal={onRejectModal}
      />
    </AgenLayout>
  );
}

function getDetailTransactionComponent(
  detailTransaction: any,
  ageninfo: MessageUserDetail
) {
  var photoUrl = "http://localhost:3000/user.png";
  if (detailTransaction.pembeli.photoURL) {
    photoUrl = detailTransaction.pembeli.photoURL;
  }
  return (
    <div>
      <div className={styles.content}>
        <div>
          <img
            src={photoUrl}
            alt="Profile Picture"
            className={`${styles.profpic} img-responsive img-rounded`}
          ></img>
          <span className={styles.displayName}>
            <strong>{detailTransaction.pembeli.displayName}</strong>
          </span>
        </div>
        <hr className="small-margin-top"></hr>
        <p>Alamat pembeli : {detailTransaction.pembeli.uid}</p>
        <p>Kontak Pembeli : {detailTransaction.pembeli.phoneNumber}</p>
        <p>Pasar : {detailTransaction.pasar_name}</p>
        <p>Alamat Pasar : {detailTransaction.pasar_address}</p>
        <p> Total harga : {detailTransaction.totalHarga}</p>
        <p> Jasa : {detailTransaction.totalHarga * 0.1}</p>
        <p>Daftar barang :</p>
      </div>
      <table className="table table-striped table-bordered table-success">
        <thead>
          <tr>
            <th>#</th>
            <th>Nama Barang</th>
            <th>Jumlah</th>
            <th>Estimasi Harga</th>
          </tr>
        </thead>
        <tbody>
          {detailTransaction.arrBarang.map((value: any, i: any) => {
            return (
              <tr>
                <td>{i + 1}</td>
                <td>{value.name}</td>
                <td>{value.qty}</td>
                <td>{value.price}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

const urlBase64ToUint8Array = (base64String: string) => {
  const padding = "=".repeat((4 - (base64String.length % 4)) % 4);
  const base64 = (base64String + padding)
    .replace(/\-/g, "+")
    .replace(/_/g, "/");

  const rawData = window.atob(base64);
  const outputArray = new Uint8Array(rawData.length);

  for (let i = 0; i < rawData.length; ++i) {
    outputArray[i] = rawData.charCodeAt(i);
  }
  return outputArray;
};

async function subscribe() {
  if (!("serviceWorker" in navigator)) return;
  const registration = await navigator.serviceWorker.ready;
  // Subscribe to push notifications
  const subscription = await registration.pushManager.subscribe({
    userVisibleOnly: true,
    applicationServerKey: urlBase64ToUint8Array(publicVapidKey),
  });
  console.log("TES TES SUBSCRIPTION");
  console.log(subscription);

  axios.post("api/agen/subscribe-notification", subscription);
}

export async function getServerSideProps(context: NextPageContext) {
  const redirectionInstruction = await redirectIfRoleDoesNotMatch(
    context,
    "agen"
  );
  console.log("redirection instruction");
  console.log(redirectionInstruction.data);
  return {
    props: {
      redirectionInstruction,
    },
  };
}

export default AgenDashboard;
