import React, { useEffect, useState } from "react";
import AgenLayout from "@/layouts/agen/AgenLayout";
import axios from "axios";
import styles from "../styles/Agen.module.scss";
import detailTransaction from "../api/agen/detail-transaction";

function AgenProfile() {
    const [allTransaction, setAllTransaction] = useState<Array<any>>([]);
    const [isLoaded, setIsLoaded] = useState(false);
    const photoUrl = "http://localhost:3000/user.png";

    useEffect(() => {
        axios.get("/api/agen/transaction-history").then(res => {
            setAllTransaction(res.data)
            setIsLoaded(true)
        })
    }, [])

    if (!isLoaded){
        return (
            <AgenLayout title="Histori transasksi" >
                <br/> <br/> <br/> <br/><br/><br/>
                <div className="d-flex justify-content-center">
                    <div className="spinner-border" role="status">
                        <span className="sr-only">Loading...</span>
                    </div>
                </div>
                <h3 className="text-center">Loading...</h3>
            </AgenLayout>
        );
    }

    if (allTransaction.length === 0){
        return (<AgenLayout title="Histori transaksi" >
            <br/>
            <h4> Daftar transaksi anda : </h4>
            <div className={styles.daftarpesanan}>
            <h4 className="text-center">Pesanan anda kosong.</h4>
      </div>
        </AgenLayout>)
    }
    
    return (
        <AgenLayout title="Histori transaksi" >
            <br/>
            <h4> Daftar transaksi anda : </h4>
            <div className={styles.daftarpesanan}>
            {allTransaction.map(detailTransaction => {
                return (
                    <div className={styles.list}>
                        <div className={styles.content}>
                        <div>
                            <img
                            src={photoUrl}
                            alt="Profile Picture"
                            className={`${styles.profpic} img-responsive img-rounded`}
                            ></img>
                            <span className={styles.displayName}>
                            <strong>{detailTransaction.pembeli.displayName}</strong>
                            </span>
                            
                        </div>
                        <hr className="small-margin-top"></hr>
                        <p className="text-center">{detailTransaction.tanggal} </p>
                        <p >Alamat pembeli : </p>
                        <p>Kontak Pembeli : {detailTransaction.pembeli.phoneNumber}</p>
                        <p>Pasar : {detailTransaction.pasar_name}</p>
                        <p>Alamat Pasar : {detailTransaction.pasar_address}</p>
                        <p> Total harga : {detailTransaction.totalHarga}</p>
                        <p> Jasa : {detailTransaction.totalHarga * 0.1}</p>
                        <p>Daftar barang :</p>
                        </div>
                        <table className="table table-striped table-bordered table-success">
                        <thead>
                            <tr>
                            <th>#</th>
                            <th>Nama Barang</th>
                            <th>Jumlah</th>
                            <th>Estimasi Harga</th>
                            </tr>
                        </thead>
                        <tbody>
                            {detailTransaction.arrBarang.map((value: any, i: any) => {
                            return (
                                <tr>
                                <td>{i + 1}</td>
                                <td>{value.name}</td>
                                <td>{value.qty}</td>
                                <td>{value.price}</td>
                                </tr>
                            );
                            })}
                        </tbody>
                        </table>
                    </div>
                )
            })}
      </div>
        </AgenLayout>
    );
}

export default AgenProfile;