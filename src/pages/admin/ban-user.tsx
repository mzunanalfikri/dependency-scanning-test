import admin from "@/database/admin";
import RedirectClientHelper from "@/fe-services/redirect/RedirectClientHelper";
import redirectIfRoleDoesNotMatch from "@/fe-services/serverside/redirectIfRoleDoesNotMatch";
import AdminLayout from "@/layouts/admin/AdminLayout";
import axios from "axios";
import { NextPageContext } from "next";
import React, { useEffect, useState } from "react";
import { Button, Table } from "react-bootstrap";
import { v4 } from "uuid";

const axiosInstance = axios.create({
  baseURL: "/api/admin",
});

function BanUserPage({
  initialUsers,
  redirectionInstruction,
}: { initialUsers: string } & WithRedirectionInstruction) {
  useEffect(() => {
    RedirectClientHelper(redirectionInstruction);
  }, []);
  const [users, setUsers] = useState<admin.auth.UserRecord[]>(
    JSON.parse(initialUsers)
  );

  function toggleUserBanStatus(uid: string) {
    setUsers(
      users.map((user) => {
        if (user.uid == uid) {
          const currentBanStatus: boolean = user.customClaims?.banned ?? false;
          return {
            ...user,
            customClaims: {
              ...user.customClaims,
              banned: !currentBanStatus,
            },
          };
        }
        return user;
      })
    );
  }

  async function banUser(uid: string) {
    try {
      const response = await axiosInstance.post<ResponseMessageProps>("/ban", {
        uid,
      });
      console.log(response.data);
      if (response.data.type == "error") {
        alert("error on server side");
      } else {
        toggleUserBanStatus(uid);
        alert("successfully banned the user");
      }
    } catch (err) {
      alert("ban not successfull");
    }
  }

  async function unBanUser(uid: string) {
    try {
      const response = await axiosInstance.post<ResponseMessageProps>(
        "/unban",
        {
          uid,
        }
      );
      console.log(response.data);
      if (response.data.type == "error") {
        alert("error on server side");
      } else {
        toggleUserBanStatus(uid);
        alert("successfully unbanned the user");
      }
    } catch (err) {
      alert("unban successfull");
    }
  }

  return (
    <AdminLayout title="Ban User">
      <h1>Ban User</h1>
      <Table>
        <thead>
          <tr>
            <th>#</th>
            <th>Email</th>
            <th>Role</th>
            <th>Ban/Unban</th>
          </tr>
        </thead>
        <tbody>
          {users.map((user, idx) => (
            <tr key={v4()}>
              <td>{idx + 1}</td>
              <td>{user.email}</td>
              <td>{user.customClaims?.role ?? "pembeli"}</td>
              <td>
                <Button
                  onClick={() => {
                    user.customClaims?.banned
                      ? unBanUser(user.uid)
                      : banUser(user.uid);
                  }}
                  variant={user.customClaims?.banned ? "success" : "danger"}
                  title={user.customClaims?.banned ? "unban user" : "ban user"}
                >
                  <i
                    className={`fas ${
                      user.customClaims?.banned ? "fa-check" : "fa-hammer "
                    } lead`}
                  ></i>
                </Button>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </AdminLayout>
  );
}

export default BanUserPage;

export async function getServerSideProps(context: NextPageContext) {
  const redirectionInstruction = await redirectIfRoleDoesNotMatch(
    context,
    "admin"
  );
  const users = await admin.auth().listUsers(10);
  return {
    props: {
      initialUsers: JSON.stringify(users.users),
      redirectionInstruction,
    },
  };
}
