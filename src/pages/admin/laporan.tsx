import { getCollection } from "@/database/firebase";
import RedirectClientHelper from "@/fe-services/redirect/RedirectClientHelper";
import redirectIfRoleDoesNotMatch from "@/fe-services/serverside/redirectIfRoleDoesNotMatch";
import AdminLayout from "@/layouts/admin/AdminLayout";
import LaporanData from "@/models/Laporan";
import { NextPageContext } from "next";
import React, { useEffect } from "react";

interface LaporanUserPageProps {
  laporanDatas: LaporanData[];
}

function LaporanUser({
  laporanDatas,
  redirectionInstruction,
}: LaporanUserPageProps & WithRedirectionInstruction) {
  useEffect(() => {
    RedirectClientHelper(redirectionInstruction);
  }, []);
  return (
    <AdminLayout title="Laporan">
      {laporanDatas.map((laporan) => (
        <></>
      ))}
    </AdminLayout>
  );
}

export async function getServerSideProps(context: NextPageContext) {
  const redirectionInstruction = await redirectIfRoleDoesNotMatch(
    context,
    "admin"
  );
  const laporanCollection = getCollection("laporan");
  const laporanDatas = (await laporanCollection.get()).docs.map((doc) => ({
    id: doc.id,
    ...doc.data(),
  }));
  return {
    props: {
      laporanDatas,
      redirectionInstruction,
    },
  };
}

export default LaporanUser;
