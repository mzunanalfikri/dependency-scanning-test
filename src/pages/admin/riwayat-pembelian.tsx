import RedirectClientHelper from "@/fe-services/redirect/RedirectClientHelper";
import redirectIfRoleDoesNotMatch from "@/fe-services/serverside/redirectIfRoleDoesNotMatch";
import AdminLayout from "@/layouts/admin/AdminLayout";
import { NextPageContext } from "next";
import Head from "next/head";
import { useEffect } from "react";

export default function RiwayatPembelian({
  redirectionInstruction,
}: WithRedirectionInstruction) {
  useEffect(() => {
    RedirectClientHelper(redirectionInstruction);
  }, []);

  return (
    <AdminLayout title="Admin Riwayat Pembelian">
      <Head>
        <meta
          name="viewport"
          content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"
        />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Admin Page</title>
        <link rel="icon" href="/landing_page/logotitip1.svg" />
      </Head>

      <main className="main">
        <h1>Selamat datang Admin !</h1>
        <h1>LALALALALALALALALALALALLALALALALAL</h1>
      </main>
    </AdminLayout>
  );
}

export async function getServerSideProps(context: NextPageContext) {
  // kalo bukan visitor, gaboleh ke rute ini
  const redirectionInstruction = await redirectIfRoleDoesNotMatch(
    context,
    "admin"
  );
  return {
    props: { redirectionInstruction },
  };
}
