import RedirectClientHelper from "@/fe-services/redirect/RedirectClientHelper";
import redirectIfRoleDoesNotMatch from "@/fe-services/serverside/redirectIfRoleDoesNotMatch";
import AdminLayout from "@/layouts/admin/AdminLayout";
import { NextPageContext } from "next";
import React, { useEffect } from "react";

function Index({
  redirectionInstruction,
}: WithRedirectionInstruction<AuthUserData>) {
  useEffect(() => {
    RedirectClientHelper(redirectionInstruction);
  }, []);
  return (
    <AdminLayout title="Admin Home">
      <h1>
        Halo Admin {redirectionInstruction && redirectionInstruction.data?.name}
        !
      </h1>
    </AdminLayout>
  );
}

export async function getServerSideProps(context: NextPageContext) {
  const redirectionInstruction = await redirectIfRoleDoesNotMatch(
    context,
    "admin"
  );
  const props: WithRedirectionInstruction = {
    redirectionInstruction,
  };
  return {
    props,
  };
}

export default Index;
