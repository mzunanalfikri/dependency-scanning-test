import StatefulModal from "@/components/StatefulModal";
import PasarFeService from "@/fe-services/pasar/pasar";
import Layout from "@/layouts/admin/AdminLayout";
import Pasar from "@/models/Pasar";
import axios from "axios";
import React, { useEffect, useState } from "react";
import { Button, Form, Modal, Table } from "react-bootstrap";
import RedirectClientHelper from "@/fe-services/redirect/RedirectClientHelper";
import redirectIfRoleDoesNotMatch from "@/fe-services/serverside/redirectIfRoleDoesNotMatch";
import { NextPageContext } from "next";

export default function PengelolaPasar({
  redirectionInstruction,
}: WithRedirectionInstruction) {
  useEffect(() => {
    RedirectClientHelper(redirectionInstruction);
  }, []);
  const [listDataEntry, setListDataEntry] = useState<
    (firebase.default.User & { customClaims: any })[]
  >([]);
  const [formModal, setFormModal] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [requestState, setRequestState] = useState<RequestState>("none");
  const [errorMessage, setErrorMessage] = useState("");
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [idPasar, setIdPasar] = useState("");
  const [listPasar, setListPasar] = useState<Pasar[]>([]);

  useEffect(() => {
    axios.get("/api/pengelola-pasar/list").then((res) => {
      console.log(res.data);
      setListDataEntry(res.data);
    });
    PasarFeService.getAllPasar().then((data) => {
      setListPasar(data);
    });
  }, []);

  const onShowModal = (e: React.FormEvent) => {
    e.preventDefault();
    setFormModal(false);
    setShowModal(true);
  };
  const onAcceptModal = () => {
    setRequestState("loading");

    const payload = {
      username,
      email,
      password,
      phoneNumber,
      idPasar,
    };

    axios.post("/api/pengelola-pasar/add-account", payload).then((res) => {
      if (res.status == 200) {
        setRequestState("success");
        setTimeout(() => window.location.reload(), 1000);
      } else {
        setErrorMessage(res.data);
        setTimeout(() => setRequestState("error"), 1000);
      }
    });
  };
  const onRejectModal = () => {
    setShowModal(false);
  };

  useEffect(() => {
    if (requestState === "success" || requestState === "error") {
      setTimeout(() => {
        setShowModal(false);
      }, 1000); // wait 1 sec before closing the modal
    }
  }, [requestState]);

  useEffect(() => {
    if (showModal === false) {
      setTimeout(() => {
        setRequestState("none");
      }, 200); // add delay because there is animation
    }
  }, [showModal]);

  return (
    <Layout title="Pengelola Pasar">
      <div className="text-center">
        <h2>Halaman Pengelola Pasar</h2>
        <button className="btn btn-primary" onClick={() => setFormModal(true)}>
          Buat Akun Pengelola Pasar
        </button>
      </div>
      <h3>Daftar Pengelola Pasar</h3>

      <Table striped bordered>
        <thead>
          <th>#</th>
          <th>Nama</th>
          <th>Email</th>
          <th>Nomor Hp</th>
          <th>Pasar</th>
        </thead>
        <tbody>
          {listDataEntry.map((value, i) => {
            return (
              <tr>
                <td>{i + 1}</td>
                <td>{value.displayName}</td>
                <td>{value.email}</td>
                <td>{value.phoneNumber}</td>
                <td>{value.customClaims.namaPasar}</td>
              </tr>
            );
          })}
        </tbody>
      </Table>

      <Modal
        show={formModal}
        onHide={() => setFormModal(false)}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Buat Akun Pengelola Pasar
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={onShowModal}>
            <Form.Group controlId="name">
              <Form.Label>Username</Form.Label>
              <Form.Control
                type="text"
                placeholder="Masukkan Nama Pengelola Pasar"
                value={username}
                onChange={(e) => setUsername(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group controlId="email">
              <Form.Label>Email</Form.Label>
              <Form.Control
                type="email"
                placeholder="Kota"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group controlId="phoneNumber">
              <Form.Label>Nomor HP</Form.Label>
              <Form.Control
                type="text"
                pattern="(0|\+62)[0-9]{7,13}"
                placeholder="Nomor HP"
                value={phoneNumber}
                onChange={(e) => setPhoneNumber(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group controlId="password">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Minimal 8 karakter."
                pattern=".{8,}"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group controlId="pasar">
              <Form.Label>Pasar</Form.Label>
              {/* <Form.Control type="idPasar" placeholder="Password" value={password} onChange={e => setPassword(e.target.value)} required/> */}
              <Form.Control
                value={idPasar}
                onChange={(e) => setIdPasar(e.target.value)}
                as="select"
                required
              >
                <option value="">-</option>
                {listPasar.map((value) => {
                  return <option value={value.id}>{value.name}</option>;
                })}
              </Form.Control>
            </Form.Group>

            <div className="text-center">
              <Button variant="primary" type="submit">
                Submit
              </Button>
            </div>
          </Form>
        </Modal.Body>
      </Modal>

      <StatefulModal
        message={`Apakah anda yakin ingin membuat akun pengelola pasar?`}
        showModal={showModal}
        requestState={requestState}
        loadingMessage="Menyimpan..."
        successMessage="Akun pengelola pasar berhasil ditambahkan."
        onAcceptModal={onAcceptModal}
        onRejectModal={onRejectModal}
        errorMessage={errorMessage}
      />
    </Layout>
  );
}

export async function getServerSideProps(context: NextPageContext) {
  // kalo bukan visitor, gaboleh ke rute ini
  const redirectionInstruction = await redirectIfRoleDoesNotMatch(
    context,
    "admin"
  );
  return {
    props: { redirectionInstruction },
  };
}
