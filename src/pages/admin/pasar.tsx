import StatefulModal from "@/components/StatefulModal";
import PasarFeService from "@/fe-services/pasar/pasar";
import AdminLayout from "@/layouts/admin/AdminLayout";
import Pasar from "@/models/Pasar";
import React, { useEffect, useState } from "react";
import { Button, Col, Form, Modal, Table } from "react-bootstrap";
import RedirectClientHelper from "@/fe-services/redirect/RedirectClientHelper";
import redirectIfRoleDoesNotMatch from "@/fe-services/serverside/redirectIfRoleDoesNotMatch";
import { NextPageContext } from "next";

export default function PasarPage({
  redirectionInstruction,
}: WithRedirectionInstruction) {
  useEffect(() => {
    RedirectClientHelper(redirectionInstruction);
  }, []);

  const [name, setName] = useState("");
  const [city, setCity] = useState("");
  const [address, setAddress] = useState("");
  const [longitude, setLognitude] = useState(0);
  const [latitude, setLatitude] = useState(0);
  const [image, setImage] = useState(null);

  const [modalShow, setModalShow] = React.useState(false);
  const [listPasar, setListPasar] = React.useState<Pasar[]>([]);

  React.useEffect(() => {
    PasarFeService.getAllPasar().then((data) => {
      setListPasar(data);
    });
  }, []);

  const [showModal, setShowModal] = useState(false);
  const [requestState, setRequestState] = useState<RequestState>("none");
  const [errorMessage, setErrorMessage] = useState("");
  const onShowModal = (e: React.FormEvent) => {
    e.preventDefault();
    console.log(name, city, address, longitude, latitude);
    setModalShow(false);
    setShowModal(true);
  };
  const onAcceptModal = async () => {
    try {
      setRequestState("loading");
      const imgUrl = await PasarFeService.uploadImagePasar(image);
      console.log(imgUrl);
      const payload = {
        name: name,
        city: city,
        address: address,
        longitude: longitude,
        latitude: latitude,
        image: imgUrl,
      };

      PasarFeService.addPasar(payload).then((res) => {
        if (res.status == 200) {
          setTimeout(() => setRequestState("success"), 1000);
          window.location.reload();
        } else {
          setErrorMessage(res.message);
          setTimeout(() => setRequestState("error"), 1000);
        }
      });
    } catch (error) {
      alert("Terjadi kesalahan, silahkan isi form kembali.");
      setRequestState("error");
    }
  };
  const onRejectModal = () => {
    setShowModal(false);
  };

  useEffect(() => {
    if (requestState === "success" || requestState === "error") {
      setTimeout(() => {
        setShowModal(false);
      }, 1000); // wait 1 sec before closing the modal
    }
  }, [requestState]);

  useEffect(() => {
    if (showModal === false) {
      setTimeout(() => {
        setRequestState("none");
      }, 200); // add delay because there is animation
    }
  }, [showModal]);

  return (
    <AdminLayout title="Admin Pasar">
      <div className="text-center">
        <h2>Halaman Pasar</h2>
        <button className="btn btn-primary" onClick={() => setModalShow(true)}>
          Buat Pasar Baru
        </button>
      </div>
      <h3>Daftar Pasar</h3>

      <Table striped bordered>
        <thead>
          <th>#</th>
          <th>Nama</th>
          <th>Kota</th>
          <th>Alamat</th>
        </thead>
        <tbody>
          {listPasar.map((value, i) => {
            return (
              <tr>
                <td>{i + 1}</td>
                <td>{value.name}</td>
                <td>{value.city}</td>
                <td>{value.address}</td>
              </tr>
            );
          })}
        </tbody>
      </Table>

      <p> </p>

      {/* <AddPasarModal
        show={modalShow}
        onHide={() => setModalShow(false)}
      /> */}

      <Modal
        show={modalShow}
        onHide={() => setModalShow(false)}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Form Input Pasar Baru
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={onShowModal}>
            <Form.Group controlId="name">
              <Form.Label>Nama Pasar</Form.Label>
              <Form.Control
                type="text"
                placeholder="Masukkan Nama Pasar"
                value={name}
                onChange={(e) => setName(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group controlId="city">
              <Form.Label>Kota</Form.Label>
              <Form.Control
                type="text"
                placeholder="Masukkan Kota (bukan provinsi)"
                value={city}
                onChange={(e) => setCity(e.target.value)}
              />
            </Form.Group>

            <Form.Group controlId="address">
              <Form.Label>Alamat</Form.Label>
              <Form.Control
                type="text"
                placeholder="Masukkan alamat lengkap"
                value={address}
                onChange={(e) => setAddress(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Row>
              <Col>
                <Form.Group controlId="latitude">
                  <Form.Label>Koordinat Pasar (Latitude)</Form.Label>
                  <Form.Control
                    type="number"
                    step="any"
                    placeholder="Latitude diantara -90 dan 90."
                    min="-90"
                    max="90"
                    value={latitude}
                    onChange={(e) => setLatitude(Number(e.target.value))}
                    required
                  />
                </Form.Group>
              </Col>

              <Col>
                <Form.Group controlId="longitude">
                  <Form.Label>Koordinat Pasar (longitude)</Form.Label>
                  <Form.Control
                    type="number"
                    step="any"
                    placeholder="Lognitude diantara -180 dan 180."
                    value={longitude}
                    min="-180"
                    max="180"
                    onChange={(e) => setLognitude(Number(e.target.value))}
                    required
                  />
                </Form.Group>
              </Col>
            </Form.Row>

            <Form.Group>
              <Form.File
                id="file"
                label="Masukkan gambar/logo pasar."
                onChange={(e: any) => {
                  if (!!e.target.files) {
                    setImage(e.target.files[0]);
                  }
                }}
              />
            </Form.Group>

            <div className="text-center">
              <Button variant="primary" type="submit">
                Submit
              </Button>
            </div>
          </Form>
        </Modal.Body>
      </Modal>

      <StatefulModal
        message={`Apakah anda yakin ingin menambahkan pasar?`}
        showModal={showModal}
        requestState={requestState}
        loadingMessage="Menyimpan..."
        successMessage="Pasar telah berhasil disimpan"
        onAcceptModal={onAcceptModal}
        onRejectModal={onRejectModal}
        errorMessage={errorMessage}
      />
    </AdminLayout>
  );
}
export async function getServerSideProps(context: NextPageContext) {
  // kalo bukan visitor, gaboleh ke rute ini
  const redirectionInstruction = await redirectIfRoleDoesNotMatch(
    context,
    "admin"
  );
  return {
    props: { redirectionInstruction },
  };
}
