import admin from "@/database/admin";
import AdminLayout from "@/layouts/admin/AdminLayout";
import axios from "axios";
import _ from "lodash";
import { NextPageContext } from "next";
import React, { useEffect, useState } from "react";
import { Button, Form, Table } from "react-bootstrap";
import { v4 } from "uuid";
import RedirectClientHelper from "@/fe-services/redirect/RedirectClientHelper";
import redirectIfRoleDoesNotMatch from "@/fe-services/serverside/redirectIfRoleDoesNotMatch";

const axiosInstance = axios.create({
  baseURL: "/api/auth/set-role",
});

function SetRole({
  initialUsers,
  redirectionInstruction,
}: SetRoleProps & WithRedirectionInstruction) {
  useEffect(() => {
    RedirectClientHelper(redirectionInstruction);
  }, []);
  const [users, setUsers] = useState<admin.auth.UserRecord[]>(
    JSON.parse(initialUsers)
  );

  return (
    <AdminLayout title="Set Role">
      <h1>Set user role</h1>
      <Table>
        <thead>
          <tr>
            <th>#</th>
            <th>Email</th>
            <th>Role</th>
            <th>Set Role</th>
            <th>Save</th>
          </tr>
        </thead>
        <tbody>
          {users.map((user, idx) => (
            <tr key={v4()}>
              <td>{idx}</td>
              <td>{user.email}</td>
              <td>{user.customClaims?.role ?? "pembeli"}</td>
              <RoleSetter
                uid={user.uid}
                initialRole={user.customClaims?.role ?? "pembeli"}
                errorCallback={() => {
                  alert("Terjadi error");
                }}
                successCallback={() => {
                  alert("Sukses!");
                }}
              />
            </tr>
          ))}
        </tbody>
      </Table>
    </AdminLayout>
  );
}

const RoleOptions: Role[] = ["admin", "agen", "pengelola-pasar", "pembeli"];

function RoleSetter({
  uid,
  initialRole,
  successCallback,
  errorCallback,
}: {
  uid: string;
  initialRole: Role;
  successCallback: Function;
  errorCallback: Function;
}) {
  const [selected, setSelected] = useState(initialRole);

  useEffect(() => {}, [selected]);

  async function requestSetRole(uid: string, role: Role) {
    try {
      const response = await axiosInstance.post("/", {
        uid,
        role,
      });
      successCallback();
    } catch (err) {
      errorCallback();
    }
  }

  return (
    <>
      <td>
        <Form.Control
          as="select"
          value={selected}
          onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
            setSelected(e.target.value as Role);
          }}
          custom
        >
          {RoleOptions.map((option) => (
            <option value={option} key={option}>
              {_.capitalize(option)}
            </option>
          ))}
        </Form.Control>
      </td>
      <td>
        <Button
          onClick={() => {
            requestSetRole(uid, selected);
          }}
        >
          <i className="fas fa-check text-white"></i>
        </Button>
      </td>
    </>
  );
}

interface SetRoleProps {
  initialUsers: string;
}

export default SetRole;

export async function getServerSideProps(context: NextPageContext) {
  // kalo bukan admin, gaboleh ke rute ini
  const redirectionInstruction = await redirectIfRoleDoesNotMatch(
    context,
    "admin"
  );
  if (redirectionInstruction.redirectFrom !== "none") {
    return {
      props: { redirectionInstruction },
    };
  }
  const users = await admin.auth().listUsers(10);
  return {
    props: {
      redirectionInstruction,
      initialUsers: JSON.stringify(users.users),
    },
  };
}
