import ClosableAlert from "@/components/ClosableAlert";
import admin from "@/database/admin";
import RedirectClientHelper from "@/fe-services/redirect/RedirectClientHelper";
import redirectIfRoleDoesNotMatch from "@/fe-services/serverside/redirectIfRoleDoesNotMatch";
import AdminLayout from "@/layouts/admin/AdminLayout";
import ProposalAgen, { ProposalStatus } from "@/models/ProposalAgen";
import axios from "axios";
import { NextPageContext } from "next";
import React, { useEffect, useState } from "react";
import { Button, Table } from "react-bootstrap";
import { v4 } from "uuid";

const axiosInstance = axios.create({
  baseURL: "/api/auth/set-role",
});

interface LamaranAgenProps {
  initialDatas: ProposalAgen[];
}

async function setStatus(
  id: string,
  status: ProposalStatus,
  successCallback?: Function,
  errorCallback?: Function
) {
  try {
    await axios.post("/api/admin/lamaran", {
      id,
      status,
    });
    if (successCallback) {
      successCallback(id);
    }
  } catch (err) {
    if (errorCallback) {
      errorCallback(err);
    }
  }
}

function LamaranAgen({
  initialDatas,
  redirectionInstruction,
}: LamaranAgenProps & WithRedirectionInstruction) {
  useEffect(() => {
    RedirectClientHelper(redirectionInstruction);
  }, []);
  const [proposalDatas, setProposalDatas] = useState(initialDatas);
  const [error, setError] = useState<null | string>(null);
  const [requestState, setRequestState] = useState<RequestState>("none");
  const removeProposal = (id: string) =>
    setProposalDatas(proposalDatas.filter((data) => data.id != id));
  const successCallback = (id: string) => {
    setRequestState("success");
    removeProposal(id);
  };
  const errorCallback = (err: any) => {
    setError(err.response.data.message);
  };

  return (
    <AdminLayout title="Lamaran Agen">
      {requestState == "success" ? (
        <ClosableAlert
          title="Berhasil"
          message={"Perubahan berhasil dilakukan"}
          variant="success"
        />
      ) : (
        ""
      )}
      {error ? (
        <ClosableAlert title="Error" message={error} variant="danger" />
      ) : (
        ""
      )}
      <Table striped bordered>
        <thead>
          <th>#</th>
          <th>Email</th>
          <th>FullName</th>
          <th>Phone</th>
          <th>Proposal</th>
          <th>Accept?</th>
        </thead>
        <tbody>
          {proposalDatas
            .filter(
              (proposalData) =>
                proposalData.status != "accepted" &&
                proposalData.status != "declined"
            )
            .map((proposal, i) => {
              return (
                <tr key={v4()}>
                  <td>{i + 1}</td>
                  <td>{proposal.email}</td>
                  <td>{proposal.fullName}</td>
                  <td>{proposal.phoneNumber}</td>
                  <td>{proposal.proposal}</td>
                  <td>
                    <Button
                      onClick={async () => {
                        await setStatus(
                          proposal.id,
                          "accepted",
                          successCallback,
                          errorCallback
                        );
                      }}
                    >
                      <i className="fas fa-check text-white"></i>
                    </Button>
                    <Button
                      onClick={() => {
                        setStatus(
                          proposal.id,
                          "declined",
                          successCallback,
                          errorCallback
                        );
                      }}
                      variant="danger"
                      className="ml-3"
                    >
                      <i className="fas fa-times text-white"></i>
                    </Button>
                  </td>
                </tr>
              );
            })}
        </tbody>
      </Table>
    </AdminLayout>
  );
}

export async function getServerSideProps(context: NextPageContext) {
  const redirectionInstruction = await redirectIfRoleDoesNotMatch(
    context,
    "admin"
  );
  const proposalAgenDocs = (
    await admin
      .firestore()
      .collection("proposal-agen")
      .where("status", "==", "submitted")
      .get()
  ).docs;
  const proposalDatas: ProposalAgen[] = proposalAgenDocs.map((proposal) => {
    return { ...proposal.data(), id: proposal.id };
  }) as ProposalAgen[];

  const props: LamaranAgenProps & WithRedirectionInstruction = {
    initialDatas: proposalDatas,
    redirectionInstruction,
  };
  return {
    props,
  };
}

export default LamaranAgen;
