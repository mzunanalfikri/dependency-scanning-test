import admin from "@/database/admin";
import { auth } from "firebase-admin";
import { NextApiRequest, NextApiResponse } from "next";

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const { body, method } = req;

  switch (method) {
    case "GET":
      try {
        const token = req.cookies.idtoken;
        if (token) {
          //   console.log("dibawah ini tokennya");
          //   console.log(token);
          var id = (await admin.auth().verifyIdToken(token)).uid;
          const data = await admin.auth().getUser(id);
          // console.log(data)
          res.status(200).send(data);
        }
      } catch (err) {
        res.status(400).send("Failed to retrieve");
      }
    case "POST":
      try {
        const newUserData = req.body;
        const token = req.cookies.idtoken;
        if (token) {
          var id = (await admin.auth().verifyIdToken(token)).uid;
          if (newUserData) {
            const dataa = await admin.auth().updateUser(id, {
              displayName: newUserData.newName,
              email: newUserData.newEmail,
              password: newUserData.newPassword,
              phoneNumber: newUserData.newPhoneNumber,
              photoURL: newUserData.newDP,
            });
            res.status(201).send(dataa);
          }
          // res.send(dataa)
        }
      } catch (err) {
        res.status(400).send("Error when post");
      }
  }
};
