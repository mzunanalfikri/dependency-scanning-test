import admin from "@/database/admin";
import { auth } from "firebase-admin";
import { NextApiRequest, NextApiResponse } from "next";

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const { query, method } = req;

  switch (method) {
    case "GET":
        let listUser = (await admin.auth().listUsers()).users
        if (query.role){
            listUser = listUser.filter((o) => {
                if (!o.customClaims) return;
                if ("customClaims" in o && "role" in o.customClaims) {
                  return o.customClaims.role === query.role;
                }
                return false;
              });
        }
        res.status(200).send(listUser)
  }
};
