import admin, { verifyIdToken } from "@/database/admin";
import firebase from "@/database/firebase";
import createResponse from "@/util/createResponse";
import { NextApiRequest, NextApiResponse } from "next";

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const { body, method } = req;

  switch (method) {
    case "POST":
      try {
        const { email, password, username, address } = body;
        let phoneNumber = String(body.phoneNumber);
        if (phoneNumber[0] == "0") {
          phoneNumber = "+62" + phoneNumber.substr(1, phoneNumber.length - 1);
        }
        const createdUser = await admin.auth().createUser({
          email,
          password,
          displayName: username,
          phoneNumber,
        });
        if (address) {
          await admin.auth().setCustomUserClaims(createdUser.uid, {
            address,
          });
        }
        console.log("creating acc");
        const userCred = await firebase
          .auth()
          .signInWithEmailAndPassword(email, password);
        const user = await admin.auth().getUser(createdUser.uid);
        console.log(user.customClaims);

        res.status(200).send(
          createResponse("Successfully signed up", "success", {
            email,
          })
        );
      } catch (err) {
        res.status(400).send(createResponse(err.errorInfo.message, "error"));
      }
      break;
    default:
      res.status(200).send("not e");
  }
};
