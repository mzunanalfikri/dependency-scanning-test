import admin from "@/database/admin";
import { auth } from "firebase-admin";
import { NextApiRequest, NextApiResponse } from "next";

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const { query, method } = req;

  switch (method) {
    case "GET":
        let listUser = (await admin.auth().listUsers()).users
        listUser = listUser.filter((o) => {
            if (!o.customClaims) return;
            if ("customClaims" in o && "role" in o.customClaims) {
                return o.customClaims.role === "agen";
            }
            return false;
            });
        

        listUser.forEach(list => {
            admin.auth().setCustomUserClaims(list.uid, { 
                role: "agen",
                isAccepted: true,
                isActive: true
            });
        })

        res.status(200).send(listUser)
  }
};
