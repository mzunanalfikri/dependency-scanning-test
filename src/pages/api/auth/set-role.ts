import { NextApiRequest, NextApiResponse } from "next";
import admin, { verifyIdToken } from "@/database/admin";
import createResponse from "@/util/createResponse";

// todo use admin auth middleware
export default async (req: NextApiRequest, res: NextApiResponse) => {
  const { body, method } = req;

  switch (method) {
    case "POST":
      try {
        const { role, uid }: { role: Role; uid: string } = body;
        await admin.auth().setCustomUserClaims(uid, { role });
        res
          .status(200)
          .send(createResponse("Telah sukses mengganti role", "success"));
      } catch (err) {
        res.status(400).send(createResponse(err.errorInfo.message, "error"));
      }
      break;
    default:
      res.status(200).send("not e");
  }
};
