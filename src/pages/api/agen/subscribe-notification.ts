import admin from "@/database/admin";
import { NextApiRequest, NextApiResponse } from "next";

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const { body, method } = req;

  switch (method) {
    case "POST":
      try {
        const token = req.cookies.idtoken;
        var id = (await admin.auth().verifyIdToken(token)).uid;
        var data = await admin.auth().getUser(id);
        await admin.auth().setCustomUserClaims(data.uid, {
          ...data.customClaims,
          notification: body,
        });
        res.status(200).send("ok");
      } catch (err) {
        console.error(err);
        res.status(400).send("not ok");
      }
  }
};
