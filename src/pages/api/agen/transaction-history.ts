import admin from "@/database/admin";
import { NextApiRequest, NextApiResponse } from "next";
import TransactionService from "@/api-services/transaction";
import PasarService from "@/api-services/pasar";
import DetilTransaksi from "@/models/DetailTransaksi";
import Pasar from "@/models/Pasar";

function getTanggal(time:any){
  var date = new Date(time * 1000);
  var tahun = date.getFullYear();
  var bulan:any = date.getMonth();
  var tanggal = date.getDate();
  var hari:any = date.getDay();
  var jam = date.getHours();
  var menit = date.getMinutes();
  var detik = date.getSeconds();
  switch(hari) {
  case 0: hari = "Minggu"; break;
  case 1: hari = "Senin"; break;
  case 2: hari = "Selasa"; break;
  case 3: hari = "Rabu"; break;
  case 4: hari = "Kamis"; break;
  case 5: hari = "Jum'at"; break;
  case 6: hari = "Sabtu"; break;
  }
  switch(bulan) {
  case 0: bulan = "Januari"; break;
  case 1: bulan = "Februari"; break;
  case 2: bulan = "Maret"; break;
  case 3: bulan = "April"; break;
  case 4: bulan = "Mei"; break;
  case 5: bulan = "Juni"; break;
  case 6: bulan = "Juli"; break;
  case 7: bulan = "Agustus"; break;
  case 8: bulan = "September"; break;
  case 9: bulan = "Oktober"; break;
  case 10: bulan = "November"; break;
  case 11: bulan = "Desember"; break;
  }
  var tampilTanggal = "Tanggal: " + hari + ", " + tanggal + " " + bulan + " " + tahun;
  var tampilWaktu = "Jam: " + jam + ":" + menit + ":" + detik;
  return tampilWaktu + " | " + tampilTanggal
}
export default async (req: NextApiRequest, res: NextApiResponse) => {
  const { body, method } = req;

  switch (method) {
    case "GET":
      const token = req.cookies.idtoken;
      var id = (await admin.auth().verifyIdToken(token)).uid;

      var allTransaction:Array<any> = await TransactionService.getAllTransaction();
      console.log("test :")
      var filteredTransaction:any = [];
      for (let index = 0; index < allTransaction.length; index++) {
        const element = allTransaction[index];
        if (element.agenId === id && element.status === "done"){
          const pasar = await element.idPasar.get()
          const pembeli = await admin.auth().getUser(element.pembeliId)
          filteredTransaction.push({
            pasar_name : pasar.name,
            pasar_address : pasar.address,
            ...element,
            pembeli, 
            tanggal : getTanggal(element.date._seconds)
          })
        }
        
      }
      filteredTransaction = filteredTransaction.sort(function(a:any, b:any){
        return (
          new Date(b.date._seconds * 1000).valueOf() -
          new Date(a.date._seconds * 1000).valueOf()
        );
      })
      res.status(200).send(filteredTransaction);

  }
};
