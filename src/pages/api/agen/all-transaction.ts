import admin from "@/database/admin";
import { NextApiRequest, NextApiResponse } from "next";
import TransactionService from "@/api-services/transaction";

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const { body, method } = req;

  switch (method) {
    case "GET":

            var allTransaction = (await TransactionService.getAllTransaction());
        res.status(200).send(allTransaction)
  }
};
