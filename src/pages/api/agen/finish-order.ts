import admin from "@/database/admin";
import { NextApiRequest, NextApiResponse } from "next";
import TransactionService from "@/api-services/transaction";

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const { body, method } = req;

  switch (method) {
    case "POST":
        const token = req.cookies.idtoken;
        var id = (await admin.auth().verifyIdToken(token)).uid
        var data = await admin.auth().getUser(id);
        var order = null;
        if ("customClaims" in data){
            order = data.customClaims===undefined?null:data.customClaims.order;
        }
        var transactionId = order.transactionId;

        await TransactionService.finishTransaction(transactionId)
        
        await admin.auth().setCustomUserClaims(data.uid, {
          ...(data.customClaims),
          order : {
            status : "no",
            transactionId : ""
          }
            // role : "agen",
            // isAccepted : true,
            // isActive : data.customClaims===undefined?false:data.customClaims.isActive
        })

        res.status(200).send("ok")
  }
};
