import admin from "@/database/admin";
import { NextApiRequest, NextApiResponse } from "next";

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const { body, method } = req;

  switch (method) {
    case "POST":
      const token = req.cookies.idtoken;
      // console.log("dibawah ini tokennya");
      // console.log(token);
      var id = (await admin.auth().verifyIdToken(token)).uid
      var data = await admin.auth().getUser(id);
      // console.log("=======")
      // console.log("uid :  ", data.uid)
      // console.log(data)
      var yey = await admin.auth().setCustomUserClaims(data.uid, {
        ...(data.customClaims),
        isActive : !(data.customClaims===undefined?false:data.customClaims.isActive)
      })
      // console.log("====")
      // console.log(yey)
      res.status(200).send("ok")
  }
};
