import { firestore } from "firebase-admin/lib/firestore";
import { NextApiRequest, NextApiResponse } from "next";
import { db } from "@/database/admin";
import createResponse from "@/util/createResponse";
import ProposalAgen from "@/models/ProposalAgen";
import phone from "phone";

const proposalAgenCollection = db.collection("proposal-agen");

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const { body, method } = req;

  switch (method) {
    case "POST":
      try {
        const props: ProposalAgen = body;
        console.log("hha");

        const [phoneNumber, country] = phone(props.phoneNumber, "IDN");
        console.log(phoneNumber);
        if (!phoneNumber) {
          res
            .status(400)
            .send(
              createResponse(
                "Nomor HP invalid, harap mengikuti format standar",
                "error"
              )
            );
          return;
        }
        const proposal: ProposalAgen = {
          ...props,
          phoneNumber,
          status: "submitted",
        };

        await proposalAgenCollection.add(proposal);

        res.status(302).redirect("/signup/agen/submitted");
      } catch (err) {
        console.log(err);
        res
          .status(400)
          .send(createResponse("Ada error dalam pembuatan proposal", "error"));
      }
      break;
    default:
      res.status(200).send("not e");
  }
};
