import admin from "@/database/admin";
import { NextApiRequest, NextApiResponse } from "next";
import TransactionService from "@/api-services/transaction";
import PasarService from "@/api-services/pasar";
import Pasar from "@/models/Pasar";

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const { body, method } = req;

  switch (method) {
    case "GET":
      const token = req.cookies.idtoken;
      var id = (await admin.auth().verifyIdToken(token)).uid;
      var agenDetails = await admin.auth().getUser(id);
      var order = null;
      if ("customClaims" in agenDetails) {
        order =
          agenDetails.customClaims === undefined
            ? null
            : agenDetails.customClaims.order;
      }
      console.log(order);
      if (order && order.status === "yes") {
        var transactionId = order.transactionId;
        console.log(transactionId);
        var allTransaction: any = (
          await TransactionService.getTransactionByID(transactionId)
        )[0];
        var pembeli = await admin
          .auth()
          .getUser(allTransaction.pembeliId as string);

        const pasar: Pasar = (
          await allTransaction.idPasar.get()
        ).data() as Pasar;

        res.status(200).send({
          pasar_name: pasar.name,
          pasar_address: pasar.address,
          ...allTransaction,
          pembeli,
        });
      } else {
        res.status(200).send(null);
      }
  }
};
