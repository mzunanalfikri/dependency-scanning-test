import admin from "@/database/admin";
import { auth } from "firebase-admin";
import { NextApiRequest, NextApiResponse } from "next";
import webpush from "web-push";

const publicVapidKey =
  "BMdz3IOWzogFjM9mXj02Dce-mGGlfhMx-8uh0A-Mae4aGIckXzd-7f3HtHwDOy0z9D9IGVR60bG0HXFOC7-CPcY";
const privateVapidKey = "8puypyUTurkaBe2XUDX39BVvn2ljSv40qGSZ6Ylml8Y";

webpush.setVapidDetails(
  "mailto:titipbelanja.business@gmail.com",
  publicVapidKey,
  privateVapidKey
);
export default async (req: NextApiRequest, res: NextApiResponse) => {
  const { body, method } = req;

  switch (method) {
    case "POST":
      if (body.uid) {
        admin
          .auth()
          .getUser(body.uid)
          .then((agen) => {
            if (
              agen.customClaims != undefined &&
              "notification" in agen.customClaims
            ) {
              webpush.sendNotification(
                agen.customClaims.notification,
                JSON.stringify({
                  title: "Anda memiliki pesanan",
                  body: "Klik disini untuk melihat pesanan.",
                })
              );
              res.status(200).send("Berhasil di notifikasi");
            } else {
              res.status(201).send("Agen tidak mengaktifkan notifikasi.");
            }
          })
          .catch((e) => {
            res.status(201).send(e);
          });
      } else {
        res.status(401).send("Body salah, tidak ditemukan uid dalam body.");
      }
  }
};
