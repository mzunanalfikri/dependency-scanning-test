import admin from "@/database/admin";
import { getCollection } from "@/database/firebase";
import createResponse from "@/util/createResponse";
import { NextApiRequest, NextApiResponse } from "next";
import nc from "next-connect";
import TransactionService from "@/api-services/transaction";
import axios from "axios";

const apiRoutes = nc<NextApiRequest, NextApiResponse>({
  onNoMatch(req, res) {
    res.status(405).send(createResponse("no such route", "error"));
  },
});

apiRoutes.get(async (req, res) => {
  const { id }: { id?: string } = req.query;
  try {
    res.status(200).send({
      res: await TransactionService.getAllTransaction(),
    });
  } catch (err) {
    console.log(err);
  }
});

apiRoutes.post(async (req, res) => {
  try {
    console.log(req.query);
    const token = req.cookies.idtoken;
    const idpembeli = (await admin.auth().verifyIdToken(token)).uid;

    const body = req.body;
    console.log(body);

    let randomAgen = await TransactionService.getRandomAgen();
    let secondTimeout = 0;
    while (randomAgen == undefined && secondTimeout < 30) {
      console.log(randomAgen);
      const searchAgenContinously = await TransactionService.getRandomAgen();
      if (searchAgenContinously != undefined) {
        randomAgen = searchAgenContinously;
        break;
      }
      secondTimeout += 1;
      console.log(secondTimeout);
    }

    if (randomAgen == undefined) {
      res.status(201).send("Failed to get agen. Try order again");
      // console.log("Failed to get agen. Try order again")
    }

    if (randomAgen != undefined) {
      const transaction = await TransactionService.addTransaction(
        body,
        randomAgen,
        idpembeli
      );

      var getTransaction = await transaction.get();

      var data = await admin.auth().getUser(randomAgen);
      admin.auth().setCustomUserClaims(data.uid, {
        ...data.customClaims,
        order: {
          status: "yes",
          transactionId: getTransaction.id,
        },
      });
      axios
        .post("/api/agen/broadcast-notification", {
          uid: randomAgen,
        })
        .then((res) => {
          // proses respon
          console.log(res);
        })
        .catch((err) => {
          console.log(err)
        });
      res.status(200).send(getTransaction.id);
    }
  } catch (err) {
    console.log(err);
  }
});

export default apiRoutes;
