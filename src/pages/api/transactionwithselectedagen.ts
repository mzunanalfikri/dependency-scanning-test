import admin from "@/database/admin";
import { getCollection } from "@/database/firebase";
import createResponse from "@/util/createResponse";
import { NextApiRequest, NextApiResponse } from "next";
import nc from "next-connect";
import TransactionService from "@/api-services/transaction";
import axios from "axios";

const apiRoutes = nc<NextApiRequest, NextApiResponse>({
  onNoMatch(req, res) {
    res.status(405).send(createResponse("no such route", "error"));
  },
});

apiRoutes.post(async (req, res) => {
  try {
    const token = req.cookies.idtoken;
    const idpembeli = (await admin.auth().verifyIdToken(token)).uid;

    const body = req.body;
    console.log(body);

    const transaction = await TransactionService.addTransactionWithSelectedAgen(
      body,
      idpembeli
    );

    var getTransaction = await transaction.get();

    var data = await admin.auth().getUser(body.idAgen);
    admin.auth().setCustomUserClaims(data.uid, {
      ...data.customClaims,
      order: {
        status: "yes",
        transactionId: getTransaction.id,
      },
    });
    axios
      .post("/api/agen/broadcast-notification", {
        uid: body.idAgen,
      })
      .then((res) => {
        // proses respon
        console.log(res);
      }).catch(() => {
        console.log("Notification not active.")
      });
    res.status(200).send(getTransaction.id);
  } catch (err) {
    console.log(err);
  }
});

export default apiRoutes;
