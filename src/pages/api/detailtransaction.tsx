import admin from "@/database/admin";
import { NextApiRequest, NextApiResponse } from "next";
import TransactionService from "@/api-services/transaction";


export default async (req: NextApiRequest, res: NextApiResponse) => {
  const { body, method } = req;

  switch (method) {
    case "GET":
        console.log(req.query.id)
        var detailtransaction:any = await TransactionService.getTransactionByID(req.query.id as string)
        console.log("Hello")
        console.log(detailtransaction)
        let agenid = detailtransaction[0].agenId as string
        console.log(agenid)
        var agen = await admin.auth().getUser(agenid);
        console.log(agen)

        res.status(200).send({
            ...detailtransaction[0],
            agen: agen
        })
  }
};
