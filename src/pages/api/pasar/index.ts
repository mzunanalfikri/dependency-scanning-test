import PasarService from "@/api-services/pasar";
import Pasar from "@/models/Pasar";
import { firestore } from "firebase-admin/lib/firestore";
import { NextApiRequest, NextApiResponse } from "next";

const GeoPoint = firestore.GeoPoint;

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const { body, method } = req;

  switch (method) {
    case "GET":
      res.status(200).send({
        res: await PasarService.getAllPasar(),
      });
      break;
    case "POST":
      try {
        // await market.add({ name: "Free city" });
        let pasar: Pasar = {
          ...body,
          geopoint: new GeoPoint(Number(body.latitude), Number(body.longitude)),
        };
        let msg = await PasarService.addPasar(pasar);
        if (msg == "") {
          res.status(200).send("Berhasil menyimpan.");
        } else {
          res.status(204).send(msg);
        }
      } catch (err) {
        console.log(err.stack);
        res
          .status(500)
          .send(PasarService.createFailureMessage("Ada kesalahan di server"));
      }
  }
};
