import ItemService from "@/api-services/item-service";
import createResponse from "@/util/createResponse";
import { NextApiRequest, NextApiResponse } from "next";
import nc from "next-connect";

const apiRoutes = nc<NextApiRequest, NextApiResponse>({
  onNoMatch(req, res) {
    res.status(405).send(createResponse("no such route", "error"));
  },
});

// todo: create item
apiRoutes.post(async (req, res) => {
  const isItemExist = await ItemService.isItemExist(req.body.itemId);
  res.status(200).send(createResponse("", "success", isItemExist));
});

export default apiRoutes;
