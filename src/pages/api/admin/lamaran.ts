import { auth, db } from "@/database/admin";
import { AuthenticatedMiddleware } from "@/middlewares/auth";
import ProposalAgen from "@/models/ProposalAgen";
import createResponse from "@/util/createResponse";
import { NextApiRequest, NextApiResponse } from "next";
import nc from "next-connect";

const collectionName: DbCollectionName = "proposal-agen";

const apiRoutes = nc<NextApiRequest, NextApiResponse>({
  onNoMatch(req, res) {
    res.status(405).send(createResponse("no such route", "error"));
  },
});

apiRoutes.use(AuthenticatedMiddleware);

// accept or decline lamaran
apiRoutes.post<RequestWithRole, NextApiResponse>(async (req, res) => {
  const { id, status }: ProposalAgen = req.body;
  const doc = db.collection(collectionName).doc(id);
  try {
    if (req.role != "admin") {
      res.status(300).send(createResponse("unauthenticated", "error"));
      return;
    }
    const data: ProposalAgen = (await doc.get()).data() as ProposalAgen;

    if (status == "accepted") {
      console.log("accepted");
      const userRecord = await auth.createUser({
        email: data.email,
        password: "new_agent",
        phoneNumber: data.phoneNumber,
        displayName: data.fullName,
      });
      console.log(userRecord);
      await auth.setCustomUserClaims(userRecord.uid, {
        isAccepted: true,
        role: "agen",
      });
      res.status(200).send(createResponse("", "success", userRecord));
      return;
    }
    await doc.update({
      status,
    });
    res.status(200).send(createResponse("", "success"));
  } catch (err) {
    // on error decline it
    await doc.update({
      status: "declined",
    });
    res.status(400).send(createResponse(err.message, "error"));
  }
});

export default apiRoutes;
