import { auth } from "@/database/admin";
import { AuthenticatedMiddleware } from "@/middlewares/auth";
import createResponse from "@/util/createResponse";
import { NextApiRequest, NextApiResponse } from "next";
import nc from "next-connect";

const apiRoutes = nc<NextApiRequest, NextApiResponse>({
  onNoMatch(req, res) {
    res.status(405).send(createResponse("no such route", "error"));
  },
});

apiRoutes.use(AuthenticatedMiddleware);

apiRoutes.post(
  async (req: RequestWithRole & NextApiRequest, res: NextApiResponse) => {
    if (req.role !== "admin") {
      res.status(300).end(createResponse("You are not authenticated", "error"));
      return;
    }
    try {
      const { uid } = req.body;
      await auth.setCustomUserClaims(uid, {
        banned: false,
      });
      console.log("user unbanned");
      res.status(200).send(createResponse("Banned the user", "success"));
    } catch (err) {
      res.status(400).send(createResponse(String(err.message), "error"));
    }
  }
);

export default apiRoutes;
