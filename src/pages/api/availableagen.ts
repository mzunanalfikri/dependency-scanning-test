import admin from "@/database/admin";
import { getCollection } from "@/database/firebase";
import createResponse from "@/util/createResponse";
import { NextApiRequest, NextApiResponse } from "next";
import nc from "next-connect";
import TransactionService from "@/api-services/transaction";
import axios from "axios";

const apiRoutes = nc<NextApiRequest, NextApiResponse>({
  onNoMatch(req, res) {
    res.status(405).send(createResponse("no such route", "error"));
  },
});

apiRoutes.get(async (req, res) => {
  try {
    let availableAgen = await TransactionService.getAvailableAgen();

    res.status(200).send(availableAgen);
  } catch (err) {
    console.log(err);
  }
});

export default apiRoutes;
