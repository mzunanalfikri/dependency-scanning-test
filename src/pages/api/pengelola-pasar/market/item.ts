import ItemService from "@/api-services/item-service";
import { db } from "@/database/admin";
import Item from "@/models/Barang";
import createResponse from "@/util/createResponse";
import { NextApiRequest, NextApiResponse } from "next";
import nc from "next-connect";

const itemsCollectionName: DbCollectionName = "items";
const itemsCollection = db.collection(itemsCollectionName);
const marketCollectionName: DbCollectionName = "market";
const getMarketItemCollection = (marketId: string) =>
  db
    .collection(marketCollectionName)
    .doc(marketId)
    .collection(itemsCollectionName);

const apiRoutes = nc<NextApiRequest, NextApiResponse>({
  onNoMatch(req, res) {
    res.status(405).send(createResponse("no such route", "error"));
  },
});

interface AddItemProp extends Item {
  marketId: string;
}

apiRoutes.get(async (req, res) => {
  const { name } = req.query;
  console.log(name);
  res.send("good");
});

apiRoutes.post(async (req, res) => {
  try {
    const {
      name,
      price,
      category,
      image,
      description,
      marketId,
    }: AddItemProp = req.body;
    console.log(name);
    const itemDocRef = itemsCollection.doc(name);
    const doc = await itemDocRef.get();
    if (!doc.exists) {
      console.log("creating a new item");
      await itemsCollection.doc(name).set({
        kategori: category,
        name,
        image,
        deskripsi: description ?? `sebuah ${name}`,
      });
    }
    const marketItemCol = getMarketItemCollection(marketId);
    await marketItemCol.add({
      item: itemDocRef,
      price: Number(price),
    });
    res.status(200).send(createResponse("Successfully added item", "success"));
  } catch (err) {
    console.error(err.message);
    res.status(400).send(createResponse(err.message, "error"));
  }
});

apiRoutes.delete(async (req, res) => {
  try {
    const { itemId, marketId } = req.body;
    await ItemService.deleteItemFromPasar(itemId, marketId);
    res
      .status(200)
      .send(createResponse("Successfully deleted the item", "success"));
  } catch (err) {
    res.status(400).send(createResponse(err.message, "error"));
  }
});

export default apiRoutes;
