import admin from "@/database/admin";
import { NextApiRequest, NextApiResponse } from "next";

const db = admin.firestore();
const pasarCollection = db.collection("market");

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const { body, method } = req;

  switch (method) {
    case "GET":
      let listUser = await getUser();
      res.status(200).send(listUser);
  }
};

async function getUser() {
  let data = await admin.auth().listUsers();
  let listUser = data["users"];
  listUser = listUser.filter((o) => {
    if (!o.customClaims) return;
    if ("customClaims" in o && "role" in o.customClaims) {
      return o.customClaims.role === "pengelola-pasar";
    }
    return false;
  });
  if (!listUser) {
    throw "No user seen";
  }

  for (let i = 0; i < listUser.length; i++) {
    const customClaims = listUser[i].customClaims;
    if (!customClaims) throw "No object";
    const pasarData = (
      await pasarCollection.doc(customClaims.pasar).get()
    ).data();
    if (!pasarData) {
      throw "No pasar data";
    }
    if (!!listUser[i].customClaims) {
      customClaims.namaPasar = pasarData["name"];
    }
  }

  return listUser;
}
