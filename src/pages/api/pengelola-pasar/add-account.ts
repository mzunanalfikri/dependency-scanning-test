import admin from "@/database/admin";
import { NextApiRequest, NextApiResponse } from "next";

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const { body, method } = req;

  switch (method) {
    case "POST":
      const { email, password, username, idPasar } = body;
      let phoneNumber = String(body.phoneNumber);
      if (phoneNumber[0] == "0") {
        phoneNumber = "+62" + phoneNumber.substr(1, phoneNumber.length - 1);
      }
      admin
        .auth()
        .createUser({
          email,
          password,
          displayName: username,
          phoneNumber,
        })
        .then((data) => {
          // console.log(data);
          admin.auth().setCustomUserClaims(data.uid, {
            role: "pengelola-pasar",
            pasar: idPasar,
          });
          res.status(200).send("ok");
        })
        .catch((error) => {
          res.status(201).send(error.message);
        });
  }
};
