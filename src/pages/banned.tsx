import VisitorLayout from "@/layouts/visitor/VisitorLayout";
import React from "react";
import { Container, Jumbotron } from "react-bootstrap";

function Banned() {
  return (
    <VisitorLayout title="You are banned">
      <Jumbotron fluid className="bg-danger m-5 text-white">
        <Container>
          <h1>Banned</h1>
          <p>
            Anda tidak bisa membuka aplikasi karena satu dan lain hal, harap
            kontak admin kami untuk meminta unban
          </p>
        </Container>
      </Jumbotron>
    </VisitorLayout>
  );
}

export default Banned;
