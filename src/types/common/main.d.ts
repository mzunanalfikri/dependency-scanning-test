/// <reference path="./layout.d.ts"/>
/// <reference path="./pengelola-pasar.d.ts"/>
/// <reference path="./header.d.ts"/>
/// <reference path="./dataterm.d.ts"/>
/// <reference path="./module.d.ts"/>
/// <reference path="./api.d.ts"/>
/// <reference path="./chatting.d.ts"/>

type RequestState = "success" | "loading" | "error" | "none";
type variant =
  | "primary"
  | "secondary"
  | "success"
  | "danger"
  | "warning"
  | "info"
  | "dark"
  | "light"
  | string;

interface ModalProps {
  showModal: boolean;
  onShowModal?: React.Dispatch<any>;
  onAcceptModal?: React.Dispatch<any>;
  onRejectModal?: React.Dispatch<any>;
  onCloseModal?: React.Dispatch<any>;
}

interface RequestStateProps {
  requestState: RequestState;
}

interface Clickable {
  onClick: React.MouseEventHandler<any>;
}

interface Deletable {
  onDelete: React.EventHandler<any>;
}

interface Editable {
  onEdit: React.EventHandler<any>;
}

interface HasClass {
  className?: string;
}

interface HasChildren {
  children?: React.ReactNode;
}

interface HasId {
  id?: string;
}

interface MessageFul {
  message: string;
  successMessage?: string;
  loadingMessage?: string;
  errorMessage?: string;
}

interface AnyObject {
  [propName: string]: any;
}
