type Role = "admin" | "pengelola-pasar" | "agen" | "visitor" | "pembeli";
type DbCollectionName =
  | "items"
  | "market"
  | "proposal-agen"
  | "transaction"
  | "laporan";
enum CollectionNames {
  ITEM,
  MARKET,
  PROPOSAL_AGEN,
  TRANSACTION,
}
