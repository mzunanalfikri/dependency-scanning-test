interface ResponseMessageProps {
  message: string;
  type: "error" | "success";
  data?: any;
}

type APIResponseType = "error" | "success";

type RequestWithRole = {
  isAuthenticated: boolean;
  role: Role;
};
