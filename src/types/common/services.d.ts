interface RedirectionInstruction<T = any> {
  redirectFrom: "server" | "client" | "none";
  clientRedirectTo?: string;
  data?: T;
}

interface WithRedirectionInstruction<T = any> {
  redirectionInstruction: RedirectionInstruction<T>;
}

interface AuthUserData {
  email: string;
  role: Role;
  banned: boolean;
  marketId?: string;
  name: string;
  uid: string;
}
