interface Message {
  from: MessageUserDetail;
  to: MessageUserDetail;
  text: string;
}

interface MessageUserDetail {
  name: string;
  id: string;
}
