interface BarangProps {
  name: string;
  imgsrc: string;
  price: number;
  onClick?: React.MouseEventHandler;
}

interface AddItemFormFieldProps {
  name: string;
  label: string;
  children?: React.ReactNode;
  as?: ElementType<any>;
  placeholder?: string;
  type?: string;
  custom?: boolean;
}
