interface HeaderProps {
  links: (NavLink | FunctionalLink)[];
}

interface NavLink {
  to: string;
  text: string;
  button?: boolean;
  variant?: variant;
}

interface FunctionalLink {
  callback: Function;
  text: string;
  button?: boolean;
  variant?: variant;
}

interface NavLinkProp extends NavLink {
  children: React.ReactNode;
  text?: string;
}
