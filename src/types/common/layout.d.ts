interface LayoutProps extends SEOProps {
  children?: React.ReactNode;
  Header: React.ReactNode;
  className?: string;
}

interface LayoutImplementProps extends LayoutProps {
  Header?: React.ReactNode;
}

interface SEOProps {
  title: string;
  description?: string;
  keywords?: string;
}
