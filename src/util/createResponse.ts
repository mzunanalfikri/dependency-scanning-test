function createResponse(
  message: string,
  type: APIResponseType,
  data?: any
): ResponseMessageProps {
  return {
    message,
    data,
    type,
  };
}

export default createResponse;
