export class SocketMessage {
  static SESSION = "session";
  static MESSAGE = "private message";
  static DISCONNECT = "disconnect";
}
