import { io } from "socket.io-client";

function createSocket(node_env: "development" | "test" | "production") {
  const url =
    node_env === "production"
      ? "https://titipinaja.vercel.app/"
      : "http://localhost:3000";
  const socket = io(url, { autoConnect: false });

  socket.onAny((event, ...args) => {
    console.log(event, args);
  });
  return socket;
}

export { createSocket };
