import admin from "@/database/admin";
import ApiService from "./api-service";

const db = admin.firestore();
const transactionCollection = db.collection("transaction");

class TransactionService extends ApiService {
  protected static serviceName: string = "pasar";

  static async getAllTransaction() {
    const docs = (await transactionCollection.get()).docs;
    const allData = docs.map((o) => {
      const id = o.id;
      const data = o.data();
      return { id, ...data };
    });
    return allData;
  }

  static async getTransactionByID(id: string) {
    const docs = (await transactionCollection.where("__name__", "==", id).get())
      .docs;
    const allData = docs.map((o) => {
      const id = o.id;
      const data = o.data();
      return { id, ...data };
    });
    return allData;
    // const docs = (await transactionCollection.doc(id).get());
    // return docs.data();
  }

  static async finishTransaction(id:string){
    transactionCollection.where("__name__","==",id).get().then(res => {
      res.forEach(doc => {
        doc.ref.update({
          status : "done"
        })
      })
    })
  }

  static async getRandomAgen() {
    let availableAgen = [];
    let listUser = (await admin.auth().listUsers()).users;
    listUser = listUser.filter((o) => {
      if (!o.customClaims) return;
      if ("customClaims" in o && "role" in o.customClaims) {
        // cek udah punya pesanan atau belum
        if (
          "order" in o.customClaims &&
          o.customClaims.order.status === "yes"
        ) {
          return false;
        }
        return (
          o.customClaims.role === "agen" &&
          o.customClaims.isAccepted === true &&
          o.customClaims.isActive === true
        );
      }
      return false;
    });
    for (var idx in listUser) {
      availableAgen.push(listUser[idx].uid);
    }

    let chooseRandomAgen =
      availableAgen[Math.floor(Math.random() * availableAgen.length)];
    return chooseRandomAgen;
  }

  static async getAvailableAgen() {
    let availableAgen = [];
    let listUser = (await admin.auth().listUsers()).users;
    listUser = listUser.filter((o) => {
      if (!o.customClaims) return;
      if ("customClaims" in o && "role" in o.customClaims) {
        // cek udah punya pesanan atau belum
        if (
          "order" in o.customClaims &&
          o.customClaims.order.status === "yes"
        ) {
          return false;
        }
        return (
          o.customClaims.role === "agen" &&
          o.customClaims.isAccepted === true &&
          o.customClaims.isActive === true
        );
      }
      return false;
    });
    for (var idx in listUser) {
      availableAgen.push(listUser[idx].uid);
    }

    return availableAgen;
  }

  static async addTransaction(
    body: any,
    randomAgen: string,
    idpembeli: string
  ) {
    const transactionCollection = db.collection("transaction");

    var transaction = await transactionCollection.add({
      pembeliId: idpembeli,
      agenId: randomAgen,
      pembeliLocation: new admin.firestore.GeoPoint(
        Number(body.userLocation.latitude),
        Number(body.userLocation.longitude)
      ),
      arrBarang: body.arrBarang,
      idPasar: admin.firestore().doc("market/" + body.idPasar),
      totalHarga: body.totalHarga,
      date: new Date(),
      status: "inprocess",
    });

    return transaction;
  }

  static async addTransactionWithSelectedAgen(body: any, idpembeli: string) {
    const transactionCollection = db.collection("transaction");

    var transaction = await transactionCollection.add({
      pembeliId: idpembeli,
      agenId: body.idAgen,
      pembeliLocation: new admin.firestore.GeoPoint(
        Number(body.userLocation.latitude),
        Number(body.userLocation.longitude)
      ),
      arrBarang: body.arrBarang,
      idPasar: admin.firestore().doc("market/" + body.idPasar),
      totalHarga: body.totalHarga,
      date: new Date(),
      status: "inprocess",
    });

    return transaction;
  }
}

export default TransactionService;
