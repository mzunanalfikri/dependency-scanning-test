import admin from "@/database/admin";
import PasarModel from "@/models/Pasar";
import ApiService from "./api-service";

const db = admin.firestore();
const pasarCollection = db.collection("market");
const pasarSubCollection = db.collectionGroup("itempasar");

class PasarService extends ApiService {
  protected static serviceName: string = "pasar";

  static async getAllPasar() {
    const docs = (await pasarCollection.get()).docs;
    const allData = docs.map((o) => {
      const id = o.id;
      const data = o.data();
      return { id, ...data };
    });
    return allData;
  }

  static async getPasarById() {
    //todo
  }

  static async getAllItems() {
    const docs = (await pasarSubCollection.get()).docs;
    const allData = docs.map((o) => {
      const id = o.id;
      const data = o.data();
      return { id, ...data };
    });
    return allData;
  }

  static async addPasar(body: PasarModel) {
    let error = [];
    if (body.name == null) error.push("Nama tidak boleh kosong");
    if (body.address == null) error.push("Alamat tidak boleh kosong");
    if (body.geopoint == null) error.push("Lokasi tidak boleh kosong");
    if (body.city == null) error.push("Kota tidak boleh kosong");
    if (body.geopoint == null)
      error.push("Geopoint harus berisi lokasi yang valid.");

    if (
      body.name != null &&
      body.city != null &&
      (await this.isPasarExistUsingNameAndCity(body.name, body.city))
    ) {
      error.push("Pasar sudah ada");
    }

    if (error.length == 0) {
      try {
        await pasarCollection.add(body);
      } catch (err) {
        error.push(err);
      }
    }

    return error.join(", ");
  }
  static async isPasarExistUsingNameAndCity(name: string, city: string) {
    const docs = (
      await pasarCollection
        .where("name", "==", name)
        .where("city", "==", city)
        .get()
    ).docs;
    const data = docs.map((o) => o.data());
    return data.length != 0;
  }
}

export default PasarService;
