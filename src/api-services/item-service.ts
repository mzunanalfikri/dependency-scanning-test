import admin from "@/database/admin";
import ApiService from "./api-service";

const db = admin.firestore();
const itemColName: DbCollectionName = "market";
const itemCollection = db.collection("items");
const pasarColName: DbCollectionName = "market";
const pasarCollection = db.collection(pasarColName);

class ItemService extends ApiService {
  protected static serviceName: string = "items";

  static async getAllItem() {
    const docs = (await itemCollection.get()).docs;
    const allData = docs.map((o) => o.data());
    return allData;
  }
  static async isItemExist(id: string) {
    const ref = itemCollection.doc(id);
    return (await ref.get()).exists;
  }
  static async deleteItemFromPasar(id: string, marketId: string) {
    const ref = pasarCollection.doc(marketId).collection("items").doc(id);
    return await ref.delete();
  }
}

export default ItemService;
