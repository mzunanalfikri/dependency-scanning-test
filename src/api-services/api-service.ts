class ApiService {
  protected static serviceName: string;

  constructor(serviceName: string) {
    ApiService.serviceName = serviceName;
  }

  static createResponse({ message, type }: ResponseMessageProps) {
    return {
      source: ApiService.serviceName,
      type,
      message,
    };
  }

  static createSuccessMessage(message: string) {
    return ApiService.createResponse({ message, type: "success" });
  }

  static createFailureMessage(message: string) {
    return ApiService.createResponse({ message, type: "error" });
  }
}
export default ApiService;
