type ProposalStatus = "accepted" | "declined" | "submitted";

interface ProposalAgen {
  id: string;
  fullName: string;
  email: string;
  phoneNumber: string;
  proposal: string;
  status: ProposalStatus;
}

export type { ProposalStatus };
export default ProposalAgen;
