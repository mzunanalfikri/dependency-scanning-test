import firestore from "firebase-admin/lib/firestore";

export interface GeoPoint {
    _latitude: number;
    _longitude: number;
  }

export default interface DetailTransaksi {
    arrBarang: [{
        name: string,
        qty: number
    }],
    totalHarga: number,
    id: string,
    date: {
        _seconds : number,
        _nanosecond: number,
    },
    agen : {
        customClaims: {
        role: string,
        isAccepted: boolean,
        isActive: boolean,
        order: {
            status: string,
            transactionId: string
        }

        },
        displayName : string,
        phoneNumber : string,
        id: string
    },
    agenId: string,
    pembeliLocation: GeoPoint | firestore.firestore.GeoPoint,
    status: string,

}