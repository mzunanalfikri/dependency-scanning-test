export type ItemCategory =
  | "sayuran"
  | "bahan makanan"
  | "snack"
  | "minuman"
  | "none";

interface Item {
  id?: string;
  name: string;
  price: number;
  category: ItemCategory[];
  image: string;
  description?: string;
}

export interface ItemWithQty extends Item {
  qty: number;
}

export interface AddItemRequest {
  name: string;
  price: number;
  category: string;
  image: File | null;
}

export default Item;
