export type LaporanType = "bug" | "ketidakpuasan" | "tampilan";

export default interface LaporanData {
  title: string;
  body: string;
  from: string;
  type: LaporanType;
  id?: string;
}
