import firestore from "firebase-admin/lib/firestore";

interface GeoPoint {
  _latitude: number;
  _longitude: number;
}

interface Pasar {
  id: string;
  name: string;
  city: string;
  address: string;
  geopoint: GeoPoint | firestore.firestore.GeoPoint;
  image?: string;
}

export type { GeoPoint };
export default Pasar;
