import { useAuth } from "@/contexts/AuthContext";
import React from "react";
import { Container } from "react-bootstrap";
import Header from "../Header";
import Layout from "../Layout";
import styles from "./PengelolaPasarLayout.module.scss";

function PengelolaPasarLayout({
  title,
  children,
  description,
  keywords,
}: LayoutImplementProps) {
  return (
    <Layout
      Header={<DEHeader />}
      title={title}
      description={description}
      keywords={keywords}
    >
      <Container
        fluid
        className={`${styles.main} container-padding py-3 h-100`}
      >
        {children}
      </Container>
    </Layout>
  );
}

const baseUrl = "/pengelola-pasar";

function DEHeader() {
  const auth = useAuth();

  return (
    <Header
      links={[
        {
          to: `${baseUrl}`,
          text: "Beranda",
        },
        {
          to: `${baseUrl}/market/recommend`,
          text: "Rekomen Pasar Baru",
        },
        {
          to: `${baseUrl}/profile/123123`,
          text: "Profil",
        },
        {
          text: "Logout",
          button: true,
          to: "/logout",
          variant: "danger",
        },
      ]}
    />
  );
}

export default PengelolaPasarLayout;
