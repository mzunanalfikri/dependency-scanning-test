import React from "react";
import { Navbar, Nav, Form, NavDropdown, Container } from "react-bootstrap";
import Link from "next/link";
import styles from "./AdminLayout.module.scss";
import styled from "styled-components";

function HeadAdmin() {
  return (
    <HeaderAdminElmt>
      <Navbar bg="black" expand="lg" className={styles.header}>
        <Container fluid>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ml-auto">
              <NavLink to="/login">
                <Nav.Link className="text-sm text-white">Ada kendala?</Nav.Link>
              </NavLink>
              <NavLink to="/login">
                <Nav.Link className="btn btn-primary btn-sm text-sm text-white">
                  Hubungi Developer
                </Nav.Link>
              </NavLink>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </HeaderAdminElmt>
  );
}

function NavLink({ to, children }: NavLinkProp) {
  return (
    <div className="mx-2 d-flex justify-content-center align-items-center">
      <Link href={to} passHref>
        {children}
      </Link>
    </div>
  );
}

const HeaderAdminElmt = styled.div`
  border-bottom: 3px solid rgba(0, 0, 0, 0.5);
`;

export default HeadAdmin;
