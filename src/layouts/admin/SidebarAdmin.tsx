import React from "react";
import { Navbar, Nav, Form, NavDropdown, Container } from "react-bootstrap";
import Link from "next/link";
import styles from "./AdminLayout.module.scss";
import styled from "styled-components";

function SidebarAdmin() {
  return (
    <SidebarAdminElmt>
      <Navbar bg="black" expand="lg" className={styles.sidebar}>
        <Container fluid className="d-flex flex-column align-items-start">
          <Link href="/admin" passHref>
            <div className={styles.foto}>
              <img alt="fotoprofil" src="/admin/fotoprofil.svg" height="50" />
            </div>
          </Link>
          <div className={`${styles.hallo} text-lg text-white`}>
            Hallo Admin
          </div>
          <br />
          <Nav className={`${styles.tombol} d-flex flex-column`}>
            <NavLink to="/admin/riwayat-pembelian">
              <Nav.Link className={`${styles.bagian} text-sm text-white`}>
                Riwayat Pembelian
              </Nav.Link>
            </NavLink>
            <NavLink to="/#pembeli">
              <Nav.Link className={`${styles.bagian} text-sm text-white`}>
                Pembeli
              </Nav.Link>
            </NavLink>
            <NavLink to="/admin/pasar">
              <Nav.Link className={`${styles.bagian} text-sm text-white`}>
                Pasar
              </Nav.Link>
            </NavLink>
            <NavLink to="/admin/pengelola-pasar">
              <Nav.Link className={`${styles.bagian} text-sm text-white`}>
                Pengelola Pasar
              </Nav.Link>
            </NavLink>
            <NavLink to="/admin/lamaran_agen">
              <Nav.Link className={`${styles.bagian} text-sm text-white`}>
                Lamaran Agen
              </Nav.Link>
            </NavLink>
            <NavLink to="/#agen">
              <Nav.Link className={`${styles.bagian} text-sm text-white`}>
                Agen
              </Nav.Link>
            </NavLink>
            <NavLink to="/#laporan">
              <Nav.Link className={`${styles.bagian} text-sm text-white`}>
                Laporan
              </Nav.Link>
            </NavLink>
            <NavLink to="/admin/set-role">
              <Nav.Link className={`${styles.bagian} text-sm text-white`}>
                Set Role
              </Nav.Link>
            </NavLink>
            <NavLink to="/admin/ban-user">
              <Nav.Link className={`${styles.bagian} text-sm text-white`}>
                Ban User
              </Nav.Link>
            </NavLink>
            <br />
            <NavLink to="/logout">
              <Nav.Link className="btn btn-danger btn-sm text-sm text-white">
                Logout
              </Nav.Link>
            </NavLink>
          </Nav>
        </Container>
      </Navbar>
    </SidebarAdminElmt>
  );
}

function NavLink({ to, children }: NavLinkProp) {
  return (
    <div>
      <Link href={to} passHref>
        {children}
      </Link>
    </div>
  );
}

const SidebarAdminElmt = styled.div`
  border-bottom: 3px solid rgba(0, 0, 0, 0.5);
`;

export default SidebarAdmin;
