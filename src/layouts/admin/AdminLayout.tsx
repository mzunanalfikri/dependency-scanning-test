import React from "react";
import Header from "./HeadAdmin";
//import Sidebar from "./Sidebar";
import styled from "styled-components";
import SidebarAdmin from "./SidebarAdmin";
import SEO from "@/components/SEO";
import styles from "@/pages/styles/Admin.module.scss";

const LayoutElmt = styled.div`
  min-height: 100vh;
  padding: 0;
`;

function AdminLayout({
  children,
  title,
  description,
  keywords,
}: LayoutImplementProps) {
  return (
    <LayoutElmt>
      <SEO title={title} description={description} keywords={keywords} />

      <Header />
      <SidebarAdmin />
      <main className={styles.content}>{children}</main>
    </LayoutElmt>
  );
}

export default AdminLayout;
