import { useAuth } from "@/contexts/AuthContext";
import React, { useEffect, useState } from "react";
import Header from "../Header";
import Layout from "../Layout";
import router from "next/router";

function VisitorLayout({ children, title, ...props }: LayoutImplementProps) {
  return (
    <Layout
      Header={<DEHeader />}
      title={title}
      description={props.description}
      keywords={props.keywords}
      className={props.className}
    >
      {children}
    </Layout>
  );
}

function DEHeader() {
  const auth = useAuth();
  const [authLink, setAuthLink] = useState<(NavLink | FunctionalLink)[]>([]);

  useEffect(() => {
    if (!!auth.user) {
      setAuthLink([
        {
          text: "Home",
          to: "/redirect",
        },
        {
          text: "Logout",
          button: true,
          variant: "danger",
          callback: async () => {
            await auth.signout();
            router.push("/");
          },
        },
      ]);
    } else {
      setAuthLink([
        {
          to: "/signup",
          text: "Daftar",
          button: true,
        },
        {
          to: "/login",
          text: "Masuk",
          button: true,
        },
      ]);
    }
  }, [auth.user]);

  return (
    <Header
      links={[
        {
          to: "/#about",
          text: "Tentang kami",
        },
        ...authLink,
      ]}
    />
  );
}

export default VisitorLayout;
