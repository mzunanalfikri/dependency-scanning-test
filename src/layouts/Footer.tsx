import LogoResponsive from "@/components/LogoResponsive";
import Link from "next/link";
import React from "react";
import styles from "./Footer.module.scss";

function Footer() {
  return (
    <div>
      <div className={styles.footer}>
        <div className={styles.logo}>
          <Link href="/" passHref>
            <div>
              <LogoResponsive />
            </div>
          </Link>
        </div>
        <div className={styles.kontak}>
          <a href="youtube.com" target="_blank" rel="kontakfacebook">
            <img
              className={styles.fb}
              alt="kontakfacebook"
              src="/landing_page/facebook.svg"
            />
          </a>
          <a href="youtube.com" target="_blank" rel="kontakig">
            <img
              className={styles.ig}
              alt="kontakig"
              src="/landing_page/instagram.svg"
            />
          </a>
        </div>
      </div>
    </div>
  );
}

export default Footer;
