import SEO from "@/components/SEO";
import React from "react";
import Footer from "./Footer";
import styles from "./Layout.module.scss";

function Layout({
  children,
  title,
  description,
  keywords,
  Header,
  className,
}: LayoutProps) {
  return (
    <div>
      <SEO title={title} description={description} keywords={keywords} />
      <div className={styles.layoutElmt}>
        <div className={`m-0 p-0 h-100 ${className} ${styles.main}`}>
          {Header}
          <div className={`${styles.pushHeader} h-100`}></div>
          {children}
        </div>
        <Footer />
      </div>
    </div>
  );
}

export default Layout;
