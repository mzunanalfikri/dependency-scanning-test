import { useAuth } from "@/contexts/AuthContext";
import React from "react";
import { Container } from "react-bootstrap";
import Header from "../Header";
import Layout from "../Layout";
import styles from "./AgenLayout.module.scss";

function AgenLayout({
  title,
  children,
  description,
  keywords,
}: LayoutImplementProps) {
  return (
    <Layout
      Header={<DEHeader />}
      title={title}
      description={description}
      keywords={keywords}
    >
      <Container
        fluid
        className={`${styles.main} container-padding bg-secondary pt-3 h-100`}
      >
        {children}
      </Container>
    </Layout>
  );
}

const baseUrl = "/agen";

function DEHeader() {
  const auth = useAuth();

  return (
    <Header
      links={[
        {
          to: `${baseUrl}`,
          text: "Beranda",
        },
        {
          to: `${baseUrl}/transaction-history`,
          text: "History Pesanan",
        },
        {
          to: `${baseUrl}/profile`,
          text: "Profil",
        },
        {
          text: "Logout",
          button: true,
          to: "/logout",
          variant: "danger",
        },
      ]}
    />
  );
}

export default AgenLayout;
