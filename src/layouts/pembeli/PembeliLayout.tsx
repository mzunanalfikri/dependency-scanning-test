import React from "react";
import Header from "../Header";
import Layout from "../Layout";

function PembeliLayout({ children, title, ...props }: LayoutImplementProps) {
  return (
    <Layout
      Header={<DEHeader />}
      title={title}
      description={props.description}
      keywords={props.keywords}
      className={props.className}
    >
      {children}
    </Layout>
  );
}

function DEHeader() {
  return (
    <Header
      links={[
        {
          to: "/pasarpembeli",
          text: "Beranda",
        },
        {
          to: "/pasarpembeli/myorder",
          text: "Pesanan Saya",
          button: true,
        },
        {
          to: "/pasarpembeli/historyorder",
          text: "Histori Pesanan",
          button: true,
        },
        {
          to: "/pasarpembeli/myprofile",
          text: "Profil",
          button: true,
        },
        {
          to: "/logout",
          text: "Keluar",
          button: true,
          variant: "danger",
        },
      ]}
    />
  );
}

export default PembeliLayout;
