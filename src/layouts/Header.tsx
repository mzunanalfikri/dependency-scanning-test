import React, { useState, useEffect } from "react";
import { Navbar, Nav, Button } from "react-bootstrap";
import Link from "next/link";
import styles from "./Header.module.scss";
import LogoResponsive from "@/components/LogoResponsive";
import { useSpring, animated } from "react-spring";
import breakpointinfo from "@/util/breakpointinfo";

const isFunctionalLink = (
  obj: FunctionalLink | NavLink
): obj is FunctionalLink => {
  return !!(obj as FunctionalLink).callback;
};

function Header({ links }: HeaderProps) {
  const [showSidebar, setShowSidebar] = useState(false);
  const fadeInAnimation = useSpring({
    opacity: 1,
    from: { opacity: 0 },
    config: { duration: 5 },
  });

  return (
    <Navbar
      bg="light"
      variant="light"
      id="navbar"
      expand="md"
      className={styles.header}
    >
      <Navbar.Brand className={`order-1 ${styles.navBrand}`} href="#home">
        <Link href="/" passHref>
          <div>
            <LogoResponsive />
          </div>
        </Link>
      </Navbar.Brand>
      <div
        className="cursor-pointer d-lg-none py-2"
        role="button"
        onClick={() => setShowSidebar(true)}
      >
        <i className="fas fa-bars text-dark"></i>
      </div>
      <div
        className={`${showSidebar ? styles.screenOverlay : ""}`}
        style={{
          transition: "1s",
        }}
        onClick={() => setShowSidebar(false)}
      ></div>
      <Navbar
        className={`order-2 ml-auto ${styles.navLinks} d-lg-flex ${
          showSidebar ? "" : "d-none"
        }`}
      >
        <Button
          className="d-block d-lg-none py-2 mt-3 text-light"
          onClick={() => setShowSidebar(false)}
        >
          <i className="fas fa-times"></i>
        </Button>
        <Link href="/" passHref>
          <div className="my-3 d-lg-none">
            <LogoResponsive fill="white" />
          </div>
        </Link>
        {links.map((link, idx) => (
          <div key={idx}>
            {isFunctionalLink(link) ? (
              <FunctionalLink callback={link.callback} text={link.text} />
            ) : (
              <NavLink to={link.to} button={link.button} variant={link.variant}>
                {link.text}
              </NavLink>
            )}
          </div>
        ))}
      </Navbar>
    </Navbar>
  );
}

const getButtonStyle = (variant?: string) =>
  `btn btn-${variant || "primary"} text-md text-white`;
const getTextStyle = () => `text-md px-0`;

function NavLink({ to, children, button, variant }: NavLinkProp) {
  const [buttonStyles, setButtonStyles] = useState(getButtonStyle(variant));

  useEffect(() => {
    if (window.innerWidth <= breakpointinfo.lg) {
      setButtonStyles(getTextStyle());
    }
  }, []);

  return (
    <div className={`${styles.navLink} pl-3`}>
      <Link href={to} passHref>
        <Nav.Link className={`${!!button ? buttonStyles : getTextStyle()}`}>
          {children}
        </Nav.Link>
      </Link>
    </div>
  );
}

function FunctionalLink({ callback, button, variant, text }: FunctionalLink) {
  const [buttonStyles, setButtonStyles] = useState(getButtonStyle(variant));

  useEffect(() => {
    if (window.innerWidth <= breakpointinfo.lg) {
      setButtonStyles(getTextStyle());
    }
  }, []);

  return (
    <div className={`${styles.navLink} pl-3`}>
      <Nav.Link
        className={`${!!button ? buttonStyles : getTextStyle()}`}
        onClick={() => {
          callback();
        }}
      >
        {text}
      </Nav.Link>
    </div>
  );
}

export default Header;
