import http from "http";
import * as socketio from "socket.io";

const crypto = require("crypto");
const randomId = () => crypto.randomBytes(8).toString("hex");

export function initSocket(server: http.Server) {
  const io: socketio.Server = new socketio.Server();
  io.attach(server);

  io.use((socket, next) => {
    const sessionId = socket.handshake.auth.sessionId;
    if (sessionId) {
      return next();
    }
    if (!socket.handshake.auth.id) {
      return next(new Error("no id defined"));
    }
    socket.data = socket.handshake.auth;
    next();
  });

  io.on("connection", (socket: socketio.Socket) => {
    const { sessionId } = socket.data;

    socket.join(sessionId);

    socket.on("private message", ({ text, from, to }: Message) => {
      socket.to(sessionId).emit("private message", {
        text,
        from,
        to,
      });
    });
  });
}
