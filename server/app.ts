import express, { Express, Request, Response } from "express";

const app: Express = express();

app.get("/hello", async (_: Request, res: Response) => {
  res.send("Hello World");
});

export default app;
