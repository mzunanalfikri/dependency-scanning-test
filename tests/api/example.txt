import { createMocks } from "node-mocks-http";
import hello from "../../src/pages/api/hello";

describe("/api/[animal]", () => {
  test("returns a message with the specified animal", async () => {
    const { req, res } = createMocks({
      method: "GET",
    });

    await hello(req, res);

    expect(res._getStatusCode()).toBe(200);
    expect(JSON.parse(res._getData())).toEqual(
      expect.objectContaining({ name: "John Doe" })
    );
  });
});
