import { render, screen } from "@testing-library/react";
import App from "@/pages/login";

describe("App", () => {
  it("renders without crashing", () => {
    render(<App />);
    expect(
      screen.getByRole("heading", {
        name: "Login" , 
      })
    ).toBeInTheDocument();
  });
});
