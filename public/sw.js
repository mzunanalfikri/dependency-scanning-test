self.addEventListener('push', (e) => {
  console.log(e.data)
    const data = e.data.json();
    self.registration.showNotification(data.title, {
      body: data.body,
      icon: "logo_tas.svg",
    });
  });
