const firebaseAdmin = require("firebase-admin");
const serviceAccountCredential = require("./serviceAccountKey.json");
const firebase = require("firebase").default;

var firebaseConfig = {
  apiKey: "AIzaSyD-_dLqfSOhctMiMc0uDt7Mqj6uTH-ec0Q",
  authDomain: "titip-belanja.firebaseapp.com",
  projectId: "titip-belanja",
  storageBucket: "titip-belanja.appspot.com",
  messagingSenderId: "394859598578",
  appId: "1:394859598578:web:721b51cfe10c63614cf66b",
  measurementId: "G-D3KQYZC1XP",
};

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
  firebaseAdmin.initializeApp({
    credential: firebaseAdmin.credential.cert(serviceAccountCredential),
  });
} else {
  firebase.app();
}

module.exports = { firebase, firebaseAdmin };
