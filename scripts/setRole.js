const admin = require("./admin").firebaseAdmin;
const readline = require("readline").createInterface({
  input: process.stdin,
  output: process.stdout,
});

const auth = admin.auth();

function getUserInput() {
  readline.question("User uid\n", async (uid) => {
    if (uid == "quit") {
      readline.close();
      return;
    }
    readline.question("User role\n", async (role) => {
      try {
        const res = await auth.setCustomUserClaims(uid, {
          role,
        });
        console.log(res);
        readline.write("Role added to the user");
        readline.close();
      } catch (err) {
        console.log(err);
      }
    });
    readline.close();
    getUserInput();
  });
}
getUserInput();

// while (go == true) {
// }
