const admin = require("./admin").firebaseAdmin;

const auth = admin.auth();

async function deleteUsers() {
  try {
    const users = await auth.listUsers(100);
    await auth.deleteUsers(
      users.users.map((user) => {
        return user.uid;
      })
    );
  } catch (err) {
    console.log(err);
  }
}

deleteUsers();
