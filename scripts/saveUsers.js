const admin = require("./admin").firebaseAdmin;
const fs = require("fs");

const db = admin.firestore();
const auth = admin.auth();

async function saveUsers() {
  const users = await auth.listUsers(100);
  const data = users.users.map((user) => ({
    email: user.email,
    id: user.uid,
    name: user.displayName,
  }));
  fs.writeFile("users.json", JSON.stringify(data));
}

saveUsers();
